#!/usr/bin/env python3

"""Defines the information to be used throughout the builds."""

# -*- mode:python; coding: utf-8 -*-

from collections import namedtuple
import versioneer

TamaasInfo = namedtuple('TamaasInfo',
                        ['version',
                         'authors',
                         'maintainer',
                         'email',
                         'copyright',
                         'description',
                         'license'])

TAMAAS_INFOS = TamaasInfo(
    version=versioneer.get_version(),
    authors=([
        u'Lucas Frérot',
        'Guillaume Anciaux',
        'Valentine Rey',
        'Son Pham-Ba',
        'Zichen Li',
        u'Jean-François Molinari'
    ]),
    maintainer=u'Lucas Frérot',
    email='lucas.frerot@sorbonne-universite.fr',
    copyright=(
        "Copyright (©) 2016-2024 EPFL "
        "(École Polytechnique Fédérale de Lausanne), "
        "Laboratory (LSMS - Laboratoire de Simulation en "
        "Mécanique des Solides)\n"
        "Copyright (©) 2020-2024 Lucas Frérot"
    ),
    description='A high-performance library for periodic rough surface contact',
    license="SPDX-License-Identifier: AGPL-3.0-or-later",
)


def main():
    import argparse

    parser = argparse.ArgumentParser(description="Print Tamaas info")
    parser.add_argument("--version", action="store_true")

    args = parser.parse_args()

    if args.version:
        print(TAMAAS_INFOS.version)


if __name__ == "__main__":
    main()
