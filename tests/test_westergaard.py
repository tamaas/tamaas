# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import division, print_function

import tamaas as tm
import pytest
import numpy as np
from conftest import pkridfn

from numpy.linalg import norm


@pytest.fixture(scope="module",
                params=[tm.PolonskyKeerRey.pressure,
                        tm.PolonskyKeerRey.gap],
                ids=pkridfn)
def pkr(westergaard, request):
    loads = {
        tm.PolonskyKeerRey.pressure: westergaard.load,
        tm.PolonskyKeerRey.gap: 0.06697415181446396
    }

    model = tm.ModelFactory.createModel(tm.model_type.basic_1d, [1.],
                                        [westergaard.n])
    model.E, model.nu = westergaard.e_star, 0
    solver = tm.PolonskyKeerRey(model, westergaard.surface, 1e-10,
                                request.param,
                                request.param)
    solver.max_iter = 5000
    solver.solve(loads[request.param])
    return model, westergaard


@pytest.fixture(scope="module")
def kato(westergaard, request):
    load = westergaard.load

    model = tm.ModelFactory.createModel(tm.model_type.basic_1d, [1.],
                                        [westergaard.n])
    model.E, model.nu = westergaard.e_star, 0
    solver = tm.KatoSaturated(model, westergaard.surface, 1e-5, np.inf)
    solver.max_iter = 5000
    solver.solve(load)
    return model, westergaard


@pytest.fixture(scope="module")
def tao(westergaard, request):
    if getattr(tm, "petsc", None) is None:
        pytest.skip("petsc is disabled")

    load = westergaard.load
    model = tm.ModelFactory.createModel(tm.model_type.basic_1d, [1.],
                                        [westergaard.n])
    model.E, model.nu = westergaard.e_star, 0
    westergaard.surface -= westergaard.delta / 2
    solver = tm.petsc.OptimizationSolver(model, westergaard.surface, 1e-9)
    solver.max_iter = 5000
    print(solver.solve(load))

    # Resetting load to fit displacement-constrained solver from PETSc
    load = model.traction.mean()
    westergaard.__init__(westergaard.n,
                         load / westergaard.p_star,
                         westergaard.e_star)

    return model, westergaard


def compute_error_energy(fixture):
    model, sol = fixture
    traction, displacement = model['traction'], model['displacement']
    error = norm((traction - sol.pressure)*(displacement - sol.displacement))
    error /= norm(sol.pressure * sol.displacement)
    return error


def test_energy_pkr(pkr):
    assert compute_error_energy(pkr) < 1e-10


def test_energy_kato(kato):
    assert compute_error_energy(kato) < 1e-5


def test_energy_tao(tao):
    assert compute_error_energy(tao) < 1e-5


def test_maxwell(westergaard):
    model = tm.Model(tm.model_type.basic_1d, [1.], [westergaard.n])

    G_ref, nu_ref = 2, 0.5
    E_ref = G_ref * (2 * (1 + nu_ref))

    model.E, model.nu = E_ref, nu_ref

    westergaard.__init__(westergaard.n, westergaard.load / westergaard.p_star,
                         model.E_star)

    G = [1]
    tau = [1]

    solver_short = tm.MaxwellViscoelastic(model, westergaard.surface, 1e-9,
                                          time_step=0., shear_moduli=G,
                                          characteristic_times=tau)
    solver_long = tm.MaxwellViscoelastic(model, westergaard.surface, 1e-12,
                                         time_step=np.inf, shear_moduli=G,
                                         characteristic_times=tau)

    # Testing long times
    solver_long.solve(westergaard.load)
    assert compute_error_energy((model, westergaard)) < 1e-10

    # Testing short times
    westergaard.__init__(westergaard.n,
                         westergaard.load / westergaard.p_star,
                         model.E_star + 2 * G[0] / (1 - model.nu))

    model.traction[:] = 0
    model.displacement[:] = 0
    solver_short.solve(westergaard.load)

    area0_num = np.mean(model.traction > 0)

    assert compute_error_energy((model, westergaard)) < 1e-10

    # Testing time evolution
    dt = 5e-2
    solver_slope = tm.MaxwellViscoelastic(model, westergaard.surface, 1e-12,
                                          time_step=dt, shear_moduli=G,
                                          characteristic_times=tau)
    model.traction[:] = 0
    model.displacement[:] = 0

    # area0 = 2 * westergaard.a / westergaard.domain_size
    areas = [area0_num]

    solver_slope.solve(westergaard.load)
    areas.append(np.mean(model.traction > 0))

    slope = (areas[1] - areas[0]) / dt

    # Reference slope from Westergaard's solution
    w0 = westergaard.load / westergaard.p_star
    Einf = model.E_star
    E0 = westergaard.e_star
    slope_ref = 2 * westergaard.lamda / (2 * np.pi * tau[0]) \
        * (1 - Einf/E0) * np.sqrt(w0 / (1 - w0))

    error = np.abs(slope - slope_ref) / slope_ref
    assert error < 3e-2


@pytest.mark.xfail(tm.TamaasInfo.backend == 'tbb',
                   reason='TBB reductions are unstable?')
def test_pkr_multi(westergaard):
    model_basic = tm.ModelFactory.createModel(tm.model_type.basic_1d, [1.],
                                              [westergaard.n])
    model_volume = tm.ModelFactory.createModel(tm.model_type.volume_1d,
                                               [1., 1.],
                                               [20, westergaard.n])

    pressures = {}
    for model in [model_basic, model_volume]:
        print(model)
        solver = tm.PolonskyKeerRey(model, westergaard.surface, 1e-12,
                                    tm.PolonskyKeerRey.pressure,
                                    tm.PolonskyKeerRey.pressure)
        solver.solve(westergaard.load)
        pressures[model] = model['traction']

    error = norm(pressures[model_basic] - pressures[model_volume][:, 1])\
        / westergaard.load
    assert error < 1e-16


if __name__ == "__main__":
    print("Test is meant to run with pytest")
