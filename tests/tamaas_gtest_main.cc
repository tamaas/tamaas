/*
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "mpi_interface.hh"
#include "test.hh"
#ifdef TAMAAS_USE_PYTHON
#include <pybind11/pybind11.h>
#include <pybind11/embed.h>
#include <pybind11/numpy.h>
#include <pybind11/pybind11.h>
#endif
/* -------------------------------------------------------------------------- */

class TamaasEnvironment : public ::testing::Environment {
public:
  void SetUp() override { ::tamaas::initialize(); }

  void TearDown() override { ::tamaas::finalize(); }
};

/* -------------------------------------------------------------------------- */
int main(int argc, char ** argv) {
#ifdef TAMAAS_USE_PYTHON
#ifdef TAMAAS_DEBUG
  std::cerr << "Initializing python interpreter" << std::endl;
#endif
  pybind11::scoped_interpreter guard{};
#endif
  ::testing::InitGoogleTest(&argc, argv);
  ::testing::AddGlobalTestEnvironment(new TamaasEnvironment());

#ifndef TAMAAS_DEBUG
  ::testing::TestEventListeners& listeners =
      ::testing::UnitTest::GetInstance()->listeners();
  if (::tamaas::mpi::rank() != 0) {
    delete listeners.Release(listeners.default_result_printer());
  }
#endif
  return RUN_ALL_TESTS();
}
