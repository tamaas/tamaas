/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 * Copyright (©) 2016-2024 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 * Tamaas is free  software: you can redistribute it and/or  modify it under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Tamaas is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with Tamaas. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "fftw/interface.hh"
#include "tamaas.hh"
#include "gtest/gtest.h"
#include <algorithm>
#include <array>
#include <limits>
#include <numeric>
#include <thrust/complex.h>
#include <utility>

template <typename T, typename U>
bool compare(T&& a, U&& b) {
  return std::mismatch(std::begin(a), std::end(a), std::begin(b)) ==
         std::make_pair(std::end(a), std::end(b));
}

template <typename T, typename U, typename Pred>
bool compare(T&& a, U&& b, Pred&& pred) {
  return std::mismatch(std::begin(a), std::end(a), std::begin(b), pred) ==
         std::make_pair(std::end(a), std::end(b));
}

struct AreFloatEqual {
  tamaas::Real tolerance = 4 * std::numeric_limits<tamaas::Real>::epsilon();
  template <typename T, typename U>
  inline bool operator()(T&& x, U&& y) {
    // T y = reinterpret_cast<T>(z);
    tamaas::Real abs_max = std::max<std::decay_t<T>>(std::abs(x), std::abs(y));
    abs_max = std::max(abs_max, tamaas::Real(1.));
    return std::abs(x - y) <= (tolerance * abs_max);
  }
};

struct AreComplexEqual {
  tamaas::Real tolerance = 4 * std::numeric_limits<tamaas::Real>::epsilon();
#ifdef TAMAAS_USE_FFTW
  inline bool operator()(const tamaas::Complex& x,
                         const fftw::helper<tamaas::Real>::complex& y) {
    auto equal = AreFloatEqual{tolerance};
    return equal(x.real(), y[0]) && equal(x.imag(), y[1]);
  }
#endif
  inline bool operator()(const tamaas::Complex& x, const tamaas::Complex& y) {
    auto equal = AreFloatEqual{tolerance};
    return equal(x.real(), y.real()) && equal(x.imag(), y.imag());
  }
};
