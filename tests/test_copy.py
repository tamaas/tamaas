# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import copy
import pytest

from numpy.testing import assert_allclose


def test_model_deepcopy(model_fixture):
    model = copy.deepcopy(model_fixture)

    assert model is not model_fixture, "Copy returns a reference."

    for field in model_fixture:
        assert_allclose(model[field], model_fixture[field], 1e-15)


def test_model_copy(model_fixture):
    with pytest.raises(RuntimeError):
        copy.copy(model_fixture)
