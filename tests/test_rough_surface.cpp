/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "isopowerlaw.hh"
#include "model_factory.hh"
#include "polonsky_keer_rey.hh"
#include "surface_generator_filter.hh"
#include "tamaas.hh"
/* -------------------------------------------------------------------------- */

using namespace tamaas;

int main() {
  /// Surface parameters
  UInt grid_size = 512;
  const UInt k0 = 4, k1 = 4, k2 = 32;
  const Real hurst = 0.8;

  /// Surface generation (2D)
  SurfaceGeneratorFilter<2> sg;
  sg.setSizes({grid_size, grid_size});
  sg.setRandomSeed(1);

  auto iso{std::make_unique<Isopowerlaw<2>>()};
  iso->setQ0(k0);
  iso->setQ1(k1);
  iso->setQ2(k2);
  iso->setHurst(hurst);

  sg.setSpectrum(std::move(iso));

  std::cout << "Building rough surface with properties\n"
            << "    Q0 = " << k0 << '\n'
            << "    Q1 = " << k1 << '\n'
            << "    Q2 = " << k2 << '\n'
            << "    Hurst = " << hurst << '\n'
            << "    size = " << grid_size << 'x' << grid_size << "\n";

  /// Making surface
  auto& grid = sg.buildSurface();

  std::cout << "Creating model" << "\n";

  /// Model initialization
  auto model{ModelFactory::createModel(model_type::basic_2d, {1., 1.},
                                       {grid_size, grid_size})};

  Real load = 0.1;
  Real precision = 1e-12;

  std::cout << "Solving contact problem" << "\n";

  /// Solve normal contact problem
  PolonskyKeerRey solver{*model, grid, precision, PolonskyKeerRey::pressure,
                         PolonskyKeerRey::pressure};

  solver.solve(load);

  std::cout << "Finished" << "\n";
  return 0;
}
