# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-20 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import tamaas as tm

from numpy.testing import assert_allclose
from tamaas.dumpers import NumpyDumper
from tamaas._tamaas import _DFSANESolver as CXXSolver


def test_restart_plasticity(patch_isotropic_plasticity, tmp_path):
    model = patch_isotropic_plasticity.model
    residual = patch_isotropic_plasticity.residual

    applied_pressure = 0.1

    solver = CXXSolver(residual)
    solver.tolerance = 1e-15
    pressure = model['traction'][..., 2]
    pressure[:] = applied_pressure

    solver.solve()
    solver.updateState()

    path = tmp_path / 'restart'
    NumpyDumper(path, all_fields=True, mkdir=False) << model

    new_model = NumpyDumper.read(
        path.parent / "restart_0000.npz"
    )

    mat = tm.materials.IsotropicHardening(model, sigma_y=1e-2, hardening=0.1)
    tm.Residual(model, mat)
    CXXSolver(residual)

    for key in new_model:
        print(key)
        assert_allclose((model[key] - new_model[key]), 0, atol=3e-15)
