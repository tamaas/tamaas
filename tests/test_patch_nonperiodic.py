# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import division, print_function

import tamaas as tm
import numpy as np

from numpy.linalg import norm


def boussinesq(x, y, a, b):
    plus_sqrt = np.sqrt((x + a)**2 + (y + b)**2)
    minu_sqrt = np.sqrt((x - a)**2 + (y - b)**2)
    pm_sqrt = np.sqrt((x + a)**2 + (y - b)**2)
    mp_sqrt = np.sqrt((x - a)**2 + (y + b)**2)
    return 1 / np.pi * (
        + (x + a) * np.log((y + b + plus_sqrt) / (y - b + pm_sqrt))
        + (y + b) * np.log((x + a + plus_sqrt) / (x - a + mp_sqrt))
        + (x - a) * np.log((y - b + minu_sqrt) / (y + b + mp_sqrt))
        + (y - b) * np.log((x - a + minu_sqrt) / (x + a + pm_sqrt))
    )


def test_patch_westergaard():
    m = tm.Model(tm.model_type.basic_2d, [1, 1], [8, 8])
    tm.ModelFactory.registerNonPeriodic(m, 'dcfft')

    Nx, Ny = m.shape
    Lx, Ly = m.system_size
    dx, dy = Lx / Nx, Ly / Ny

    # Adjust for pixel size
    x = np.linspace(-Lx / 2 + dx/2, Lx / 2 - dx/2, Nx, endpoint=True)
    y = np.linspace(-Ly / 2 + dy/2, Ly / 2 - dy/2, Nx, endpoint=True)
    x, y = np.meshgrid(x, y, indexing='ij')

    m.traction[(np.abs(x) <= Lx / 4) & (np.abs(y) <= Ly / 4)] = 1

    reference = boussinesq(x, y, Lx / 4, Ly / 4)

    m.operators['dcfft'](m.traction, m.displacement)

    if False:
        import matplotlib.pyplot as plt
        plt.imshow(m.traction)
        Z1 = np.add.outer(range(8), range(8)) % 2  # chessboard
        plt.imshow(Z1, alpha=.1)
        plt.figure()
        plt.imshow(reference)
        plt.figure()
        plt.imshow(m.displacement)
        plt.figure()
        plt.imshow(m.displacement - reference)
        plt.show()

    assert norm(reference - m.displacement) / norm(reference) < 1e-15
