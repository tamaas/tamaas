#!/usr/bin/env bash
set -euxo pipefail

cd $CI_PROJECT_DIR

mkdir -p dist/wheels
mkdir -p dist/wheelhouse

PLAT=manylinux_2_28_x86_64

# Checking all version of CPython
for PYROOT in /opt/python/cp*; do
    MAJOR=$($PYROOT/bin/python --version 2>&1 | cut -d " " -f 2 | cut -d . -f 1)
    MINOR=$($PYROOT/bin/python --version 2>&1 | cut -d " " -f 2 | cut -d . -f 2)
    TAG=$(basename $PYROOT | cut -d "-" -f 1)

    # Skip Python 2
    [ $MAJOR -eq 2 ] && continue
    # Skip Beta Python 3 versions
    [ $MAJOR -eq 3 ] && [ $MINOR -ge 14 ] && continue

    # Compile
    scons -j4 \
          py_exec="$PYROOT/bin/python$MAJOR" \
          backend=cpp \
          fftw_threads=none \
          build_static_lib=True \
          use_mpi=False \
          strip_info=True \
          verbose=True \
          THRUST_ROOT=$THRUST_ROOT \
          FFTW_ROOT=$FFTW_ROOT \
          CXXFLAGS="-fvisibility=hidden" \
        2>&1 | tee compile.log

    if [ $? -ne 0 ]; then
        cat config.log
    fi

    # Link fftw statically
    static_command=$(grep 'build-release/python/tamaas/_tamaas' compile.log \
                         | sed -e 's/lfftw3/l:libfftw3.a/' \
                         | tr -d '$()')
    $static_command

    # Create wheel
    pushd build-release/python
    $PYROOT/bin/python \
        -m pip wheel \
        --wheel-dir ../../dist/wheels \
        --build-option=--python-tag=$TAG --build-option=--plat-name=$PLAT \
        --no-deps .
    popd

    # For Python 3.6 a generic wheel is built
    for wheel in dist/wheels/*-py3-none-any.whl; do
        renamed=${wheel/py3-none-any/${TAG}-none-${PLAT}}
        [ -f $wheel ] && mv $wheel $renamed
    done

    # Clean to avoid errors (ccache should still speedup)
    scons -c
    rm -rf build-release/
done

for wheel in dist/wheels/*.whl; do
    auditwheel show $wheel
    auditwheel repair $wheel -w dist/wheelhouse
done
