FROM nvidia/cuda:11.3.0-devel-ubuntu20.04

MAINTAINER Lucas Frérot

# Setting up timezone
ENV TZ=America/New_York
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get -qq update && apt-get install -y \
    g++ \
    git \
    libboost-dev \
    libfftw3-dev \
    libgtest-dev \
    pybind11-dev \
    python3-dev \
    python3-pip \
    python3-numpy \
    python3-scipy \
    python3-h5py \
    python3-netcdf4 \
    python3-sphinx \
    python3-breathe \
    python3-sphinx-rtd-theme \
    doxygen \
    scons \
  && rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install pytest uvw

ENV CUDA_ROOT=/usr/local/cuda
ENV THRUST_ROOT=/usr/local/cuda
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8