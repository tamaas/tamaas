#!/usr/bin/env python3

# Copyright 2022 Lucas Frérot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Create a new version draft for a Zenodo record."""

__author__ = "Lucas Frérot"
__copyright__ = (
    "Copyright (©) 2022, Lucas Frérot"
)
__license__ = "SPDX-License-Identifier: MIT"

import requests
import argparse
import sys

from datetime import date


def validate(request, ok=requests.codes.ok):
    """Validate request result."""
    if request.status_code != ok:
        sys.exit(f'{request.reason}: {request.json()["message"]}')

    return request


def get_latest_draft(latest, access):
    """Get draft url and data."""
    r = validate(requests.get(latest, params=access))

    if 'latest_draft' not in r.json()['links']:
        r = validate(
            requests.post(r.json()['links']['newversion'], params=access),
            ok=201,
        )

    draft = r.json()['links']['latest_draft']
    return draft, validate(requests.get(draft, params=access)).json()


parser = argparse.ArgumentParser(
    description="Upload new files and update version to Zenodo record"
)

# Required metadata
parser.add_argument('--record', help="Zenodo record id (any version)", type=int,
                    required=True)
parser.add_argument('--token', help="Zenodo API token", required=True)
parser.add_argument('--version', help="New version tag", required=True)

# Zenodo options
parser.add_argument('--sandbox', action="store_true",
                    help="Use Zenodo's sandbox domain")
parser.add_argument('--publish', action="store_true",
                    help="Publish new version")

parser.add_argument('files', nargs="*", help="Files to upload",
                    type=argparse.FileType('rb'))

args = parser.parse_args()
access = {
    "access_token": args.token
}

url, data = get_latest_draft(
    f"https://{'sandbox.' if args.sandbox else ''}zenodo.org"
    f"/api/deposit/depositions/{args.record}",
    access,
)

# Updating version and publication date
data['metadata']['version'] = args.version
data['metadata']['publication_date'] = date.today().isoformat()
validate(requests.put(url, params=access, json=data))

# Removing old files
for file in data['files']:
    validate(requests.delete(file['links']['self'], params=access), ok=204)

# Uploading new files
for fd in args.files:
    bucket = f"{data['links']['bucket']}/{fd.name}"
    validate(requests.put(bucket, data=fd, params=access))

# Publish draft
if args.publish:
    validate(requests.post(data['links']['publish'], params=access))
