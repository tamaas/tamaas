/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "static_types.hh"
#include "test.hh"

using namespace tamaas;

/* -------------------------------------------------------------------------- */

TEST(TestStaticTypes, GEMV) {
  Matrix<UInt, 3, 3> A{{1, 2, 3, 4, 5, 6, 7, 8, 9}};
  UInt b_[] = {1, 2, 3};
  VectorProxy<UInt, 3> b{b_};

  auto res = A * b;
  std::vector<UInt> solution{14, 32, 50};
  ASSERT_TRUE(compare(res, solution)) << "Matrix multiplication fail";
}

TEST(TestStaticTypes, Dot) {
  UInt a_[] = {2, 3};
  UInt b_[] = {1, 5};

  VectorProxy<UInt, 2> a{a_[0]}, b{b_[0]};
  ASSERT_EQ(a.dot(b), 17) << "Dot product fail";
}

TEST(TestStaticTypes, Norm) {
  Real a_[] = {3., 4.};
  VectorProxy<Real, 2> a{a_[0]};
  ASSERT_DOUBLE_EQ(a.l2norm(), 5) << "L2 norm fail";
}

TEST(TestStaticTypes, Symmetrization2D) {
  Matrix<Real, 2, 2> A{{1, 2, 3, 4}};
  Vector<Real, 3> Av{{1, 4, std::sqrt(2) * 2.5}};
  SymMatrix<Real, 2> As{};
  As.symmetrize(A);
  ASSERT_TRUE(compare(Av, As, AreFloatEqual())) << "Symmetrization fail";

  Matrix<Real, 2, 2> B{}, Bref{{1, 2.5, 2.5, 4}};
  B.fromSymmetric(As);
  ASSERT_TRUE(compare(Bref, B, AreFloatEqual())) << "Dense construction fail";
}

TEST(TestStaticTypes, Symmetrization3D) {
  Matrix<Real, 3, 3> A{{1, 2, 3, 4, 5, 6, 7, 8, 9}};
  Vector<Real, 6> Av{
      {1, 5, 9, std::sqrt(2) * 3, std::sqrt(2) * 5, std::sqrt(2) * 7}};
  SymMatrix<Real, 3> As{};
  As.symmetrize(A);
  ASSERT_TRUE(compare(Av, As, AreFloatEqual())) << "Symmetrization fail";

  Matrix<Real, 3, 3> B{}, Bref{{1, 3, 5, 3, 5, 7, 5, 7, 9}};
  B.fromSymmetric(As);
  ASSERT_TRUE(compare(Bref, B, AreFloatEqual())) << "Dense construction fail";
}

TEST(TestStaticTypes, Invariants) {
  SymMatrix<Real, 3> Av{
      {1, 5, 9, std::sqrt(2) * 4, std::sqrt(2) * 5, std::sqrt(2) * 7}};
  auto invar = invariants(Av);
  Matrix<Real, 3, 3> B{};
  B.fromSymmetric(Av);
  auto B2 = B * B;

  Vector<Real, 3> sol;
  sol(0) = B.trace();
  sol(1) = 0.5 * (B.trace() * B.trace() - B2.trace());
  sol(2) = 7;

  ASSERT_TRUE(compare(invar, sol, AreFloatEqual{1e-14})) << "Invariants fail";
}

TEST(TestStaticTypes, Eigenvalues) {
  SymMatrix<Real, 3> Av{{2, 3, 9, 0, 0, std::sqrt(2) * 4}};
  auto eigen = eigenvalues(Av);

  // invariants
  Vector<Real, 3> sol;
  sol(0) = 1;
  sol(1) = 2;
  sol(2) = 11;

  ASSERT_TRUE(compare(eigen, sol, AreFloatEqual{1e-14}))
      << "Eigenvalues do not reproduce invariants";
}
