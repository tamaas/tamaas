# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import tamaas as tm
from tamaas.utils import publications


def test_publications():
    gen = tm.SurfaceGeneratorRandomPhase2D()  # noqa
    model = tm.Model(tm.model_type.basic_2d, [2, 2], [2, 2])
    solver = tm.PolonskyKeerRey(model, model.traction, 1e-12)  # noqa
    assert len(publications()) == 5, "Wrong number of publications"

    gen2 = tm.SurfaceGeneratorFilter2D()  # noqa

    assert len(publications()) == 6, "Wrong number of publications"


if __name__ == "__main__":
    test_publications()
