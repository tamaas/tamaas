# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import division

__author__ = "Lucas Frérot"

import numpy as np

from numpy.fft import fftfreq


def frequencies1D(frequencies):
    n = len(frequencies)
    frequencies[:] = n * fftfreq(n)


def assign2DFrequencies(qx, qy, frequencies):
    qx, qy = np.meshgrid(qx, qy, indexing='ij')

    frequencies[:, :, 0] = qx
    frequencies[:, :, 1] = qy


def frequencies2D(frequencies):
    n = frequencies.shape
    qx = fftfreq(n[0]) * n[0]
    qy = fftfreq(n[1]) * n[1]
    assign2DFrequencies(qx, qy, frequencies)


def hfrequencies2D(frequencies):
    n = frequencies.shape
    qx = fftfreq(n[0]) * n[0]
    qy = np.arange(n[1])
    assign2DFrequencies(qx, qy, frequencies)
