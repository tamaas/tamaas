# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import division, print_function

import pytest
import tamaas as tm

from numpy.linalg import norm
from tamaas._tamaas import _DFSANESolver as NLSolver


@pytest.fixture(scope="module",
                params=[tm.EPICSolver, tm.AndersonMixing],
                ids=["EPIC", "Anderson"])
def model_setup(hertz_coarse, request):
    models, solvers = [], []

    for mtype in [tm.model_type.basic_2d, tm.model_type.volume_2d]:
        dim = tm.type_traits[mtype].dimension
        n = [hertz_coarse.n] * dim
        d = [hertz_coarse.domain_size] * dim

        if dim == 3:
            n[0] = 2
            d[0] = 0.001

        model = tm.ModelFactory.createModel(mtype, d, n)
        model.E, model.nu = hertz_coarse.e_star, 0

        solver = tm.PolonskyKeerRey(model, hertz_coarse.surface, 1e-12)

        models.append(model)
        solvers.append(solver)

    mat = tm.materials.LinearElastic(model, 'hooke')
    epsolver = NLSolver(tm.Residual(model, mat))

    solvers[1] = request.param(solvers[1], epsolver, 1e-12)

    for solver in solvers:
        solver.solve(hertz_coarse.load)

    return models, hertz_coarse


@pytest.mark.xfail(tm.TamaasInfo.backend == 'tbb',
                   reason='TBB reductions are unstable?')
def test_energy(model_setup):
    """Test the full elastic-plastic solve step in elasticity"""
    # We use computed values from basic PKR to test
    (model_elastic, model_plastic), hertz = model_setup

    error = norm(
        (model_plastic.traction[..., 2] - model_elastic.traction) *
        (model_plastic.displacement[0, ..., 2] - model_elastic.displacement))
    error /= norm(hertz.pressure * hertz.displacement)
    assert error < 1e-16
