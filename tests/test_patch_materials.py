# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-20 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
import pytest
import tamaas as tm
import numpy as np

from subprocess import run
from numpy.testing import assert_allclose
from tamaas._tamaas import _DFSANESolver as CXXSolver


solvers = [CXXSolver]

try:
    from tamaas.nonlinear_solvers import \
        DFSANESolver as PySolver, \
        NewtonKrylovSolver, \
        NewtonRaphsonSolver

    solvers += [
        NewtonKrylovSolver,
        PySolver,
        NewtonRaphsonSolver,
    ]

except ImportError:
    pass

if tm.TamaasInfo.has_petsc:
    from tamaas.nonlinear_solvers import PETScSolver
    solvers.append(PETScSolver)


@pytest.mark.parametrize('solvers', solvers)
def test_patch_plasticity(patch_isotropic_plasticity, solvers):
    "Test analyitical solution of 1d plasticity"
    tm.set_log_level(tm.LogLevel.info)
    Solver = solvers
    model = patch_isotropic_plasticity.model
    residual = patch_isotropic_plasticity.residual

    applied_pressure = 0.1

    solver = Solver(residual)
    solver.tolerance = 1e-15
    pressure = model['traction'][..., 2]
    pressure[:] = applied_pressure

    solver.solve()
    solver.updateState()

    solution, normal = patch_isotropic_plasticity.solution(applied_pressure)

    for key in solution:
        assert_allclose(
            (model[key] - solution[key]) / normal[key], 0, atol=3e-15)


def test_patch_elasticity():
    model = tm.Model(tm.model_type.volume_2d, [1, 1, 1], [1, 1, 1])
    mat = tm.materials.LinearElastic(model, 'hooke')

    gen = np.random.default_rng()
    strain = np.zeros([1, 1, 1, 6])
    strain[:] = gen.uniform(-1, 1, size=strain.shape)
    strain_inc = gen.uniform(-1, 1, size=strain.shape)
    stress = np.zeros_like(strain)
    ref = np.zeros_like(strain)

    mat.computeStress(stress, strain, strain_inc)
    model.operators['hooke'](strain + strain_inc, ref)

    np.testing.assert_allclose(stress, ref, rtol=1e-15)


def test_mfront_plasticity(patch_isotropic_plasticity):
    me_path = os.path.realpath(__file__)
    current_dir = os.path.dirname(me_path)
    mfront_file = \
        os.path.join(current_dir, "IsotropicLinearHardeningPlasticity.mfront")

    # Get location of tfel dir for rpath
    res = run("tfel-config --library-path".split(),
              capture_output=True, shell=False)

    # Update environment with rpath link flag
    mfront_env = os.environ.copy()
    mfront_env.update(**{
        "LDFLAGS": "-Wl,-rpath={}".format(res.stdout.decode('utf-8').strip())
    })

    # Build material law
    command = f"mfront --interface=generic --obuild {mfront_file}".split()
    res = run(command,
              capture_output=True,
              cwd=current_dir,
              env=mfront_env,
              shell=False)

    if res.returncode != 0:
        for out in [res.stdout, res.stderr]:
            print(out.decode('utf-8'), file=sys.stderr)
        pytest.skip('mfront build of material law failed')

    mgis_bv = pytest.importorskip("mgis.behaviour")

    model = patch_isotropic_plasticity.model
    E_h = patch_isotropic_plasticity.E_h
    sigma_y = patch_isotropic_plasticity.sigma_y

    behaviour = mgis_bv.load(
        os.path.join(current_dir, 'src/libBehaviour.so'),
        'IsotropicLinearHardeningPlasticity',
        mgis_bv.Hypothesis.Tridimensional,
    )

    behaviour.setParameter("YoungModulus", model.E)
    behaviour.setParameter("PoissonRatio", model.nu)
    behaviour.setParameter("YieldStrength", sigma_y)
    behaviour.setParameter("HardeningSlope", E_h)

    mat = tm.materials.MFrontMaterial(model, behaviour)
    tm_mat = patch_isotropic_plasticity.mat

    generator = np.random.default_rng(12345)
    strain = generator.uniform(-1, 1, size=model['strain'].shape)
    zero = np.zeros_like(strain)
    stress = np.zeros_like(strain)
    tm_stress = np.zeros_like(strain)

    tm_mat.computeStress(tm_stress, zero, strain)
    mat.computeStress(stress, zero, strain)

    np.testing.assert_allclose(tm_stress, stress, rtol=4e-14)
