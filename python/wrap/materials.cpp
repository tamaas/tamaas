/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "materials/isotropic_hardening.hh"
#include "materials/linear_elastic.hh"
#include "materials/material.hh"
#include "wrap.hh"
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace tamaas {
namespace wrap {
using namespace py::literals;

class PyMaterial : public Material {
  using Field = typename Material::Field;

public:
  using Material::Material;

  void computeStress(Field& stress, const Field& strain,
                     const Field& strain_increment) override {
    // NOLINTNEXTLINE(readability-else-after-return)
    PYBIND11_OVERLOAD_PURE(void, Material, computeStress, stress, strain,
                           strain_increment);
  }

  void computeEigenStress(Field& stress, const Field& strain,
                          const Field& strain_increment) override {
    // NOLINTNEXTLINE(readability-else-after-return)
    PYBIND11_OVERLOAD_PURE(void, Material, computeEigenStress, stress, strain,
                           strain_increment);
  }

  void update() override {
    // NOLINTNEXTLINE(readability-else-after-return)
    PYBIND11_OVERRIDE_PURE(void, Material, update);
  }
};

void wrapMaterialInterface(py::module& mod) {
  py::class_<Material, PyMaterial, std::shared_ptr<Material>>(mod, "Material")
      .def(py::init<Model*>(), py::keep_alive<1, 2>())
      .def("computeStress", &Material::computeStress, "stress"_a, "strain"_a,
           "strain_increment"_a,
           "Compute the total stress from the total strain and total strain "
           "increment")
      .def("computeEigenStress", &Material::computeEigenStress, "stress"_a,
           "strain"_a, "strain_increment"_a,
           "Compute the stress from the eigen strain (e.g. plastic strain "
           "increment)")
      .def("update", &Material::update);
}

void wrapIsotropicHardening(py::module& mod) {
  py::class_<IsotropicHardening, Material, std::shared_ptr<IsotropicHardening>>(
      mod, "IsotropicHardening")
      .def(py::init<Model*, Real, Real>(), "model"_a, "sigma_y"_a,
           "hardening"_a, py::keep_alive<1, 2>(),
           R"-(Create an isotropic linear hardening material.
:param model: the model on which to define the material
:param sigma_y: the (von Mises) yield stress
:param hardening: the hardening modulus)-")
      .def("computeInelasticDeformationIncrement",
           &IsotropicHardening::computeInelasticDeformationIncrement,
           "plastic_strain_increment"_a, "strain"_a, "strain_increment"_a,
           "Compute the plastic strain increment");

  py::class_<LinearElastic, Material, std::shared_ptr<LinearElastic>>(
      mod, "LinearElastic")
      .def(py::init<Model*, std::string>(), "model"_a, "operator_name"_a,
           py::keep_alive<1, 2>(),
           "Create a linear elastic material from a linear operator registered "
           "with a model.");
}

void wrapMaterials(py::module& m) {
  auto mod = m.def_submodule("_materials");
  mod.doc() = "Module defining classes that implement constitutive relations";
  wrapMaterialInterface(mod);
  wrapIsotropicHardening(mod);
}
}  // namespace wrap
}  // namespace tamaas
