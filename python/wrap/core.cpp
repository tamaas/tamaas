/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "logger.hh"
#include "statistics.hh"
#include "wrap.hh"
#include <pybind11/stl.h>
/* -------------------------------------------------------------------------- */

namespace tamaas {

/* -------------------------------------------------------------------------- */
namespace wrap {

using namespace py::literals;

/* -------------------------------------------------------------------------- */
template <UInt dim>
void wrapStatistics(py::module& mod) {
  auto name = makeDimensionName("Statistics", dim);
  py::class_<Statistics<dim>>(mod, name.c_str())
      .def_static("computePowerSpectrum",
                  &Statistics<dim>::computePowerSpectrum,
                  py::return_value_policy::move, "Compute PSD of surface")
      .def_static(
          "computeAutocorrelation", &Statistics<dim>::computeAutocorrelation,
          py::return_value_policy::move, "Compute autocorrelation of surface")
      .def_static("computeMoments", &Statistics<dim>::computeMoments,
                  "Compute spectral moments")
      .def_static("computeSpectralRMSSlope",
                  &Statistics<dim>::computeSpectralRMSSlope,
                  "Compute hrms' in Fourier space")
      .def_static("computeFDRMSSlope", &Statistics<dim>::computeFDRMSSlope,
                  "Compute hrms' with finite differences")
      .def_static("computeRMSHeights", &Statistics<dim>::computeRMSHeights,
                  "Compute hrms")
      .def_static("contact", &Statistics<dim>::contact, "tractions"_a,
                  "perimeter"_a = 0,
                  "Compute the (corrected) contact area. Permieter is the "
                  "total contact perimeter in number of segments.")
      .def_static("graphArea", &Statistics<dim>::graphArea, "zdisplacement"_a,
                  "Compute area defined by function graph.");
}

/* -------------------------------------------------------------------------- */
void wrapCore(py::module& mod) {
  // Exposing logging facility
  py::enum_<LogLevel>(mod, "LogLevel")
      .value("debug", LogLevel::debug)
      .value("info", LogLevel::info)
      .value("warning", LogLevel::warning)
      .value("error", LogLevel::error);
  mod.def("set_log_level", [](LogLevel level) { Logger::setLevel(level); });
  mod.def("get_log_level", Logger::getCurrentLevel);

  py::class_<Logger>(mod, "Logger")
      .def(py::init<>())
      .def(
          "get",
          [](Logger& logger, LogLevel level) -> Logger& {
            logger.get(level);
            return logger;
          },
          "Get a logger object for a log level")
      .def(
          "__lshift__",
          [](Logger& logger, std::string msg) -> Logger& {
            auto level = logger.getWishLevel();
            if (level >= Logger::getCurrentLevel() and
                not(mpi::rank() != 0 and level == LogLevel::info)) {
              py::print(msg,
                        "file"_a = py::module::import("sys").attr("stderr"));
            }
            return logger;
          },
          "Log a message");

  wrapStatistics<1>(mod);
  wrapStatistics<2>(mod);

  mod.def(
      "to_voigt",
      [](const Grid<Real, 3>& field) {
        TAMAAS_DEPRECATE("tamaas.to_voigt()", "tamaas.compute.to_voigt()");

        if (field.getNbComponents() == 9) {
          Grid<Real, 3> voigt(field.sizes(), 6);
          Loop::loop([] CUDA_LAMBDA(
                         MatrixProxy<const Real, 3, 3> in,
                         SymMatrixProxy<Real, 3> out) { out.symmetrize(in); },
                     range<MatrixProxy<const Real, 3, 3>>(field),
                     range<SymMatrixProxy<Real, 3>>(voigt));
          return voigt;
        }

        throw std::invalid_argument{
            TAMAAS_MSG("Wrong number of components to symmetrize")};
      },
      "Convert a 3D tensor field to voigt notation",
      py::return_value_policy::copy);
}

}  // namespace wrap
/* -------------------------------------------------------------------------- */
}  // namespace tamaas
