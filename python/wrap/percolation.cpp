/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "flood_fill.hh"
#include "wrap.hh"
/* -------------------------------------------------------------------------- */
#include <pybind11/stl.h>
/* -------------------------------------------------------------------------- */

namespace tamaas {

namespace wrap {

/* -------------------------------------------------------------------------- */
using namespace py::literals;

/* -------------------------------------------------------------------------- */
template <UInt dim>
void wrapCluster(py::module& mod) {
  auto name = makeDimensionName("Cluster", dim);
  py::class_<Cluster<dim>>(mod, name.c_str())
      .def(py::init<>())
      .def_property_readonly("area", &Cluster<dim>::getArea, "Area of cluster")
      .def_property_readonly("points", &Cluster<dim>::getPoints,
                             "Get list of points of cluster")
      .def_property_readonly("perimeter", &Cluster<dim>::getPerimeter,
                             "Get perimeter of cluster")
      .def_property_readonly("bounding_box", &Cluster<dim>::boundingBox,
                             "Compute the bounding box of a cluster")
      .def_property_readonly("extent", &Cluster<dim>::extent,
                             "Compute the extents of a cluster")

      .def(TAMAAS_DEPRECATE_ACCESSOR(getArea, Cluster<dim>, "area"))
      .def(TAMAAS_DEPRECATE_ACCESSOR(getPoints, Cluster<dim>, "points"))
      .def(TAMAAS_DEPRECATE_ACCESSOR(getPerimeter, Cluster<dim>, "perimeter"))

      .def("__str__", [](const Cluster<dim>& cluster) {
        std::stringstream sstr;
        sstr << '{';
        auto&& points = cluster.getPoints();
        auto print_point = [&sstr](const auto& point) {
          sstr << '(';
          for (UInt i = 0; i < point.size() - 1; ++i)
            sstr << point[i] << ", ";
          sstr << point.back() << ')';
        };

        auto point_it = points.begin();
        for (UInt p = 0; p < points.size() - 1; ++p) {
          print_point(*(point_it++));
          sstr << ", ";
        }
        print_point(points.back());
        sstr << "}";
        return sstr.str();
      });
}

void wrapPercolation(py::module& mod) {
  wrapCluster<1>(mod);
  wrapCluster<2>(mod);
  wrapCluster<3>(mod);

  py::class_<FloodFill>(mod, "FloodFill")
      .def_static("getSegments", &FloodFill::getSegments,
                  "Return a list of segments from boolean map", "contact"_a)
      .def_static("getClusters", &FloodFill::getClusters,
                  "Return a list of clusters from boolean map", "contact"_a,
                  "diagonal"_a)
      .def_static("getVolumes", &FloodFill::getVolumes,
                  "Return a list of volume clusters", "map"_a, "diagonal"_a);
}

}  // namespace wrap

}  // namespace tamaas
