/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef WRAP_HH
#define WRAP_HH
/* -------------------------------------------------------------------------- */
#include "cast.hh"
#include "numpy.hh"
#include "tamaas.hh"
/* -------------------------------------------------------------------------- */
#include <pybind11/pybind11.h>
/* -------------------------------------------------------------------------- */

namespace tamaas {

namespace wrap {

namespace py = pybind11;

/// For naming classes templated with dimension
inline std::string makeDimensionName(const std::string& name, UInt dim) {
  std::stringstream str;
  str << name << dim << "D";
  return str.str();
}

/* -------------------------------------------------------------------------- */
/* Prototypes for wrapping of tamaas components */
/* -------------------------------------------------------------------------- */
void wrapCore(py::module& mod);
void wrapPercolation(py::module& mod);
void wrapSurface(py::module& mod);
void wrapModel(py::module& mod);
void wrapSolvers(py::module& mod);
void wrapTestFeatures(py::module& mod);
void wrapCompute(py::module& mod);
void wrapMPI(py::module& mod);
void wrapMaterials(py::module& m);

/* -------------------------------------------------------------------------- */
/* Deprecation macro */
/* -------------------------------------------------------------------------- */
#define TAMAAS_DEPRECATE(olds, news)                                           \
  do {                                                                         \
    PyErr_WarnEx(PyExc_DeprecationWarning,                                     \
                 olds " is deprecated, use " news " instead.", 1);             \
  } while (0)

#define TAMAAS_DEPRECATE_ACCESSOR(acc, type, property)                         \
#acc, [](const type& m) -> decltype(m.acc()) {                       \
    TAMAAS_DEPRECATE(#acc "()", "the " property " property");                  \
    return m.acc();                                                            \
  }

}  // namespace wrap

}  // namespace tamaas

#endif  // WRAP_HH
