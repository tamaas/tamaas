FROM debian:bullseye-slim

## Singularity build steps:
## $ docker build -t tamaas --build-arg USE_MPI=True [--build-arg ...] .
## $ docker save <image_id> -o tamaas.tar
## $ singularity build tamaas.sif docker-archive://tamaas.tar

MAINTAINER Lucas Frérot

# Install build and runtime dependencies
RUN apt-get -qq update && apt-get install -qq -y \
    g++ \
    git \
    libboost-dev \
    libthrust-dev \
    libfftw3-dev \
    libfftw3-mpi-dev \
    libopenmpi-dev \
    pybind11-dev \
    python3-dev \
    python3-numpy \
    python3-pip \
    python3-scipy \
    python3-h5py \
    python3-netcdf4 \
    python3-mpi4py \
    python3-h5py-mpi \
    scons \
  && rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install --prefix=/usr/local uvw

# Basic setup
RUN mkdir -p /src/tamaas
COPY . /src/tamaas
RUN rm -rf /src/tamaas/build-* /src/tamaas/.scon*

# Build arguments
ARG BACKEND=cpp
ARG FFTW_THREADS=none
ARG USE_MPI=False

# Compiling and installing
RUN cd /src/tamaas \
  && scons \
       backend=$BACKEND \
       fftw_threads=$FFTW_THREADS \
       use_mpi=$USE_MPI \
       verbose=true \
       prefix=/usr/local \
  && scons install \
  && cd / && rm -rf /src/tamaas

# Do not use modules in ~/.local
# Singularity mounts the entire $HOME by default which conflicts if Tamaas
# is already installed there
ENV PYTHONNOUSERSITE=1
