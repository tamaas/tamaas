pipeline {
    parameters {string(defaultValue: '', description: 'api-token', name: 'API_TOKEN')
                string(defaultValue: '', description: 'Token for readthedocs', name: 'RTD_TOKEN')
                string(defaultValue: '', description: 'buildable phid', name: 'BUILD_TARGET_PHID')
                string(defaultValue: '', description: 'Commit id', name: 'COMMIT_ID')
                string(defaultValue: '', description: 'Diff id', name: 'DIFF_ID')
                string(defaultValue: 'PHID-PROJ-gbo56hpf2y5bi7t5jusk', description: 'ID of the project', name: 'PROJECT_ID')
    }

    environment {
        PHABRICATOR_HOST = 'https://c4science.ch/api/'
        PYTHONPATH = sh returnStdout: true, script: 'echo ${WORKSPACE}/build-release/python:${WORKSPACE}/tests/ci/script/'
    }

    agent {dockerfile {additionalBuildArgs '--tag tamaas-environment'}}
    options {timeout(time: 1, unit: 'HOURS')}

    stages {
        stage('SCM Checkout') {
            steps {
                checkout scm: [
                    $class: 'GitSCM',
                    branches: scm.branches,
                    extensions: [[
                        $class: 'SubmoduleOption',
                        recursiveSubmodules: true,
                    ]],
                    userRemoteConfigs: scm.userRemoteConfigs
                ]
            }
        }

        stage('Configure') {
            steps {
                writeFile(file: build-setup.conf,
                          text:'''py_exec = \'python3\'
                                  build_python = \'true\'
                                  build_tests = \'true\'
                                  use_mpi = \'true\'
                                  verbose = \'true\'"''')
            }
        }

        stage('Compile') {
            steps {
                tee('compilation.log') {
                    sh '''set -o pipefail
                          rm -rf .sconf_temp .sconsign.dblite build-release
                          scons'''
                }
            }

            post {
                always {
                    uploadArtifact('config.log', 'Configure log')
                    uploadArtifact('compilation.log', 'Compilation log')
                }
            }
        }

        stage('Run tests') {
            steps {
                tee('tests.log') {
                    sh '''set -o pipefail
                          python3 -m pytest -vvv --durations=0 --junitxml=results.xml build-release/tests'''
                }
            }

            post {
                always {
                    uploadArtifact('tests.log', 'Tests log')

                    createArtifact("results.xml")
                    junit 'results.xml'
                    step([$class: 'XUnitBuilder',
                          thresholds: [
                            [$class: 'SkippedThreshold', failureThreshold: '0'],
                            [$class: 'FailedThreshold', failureThreshold: '0']],
                          tools: [[$class: 'GoogleTestType', pattern: 'build-release/tests/gtest_results.xml']]])
                }
            }
        }
    }

    post {
        success {
            trigger_rtd()
            passed_hbm()
        }

        failure {
            failed_hbm()
            emailext(
                body: '''${SCRIPT, template="groovy-html.template"}''',
                mimeType: 'text/html',
                subject: "[Jenkins] ${currentBuild.fullDisplayName} Failed",
                recipientProviders: [[$class: 'CulpritsRecipientProvider']],
                to: 'lfrerot1@jhu.edu',
                attachLog: true,
                compressLog: true)
        }
    }
    
}

def createArtifact(filename) {
    sh "./tests/ci/scripts/hbm send-uri -k 'Jenkins URI' -u ${BUILD_URL} -l 'View Jenkins result'"
    sh "./tests/ci/scripts/hbm send-junit-results -f ${filename}"
}

def uploadArtifact(artifact, name) {
    archiveArtifacts(artifacts: "${artifact}", fingerprint: true)
    sh "./tests/ci/scripts/hbm upload-file -f ${artifact} -n \"${name}\" -v ${PROJECT_ID}"
}

def failed_hbm() {
    sh "./tests/ci/scripts/hbm failed"
}

def passed_hbm() {
    sh "./tests/ci/scripts/hbm passed"
}

def trigger_rtd() {
    def response = httpRequest(url: "https://readthedocs.org/api/v2/webhook/tamaas/106141/",
                               httpMode: 'POST',
                               requestBody: "token=${RTD_TOKEN}")
    println("Status: " + response.status)
    println("Content: " + response.content)
}
