# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from SCons.Errors import StopError
from SCons.Script import Configure, Dir


def generate(env):
    """Sets correct environement variables for Tamaas.
    - 'TAMAAS_PATH' is the path to the build directory of Tamaas"""
    if env.GetOption("clean"):
        return

    if 'TAMAAS_PATH' not in env:
        raise StopError("Please set the 'TAMAAS_PATH' variable in your " +
                        "scons environment")

    tm_path = Dir(env['TAMAAS_PATH'])
    include_dir = Dir('src', tm_path)
    subdirs = "core model solvers bem surface percolation".split(" ")
    include_dirs = [Dir(x, include_dir).abspath for x in subdirs]
    lib_dir = include_dir

    env.AppendUnique(CPPPATH=include_dirs)
    env.AppendUnique(LIBPATH=[lib_dir.abspath])
    env.AppendUnique(RPATH=[lib_dir.abspath])
    env.AppendUnique(CXXFLAGS=["-std=c++14"])
    env.AppendUnique(
        CPPDEFINES=["THRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_OMP"])
    env.AppendUnique(LIBS=['fftw3', 'fftw3_omp'])

    conf = Configure(env)
    if not conf.CheckLibWithHeader("Tamaas", "tamaas.hh", language="CXX"):
        raise StopError(
            "Tamaas not found in {}".format(tm_path.abspath))
    env['TAMAAS_PATH'] = tm_path.abspath
    env = conf.Finish()


def exists(env):
    if 'TAMAAS_PATH' in env:
        return True
    return False
