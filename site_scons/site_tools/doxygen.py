# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
SCons tool to generate Doxygen API documentation
"""

from __future__ import print_function

from SCons.Script import Builder, Action


def generate(env):
    env['has_doxygen'] = exists(env)
    if not env['has_doxygen']:
        return

    action = 'doxygen $SOURCE'

    if env['verbose']:
        action = Action(action)
    else:
        action = Action(action,
                        cmdstr='{}[Doxygen] {}$SOURCE'.format(
                            env['COLOR_DICT']['orange'],
                            env['COLOR_DICT']['end']
                        ))

    doxygen_builder = Builder(action=action)
    env['BUILDERS']['Doxygen'] = doxygen_builder


def exists(env):
    conf = env.Configure()
    has_doxygen = conf.CheckProg('doxygen')
    conf.Finish()
    return has_doxygen
