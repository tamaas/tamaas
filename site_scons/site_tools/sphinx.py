# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
SCons tool to generate Sphinx documentation
"""

from __future__ import print_function

from SCons.Script import Builder, Action

from site_init import CheckPythonModule

def generate(env):
    env['has_sphinx'] = exists(env)
    if not env['has_sphinx']:
        return

    def sphinx_action(source, target, env, for_signature):
        args = {
            'verbose': "" if env['verbose'] else '-Q',
        }

        if 'html' in target[0].path:
            args['builder'] = 'html'
        elif env.subst('.${man_section}') in target[0].path:
            args['builder'] = 'man'
        elif '.tex' in target[0].path:
            args['builder'] = 'latex'

        action = ('sphinx-build {verbose} -b {builder}'.format(**args)
                  + ' -- ${SOURCE.dir} ${TARGET.dir}')

        if env['verbose']:
            return Action(action)

        return Action(action,
                      cmdstr='{}[Sphinx/{}] {}${{TARGET}}'.format(
                          env['COLOR_DICT']['orange'],
                          args['builder'],
                          env['COLOR_DICT']['end']
                      ))

    sphinx_build = Builder(generator=sphinx_action,
                           emitter=None)

    env['BUILDERS']['Sphinx'] = sphinx_build


def exists(env):
    conf = env.Configure(custom_tests={'CheckPythonModule': CheckPythonModule})
    has_sphinx = \
        conf.CheckProg('sphinx-build') and conf.CheckPythonModule('breathe')
    conf.Finish()
    return has_sphinx
