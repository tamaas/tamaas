.. Tamaas documentation master file, created by sphinx-quickstart on Mon Apr 29
   18:08:33 2019. You can adapt this file completely to your liking, but it
   should at least contain the root `toctree` directive.

Tamaas --- A high-performance library for periodic rough surface contact
========================================================================

.. image:: https://img.shields.io/badge/code-gitlab.com%2Ftamaas-9cf.svg
   :target: https://gitlab.com/tamaas/tamaas

.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.3479236.svg
   :target: https://doi.org/10.5281/zenodo.3479236

.. image:: https://joss.theoj.org/papers/86903c51f3c66964eef7776d8aeaf17d/status.svg
   :target: https://joss.theoj.org/papers/86903c51f3c66964eef7776d8aeaf17d

.. image:: https://mybinder.org/badge_logo.svg
   :target: https://mybinder.org/v2/gl/tamaas%2Ftutorials/HEAD

Tamaas (from تماس meaning "contact" in Arabic and Farsi) is a high-performance
rough-surface periodic contact code based on boundary and volume integral
equations. The clever mathematical formulation of the underlying numerical
methods (see e.g. :doi:`10.1007/s00466-017-1392-5` and :arxiv:`1811.11558`)
allows the use of the fast-Fourier Transform, a great help in achieving peak
performance: Tamaas is consistently :doc:`two orders of magnitude faster
<performance>` (and lighter) than traditional FEM! Tamaas is aimed at
researchers and practitioners wishing to compute realistic contact solutions for
the study of interface phenomena.

You can see Tamaas in action with our interactive :ref:`tutorials <sect-tutorials>`.

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents:

   ./overview
   ./quickstart
   ./rough_surfaces
   ./model
   ./contact
   ./mpi
   ./examples
   ./performance
   ./faq
   ./developer
   ./api_reference


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
