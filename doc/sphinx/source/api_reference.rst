API Reference
=============

Python API
----------

Tamaas root module
^^^^^^^^^^^^^^^^^^

.. automodule:: tamaas
   :no-undoc-members:

Tamaas C++ bindings
^^^^^^^^^^^^^^^^^^^

.. automodule:: tamaas._tamaas

.. autoclass:: tamaas._tamaas._DFSANESolver

.. automodule:: tamaas._tamaas.mpi

.. automodule:: tamaas._tamaas.compute

Tamaas Dumpers for :py:class:`Model <tamaas._tamaas.Model>`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: tamaas.dumpers
   :exclude-members: prefix, postfix, fields, all_fields

Tamaas Nonlinear solvers
^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: tamaas.nonlinear_solvers

Tamaas utilities
^^^^^^^^^^^^^^^^

.. automodule:: tamaas.utils

C++ API
-------

.. doxygenindex::
