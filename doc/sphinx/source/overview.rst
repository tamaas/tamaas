Library overview
================

Tamaas is mainly composed of three components:

- Random surface generation procedures
- Model state objects and operators
- Contact solving procedures

These parts are meant to be independent as well as inter-operable. The first one
provides an interface to several stochastic surface generation algorithms
described in :doc:`rough_surfaces`. The second provides an interface to a state
object :py:class:`Model <tamaas._tamaas.Model>` (and C++ class
:cpp:class:`tamaas::Model`) as well as integral operators based on the state
object (see :doc:`model`). Finally, the third part provides contact solving
routines that make use of the integral operators for performance (see
:doc:`contact`).

.. _sect-tutorials:

Tutorials
---------

The following tutorial notebooks can also be used to learn about Tamaas:

- Elastic Contact (`live notebook
  <https://mybinder.org/v2/gl/tamaas%2Ftutorials/HEAD?filepath=elastic_contact.ipynb>`_,
  `notebook viewer
  <https://gitlab.com/tamaas/tutorials/-/blob/master/elastic_contact.ipynb>`_)
- Rough Surfaces (`live notebook
  <https://mybinder.org/v2/gl/tamaas%2Ftutorials/HEAD?filepath=rough_surfaces.ipynb>`__,
  `notebook viewer
  <https://gitlab.com/tamaas/tutorials/-/blob/master/rough_surfaces.ipynb>`__)


Citations
---------

Tamaas is the fruit of a research effort. To give proper credit to its creators
and the scientists behind the methods it implements, you can use the
:py:func:`tamaas.utils.publications` function at the end of your python scripts.
This function scans global variables and prints the relevant citations for the
different capabilities of Tamaas used. Note that it may miss citations if some
objects are not explicitly stored in named variables, so please examine the
relevant publications in :doi:`10.21105/joss.02121`.

Changelog
---------

The changelog can be consulted `here <https://gitlab.com/tamaas/tamaas/-/blob/master/CHANGELOG.md>`__.


Seeking help
------------

You can ask your questions on `gitlab
<https://gitlab.com/tamaas/tamaas/-/issues>`_ using this `form
<https://gitlab.com/tamaas/tamaas/-/issues/new>`_. If you do not have an
account, you can create one `on this page <https://gitlab.com/users/sign_up>`_.


Contribution
------------

Contributions to Tamaas are welcome, whether they are code, bug, or
documentation related. Please have a read at the :ref:`dev-doc`.

Code
....

Please `fork
<https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork>`_
Tamaas and `submit your patch as a merge request
<https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#new-merge-request-from-a-fork>`_.

Bug reports
...........

You can also contribute to Tamaas by reporting any bugs you find `here
<https://gitlab.com/tamaas/tamaas/-/issues/new>`__ if you have an account on
`gitlab <https://gitlab.com>`__. Please read the :ref:`FAQ <faq>` before posting.
