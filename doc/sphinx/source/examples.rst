Examples
========

The directory ``examples/`` in Tamaas' root repository contains example scripts
dedicated to various aspects of Tamaas:

``statistics.py``
    This script generates a rough surface and computes its power spectrum
    density as well as its autocorrelation function.

``rough_contact.py``
    This script generates a rough surface and solves an adhesion-less elastic
    contact problem.

``adhesion.py``
    This script solves a rough contact problem with an exponential energy
    functional for adhesion. It also shows how to derive an energy functional in
    python.

``saturation.py``
    This script solves a saturated contact problem (i.e. pseudo-plasticity) with
    a rough surface.

``stresses.py``
    This script solves an equilibrium problem with an eigenstrain distribution
    and a surface traction distribution and writes the output to a VTK file. It
    demonstrates how the integral operators that Tamaas uses internally for
    elastic-plastic contact can be used directly in Python.

``plasticity.py``
    This script solves an elastoplastic Hertz contact problem with three load
    steps and writes the result to VTK files.

``scipy_penalty.py``
    This script shows how to implement a contact solver in python that uses the
    contact functionals of Tamaas. Here we use Scipy's
    :py:func:`scipy.optimize.minimize` function to solve a contact problem with
    penalty.

``nonperiodic.py``
    This script shows how to solve a non-periodic problem and compute the true
    surface interference
