.. _faq:

Frequently Asked Questions
==========================

Importing ``tamaas`` module gives a circular import error on Windows
--------------------------------------------------------------------

`(Similar to this issue) <https://gitlab.com/tamaas/tamaas/-/issues/11>`_
Installing with ``pip install tamaas`` does not work on Windows, as binary
distributions are only built for Linux. To use Tamaas in Windows, use the
Windows subsystem for Linux, then use ``pip`` to :ref:`install Tamaas
<pip-install>`, or use the :ref:`provided Docker images <docker-image>`.


What are the units in Tamaas?
-----------------------------

All quantities in Tamaas are unitless. To choose a consistent set of units, see
:ref:`Units in Tamaas <units>`.

Do contact solvers solve for a total force or an average pressure?
------------------------------------------------------------------

Solvers consider that the argument to the ``solve()`` method is an average
apparent pressure (or average gap in some cases), i.e. the total force divided
by the total system area (in the reference configuration). This means that
doubling the system size and solving for the same argument to the solver
*increases* the total load by a factor 4 (for a 2D surface).

``scons dev`` fails to install Tamaas with ``externally-managed-environment`` error
-----------------------------------------------------------------------------------

Pip now refuses refuses to install a package outside of virtual environments
(see `PEP 668 <https://peps.python.org/pep-0668/>`_), even with the ``--user``
flag on. This encourages the use of virtual environments, either with `venv
<https://docs.python.org/3/library/venv.html>`_ or `virtualenv
<https://virtualenv.pypa.io/en/latest/>`_, which works nicely with ``scons
dev``.

However, if one **really** needs an editable installation outside of a virtual
environment, the ``PIPFLAGS`` option can be used to circumvent pip's protections
(not recommended)::

  scons dev PIPFLAGS="--user --break-system-packages"

.. warning::
   If misused this can break your system's Python packages. Use virtual
   environments instead!
