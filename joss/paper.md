---
title: 'Tamaas: a library for elastic-plastic contact of periodic rough surfaces'
tags:
  - C++
  - Python
  - contact
  - rough surface
  - plasticity
authors:
  - name: Lucas Frérot
    orcid: 0000-0002-4138-1052
    affiliation: "1, 2"
  - name: Guillaume Anciaux
    orcid: 0000-0002-9624-5621
    affiliation: 1
  - name: Valentine Rey
    orcid: 0000-0003-1019-1819
    affiliation: 3
  - name: Son Pham-Ba
    orcid: 0000-0003-3451-7297
    affiliation: 1
  - name: Jean-François Molinari
    orcid: 0000-0002-1728-1844
    affiliation: 1
affiliations:
 - name: Civil Engineering Institute, École Polytechnique Fédérale de Lausanne, Switzerland
   index: 1
 - name: Department of Physics and Astronomy, Johns Hopkins University, Baltimore, Maryland, United States.
   index: 2
 - name: Université de Nantes Sciences et Techniques, Nantes, France
   index: 3

date: 13 December 2019
bibliography: paper.bibtex
---

# Summary

Physical phenomena that happen at solid contact interfaces, such as
friction and wear, are largely entwined with the roughness of the
surfaces in contact. For example, the fact that the friction force
between two solids in contact is independent of their apparent contact
area is due to roughness, as the solids are only in contact over a
smaller "true contact area" which only depends on the normal force 
[@archard_elastic_1957]. Roughness occurs on most man-made and natural surfaces
[@persson_nature_2005] and can span many orders of magnitude, from the
nanometer scale to the kilometer scale [@renard_constant_2013]. This
poses a serious challenge to conventional numerical approaches in
solid mechanics such as the finite-element method (FEM).

Boundary integral methods [@bonnet_boundary_1995] are commonly
employed in place of the FEM for rough elastic contact because of an
inherent dimensionality reduction: the computational effort is focused
on the contact interface whereas the FEM requires discretization of
the volume of the solids in contact. In addition, the use of a
half-space geometry provides a translational invariance: the
computation of periodic equilibrium solutions can then be accelerated
with the fast-Fourier Transform [@stanley_fftbased_1997].

However, because of the roughness, the total contact load is
distributed over a small area and local contact pressures are expected
to cause non-linear material behavior, such as plasticity. In this
case, volume integral methods can be employed to account for plastic
deformation [@telles_application_1979]. These enjoy properties
analogous to boundary integral methods and can also be accelerated
with a Fourier approach [@frerot_fourieraccelerated_2019].

Taking plasticity into account is necessary in the accurate
description of contact interfaces for the understanding of friction
and wear. Moreover, high performance implementations are needed to
model realistic rough surfaces with roughness spanning many orders of
magnitude in scale.

``Tamaas`` is a C++ library with a Python interface [@pybind11], developed in
the [Computational Solid Mechanics Laboratory](https://www.epfl.ch/labs/lsms) at
EPFL, that implements a unique Fourier-accelerated volume integral formulation
of equilibrium [@frerot_fourieraccelerated_2019] for the solution of
elastic-plastic rough contact problems. The use of C++ allows for a particular
focus on performance: most loops are parallelized using ``Thrust/OpenMP`` and
the fast-Fourier transforms are computed with ``FFTW3/OpenMP``. Thanks to this,
it can handle simulations with upwards of 100 million degrees of freedom on a
single compute node [@frerot_fourieraccelerated_2019]. ``Tamaas`` is aimed at
researchers and practitioners wishing to compute realistic contact solutions for
the study of interface phenomena.

# Features and Performance

``Tamaas`` provides access in its Python API to random rough surface
generation procedures (e.g. @hu_simulation_1992), statistical tools
(e.g. autocorrelation and power spectrum computations) and a variety
of contact algorithms:

- Normal and adhesive contact schemes based on the conjugate gradient
  [@polonsky_numerical_1999; @rey_normal_2017] and using the boundary integral
  method;
- Associated frictional contact using proximal algorithms [@condat_primal_2012];
- Elastic-plastic contact using the Fourier-accelerated volume integral method
  [@frerot_fourieraccelerated_2019] and saturated surface pressure
  [@almqvist_dry_2007].

We are not aware of any public software package providing implementation to all
of the above methods, although the web-based package
[contact.engineering](https://contact.engineering/) allows elastic normal
contact solutions using a boundary integral method as well.

``Tamaas`` also exposes in its Python API the accelerated linear
operators it uses to compute equilibrium solutions, making prototyping
new algorithms convenient.

We compare in figure 1 the scaling properties of ``Tamaas`` to a
reference high-performance C++ FEM implementation named
[``Akantu``](https://gitlab.com/akantu/akantu)
[@richart_implementation_2015] which uses the direct solver
[MUMPS](http://mumps.enseeiht.fr/). The reference problem is the
elastic equilibrium of a half-space with an isotropic spherical
inclusion [@mindlin_thermoelastic_1950], which is computed in serial
for both implementations. $N$ represents the number of points in the
computational domain. For large $N$, ``Tamaas`` is two orders of
magnitude faster than ``Akantu``.

![Scaling comparison between the acclerated volume integral method
implemented in ``Tamaas`` and a finite-element method with a direct
solver for the solution of the equilibrium of a half-space with a
spherical isotropic inclusion. $N$ is the number of points in the
computational domain. When $N=2^{18}$ ``Tamaas`` is 200 times faster
than the FEM implementation ``Akantu``.](complexity.pdf)

Figure 2 shows the sub-surface plastic zones in a rough contact
simulation, with color indicating their depth. The Fourier-accelerated
approach allows an unprecendented level of detail on the topography of
the zones which can have an influence on friction and wear
[@frerot_crack_2019].

![Sub-surface plastic zones in an elastic-plastic rough contact
simulation. Lighter shades are zones deeper below the contact
interface. The simulation used to produce this picture had more than
100 million degrees of freedom and ran on a single compute node (2
$\times$ 14 Intel Broadwell cores + 128GB RAM).](plastic_zones.png)

The following publications have been made possible with ``Tamaas``:

- @yastrebov_contact_2012
- @yastrebov_contact_2014
- @yastrebov_infinitesimal_2015
- @yastrebov_accurate_2017
- @yastrebov_role_2017
- @rey_normal_2017
- @rey_stability_2018
- @rey_quantifying_2019
- @frerot_mechanistic_2018
- @frerot_fourieraccelerated_2019
- @frerot_crack_2019
- @brink_parameter_2020

# Acknowledgements

We acknowledge the financial support of the Swiss National Science Foundation
(grants #162569 "Contact mechanics of rough surfaces" and #191720 "Tribology of
Polymers: from Atomistic to Continuum Scales").

# References
