# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic
Versioning](https://semver.org/spec/v2.0.0.html) for final versions and
[PEP440](https://www.python.org/dev/peps/pep-0440/) in case intermediate
versions need to be released (e.g. development version `2.2.3.dev1` or release
candidates `2.2.3rc1`), or individual commits are packaged.

## Unreleased

### Added

- Building wheels for Python versions 3.12 and 3.13
- Wheels platform tag is now manylinux_2_28 (due to CentOS EOL)


## v2.8.0 -- 2024-07-05

### Added

- Nonperiodic example with true interference computation
- Added a [PETSc](https://petsc.org) non-linear solver class
  (`tm.nonlinear_solvers.PETScSolver`). Activate with `scons use_petsc=true`.
  The class accepts PETSc "command-line" options, so everything from the linear
  solver to the iterative scheme can be tuned at runtime.
- Added an [MFront](https://thelfer.github.io/tfel/web/index.html) material
  class implemented in Python: `tm.materials.MFrontMaterial`. Currently only
  compatible with derivative-free solvers like DFSANE.
- Added a `LinearElastic` convenience material. A `Residual` object created with
  a `LinearElastic` material can be used to seamlessly perform a single linear
  solve step (e.g. with a PETSc solver).
- Added a viscoelastic contact solver `MaxwellViscoelastic` (!3, by @nathanlzc854774325)
- Added a [PETSc](https://petsc.org) contact solver class
  (`tm.petsc.OptimizationSolver`) that solves contact problems with the
  constrained TAO methods of PETSc. Like the non-linear solver, parameters and
  methods can be tuned at runtime.

### Fixed

- Removed some warnings on recent compilers
- MPI type-traits compilation issues with recent GCC versions
- Compilation issues with Thrust + libcudacxx
- **Breaking**: error in contact solvers is now independent of discretization.
  This means that contact solvers are likely to do more iterations for the same
  tolerance, or may not converge if tolerance was set to a value too small.
- Tests can be run without scipy
- Compiling issues with cccl/libcudacxx
- Compatibility with Python < 3.9
- Segmentation fault in surface generation with MPI (due to real coefficient indices)

### Changed

- Moved CI to Debian Bookworm (12)
- Changed default backends to host when macros are undefined
- Changed order of off-diagonal components for symmetric tensors
- Made tangential contact solvers conform to the `ContactSolver` interface
- `scons install-python` does not silently fail
- Using `manylinux_2_28` following CentOS 7 EOL (as of 2024-06-30)

## v2.7.1 -- 2023-09-20

### Fixed

- Fixed the elastic energy computation for `Isopowerlaw` when H = 0.5
- Fixed compilation with Thrust versions >= 2

## v2.7.0 -- 2023-07-03

### Added

- Added a `Material` class tree to decouple material behavior from residuals.
  The `Material` class can be derived in Python for custom material laws.
- `Model` can set the integration method for volume operators with the
  `setIntegrationMethod` method.
- `EPICSolver` can be used in `utils.load_path`
- `residual.material` gives a reference to the material object used in the residual
- Added a DC-FFT/Boussinesq operator for non-periodic elastic solutions
- Can now choose integral operator in `PolonskyKeerRey`, so non-periodic contact
  is possible!
- Python sub-classes of `IntegralOperator` are possible
- `TAMAAS_MSG` variadic macro which concatenates arguments with file, line no
  and function info
- `assertion_error`, `not_implemented_error`, `nan_error` and `model_type_error`
  classes for exceptions, which allow for finer exceptions for debugging
- SCons targets `clang-format`, `clang-tidy`, `flake8`, `mypy` for linting, and
  target alias `lint` to run all available lint tools
- Added Anderson mixing solver (see
  [10.1145/321296.321305](https://doi.org/10.1145/321296.321305) and
  [10.1006/jcph.1996.0059](https://doi.org/10.1006/jcph.1996.0059) for more
  details).
- `max_iter` property on `EPICSolver` and `DFSANESolver`
- Sequential `NewtonRaphsonSolver` using BiCGStab for linear solves
- `elasticEnergy` to `Isopowerlaw` to compute full contact energy from PSD

### Changed

- Removed `model_type` template for `Residual`: they are only defined on
  the `model_type::volume_2d` type.
- Simplified interface for `Residual`: it now takes a `Material` object at
  construction which handles the computation of eigenstresses involved in the
  residual equation
- Added function name to `TAMAAS_MSG`
- `TAMAAS_ASSERT` raises `assertion_error` and is not disabled in release
  builds: it should be used to check class invariants
- `TAMAAS_EXCEPTION` calls have been replaced by either `throw <exception>` or
  `TAMAAS_ASSERT` calls, so that specific more error types can be used instead
  of the catch-all `tamaas::Exception` type
- `DFSANESolver` does not throw if non-converged
- `scons dev` no longer forces installation in `~/.local`. Run in virtual
  environment to recover expected behavior. See
  [FAQ](https://tamaas.readthedocs.io/en/latest/faq.html) for more details.

### Fixed

- Random phases set to zero where the C2R transform expects real inputs
- Issue where large HDF5 files could not be read in parallel
- Copy constructor for `Grid<T, dim>`

### Deprecated

- `getStress()`, `setIntegrationMethod()` and `getVector()` in `Residual` are
  deprecated, use `model["stress"]`, `model.setIntegrationMethod()` and
  `residual.vector` respectively instead.
- `ModelFactor.createResidual` is deprecated. Instead, instanciate a `Material`
  object and use the `Residual` constructor to create the residual object.

### Removed

- `tamaas::Exception` class
- `TAMAAS_EXCEPTION`, `TAMAAS_DEBUG_MSG`, `TAMAAS_DEBUG_EXCEPTION` macros
- `setup.py` is no longer a viable way of installing the Tamaas python package

## v2.6.0 -- 2022-12-23

### Added

- Added `matvec` interface for the following operators:
  - `Hooke`
  - `Kelvin`
  - `Westergaard`
- Documented error handling with MPI
- Added `span<T>` class
- `FieldContainer` class that defines an interface to access fields registered
  with an object
- Operators now expose their fields, which can be modified from Python
- Added `HookeField` operator for heterogeneous elastic properties
- Documented proper unit choice
- Added `Statistics::computeFDRMSSlope()`, finite difference approximation of
  RMS of surface slopes
- Kato saturated uses L2 norm on residual displacement instead of L∞
- Request system to avoid erasing model data on restart
- Boundary field names are dumped

### Changed

- Resizing a grid no longer sets values to zero
- Wheels built for `manylinux2014` instead of `manylinux2010`
- Cleaned up `Array` interface
- Shared-memory compile options disabled by default
- Unknown build variables raise a warning
- Removed conditional branches from PKR
- NetCDF dumps in `NETCDF4` format instead of `NETCDF4_CLASSIC`
- VTK no longer writes FieldData, this was causing issues reading converted files

### Fixed

- Fixed flood fill algorithm to properly account for PBCs

## v2.5.0 -- 2022-07-05

### Added

- Added `tamaas.utils.publications` to print a list of publications relevant to
  the parts of Tamaas used in a script. Call at the end of a script for an
  exhaustive list.
- Added `tamaas.mpi.gather` and `tamaas.mpi.scatter` to help handling of 2D data
  in MPI contexts.
- Added `getBoundaryFields()/boundary_fields` function/property to `Model`.
- Added Python constructor to `Model`, which is an alias to
  `ModelFactory.createModel`.
- Added convenience `tamaas.dumpers._helper.netCDFtoParaview` function to
  convert model sequence to Paraview data file.
- Added dump of surface fields as `FieldData` in VTK files
- Added `scipy.sparse.linalg.LinearOperator` interface to `Westergaard`. A
  `LinearOperator` can be created by calling
  `scipy.sparse.linalg.aslinearoperator` on the operator object.
- Allowed sub-classing of `ContactSolver` in Python.
- Exposed elastic contact functionals in Python to help with custom solver
  implementation.
- Added an example of custom solver with penalty method using Scipy.
- Added `graphArea` function in statistics to compute area of function graph
- Added `tamaas.utils.radial_average` to radialy average a 2D field
- Added option to not make a dump directory (e.g. `netcdf`, `hdf5` directories
  for dump files). This allows to specify an absolute path for dump files, and
  is useful if simulation workflow is managed with file targets, as do systems
  like GNU Make and Snakemake.

### Changed

- Filter for boundary fields in dumper should be smarter.
- `read` functions in dumpers are now `@classmethod`
- Changed sign of plastic correction in KatoSaturated solver to match the
  EPICSolver sign.
- Plastic correction of KatoSaturated is registered in the model as
  `KatoSaturated::residual_displacement`.
- Changed to modern Python packaging (PEP517) with `setup.cfg` and
  `pyproject.toml`. A `setup.py` script is still provided for editable
  installations, see [gh#7953](https://github.com/pypa/pip/issues/7953). Once
  setuptools has stable support for
  [PEP621](https://peps.python.org/pep-0621/), `setup.cfg` will be merged into
  `pyproject.toml`. Note that source distributions that can be built with
  [`build`](https://pypi.org/project/build/) are not true source distributions,
  since they contain the compiled Python module of Tamaas.
- Updated copyright to reflect Tamaas' development history since leaving EPFL

### Fixed

- Fixed `tamaas.dumpers.NetCDFDumper` to dump in MPI context.

### Deprecated

- SCons versions < 3 and Python < 3.6 are explicitely no longer supported.

## v2.4.0 -- 2022-03-22

### Added

- Added a `tamaas.utils` module.
- Added `tamaas.utils.load_path` generator, which yields model objects for a
  sequence of applied loads.
- Added `tamaas.utils.seeded_surfaces` generator, which yields surfaces for a
  sequence of random seeds.
- Added `tamaas.utils.hertz_surface` which generates a parabolic (Hertzian)
  surface.
- Added `tamaas.utils.log_context` context manager which locally sets the log
  level for Tamaas' logger.
- Added `tamaas.compute.from_voigt` to convert Voigt/Mendel fields to dense
  tensor fields.
- Added a deep-copy function to `ModelFactory` to copy `Model` objects. Use
  `model_copy = copy.deepcopy(model)` in Python to make a copy. Currently only
  copies registered fields, same as dumpers/readers.
- Added a `read_sequence` method for dumpers to read model frames.
- Automatic draft release on Zenodo.

### Changed

- `*_ROOT` variables for vendored dependencies GoogleTest and pybind11 now
  default to empty strings, so that the vendored trees in `third-party/` are not
  selected by default. This is so system packages are given priority. Vendored
  dependency submodules will eventually be deprecated.

### Fixed

- Fixed an issue with `scons -c` when GTest was missing.

## v2.3.1 -- 2021-11-08

### Added

- Now using `clang-format`, `clang-tidy` and `flake8` for linting
- Pre-built Tamaas images can be pulled with `docker pull
  registry.gitlab.com/tamaas/tamaas`
- Added a `--version` flag to the `tamaas` command

### Changed

- The root `Dockerfile` now compiles Tamaas, so using Tamaas in Docker is easier.
- The model attributes dumped to Numpy files are now written in a JSON-formatted
  string to avoid unsafe loading/unpickling of objects.
- Removed the `build_doc` build option: now the doc targets are automatically
  added if the dependencies are met, and built if `scons doc` is called.
- Removed the `use_googletest` build option: if tests are built and gtest is
  present, the corresponding tests will be built

### Fixed

- The command `tamaas plot` gracefully fails if Matplotlib is missing
- Better heuristic to guess which fields are defined on boundary in dumpers
  (still not perfect and may give false negatives)

## v2.3.0 -- 2021-06-15

### Added

- Added `read()` method to dumpers to create a model from a dump file
- `getClusters()` can be called in MPI contact with partial contact maps
- Added a JSON encoder class for models and a JSON dumper
- CUDA compatibility is re-established, but has not been tested
- Docstrings in the Python bindings for many classes/methods

### Changed

- Tamaas version numbers are now managed by
  [versioneer](https://github.com/python-versioneer/python-versioneer). This
  means that Git tags prefixed with `v` (e.g. `v2.2.3`) carry meaning and
  determine the version. When no tag is set, versioneer uses the last tag,
  specifies the commit short hash and the distance to the last tag (e.g.
  `2.2.2+33.ge314b0e`). This version string is used in the compiled library, the
  `setup.py` script and the `__version__` variable in the python module.
- Tamaas migrated to [GitLab](https://gitlab.com/tamaas/tamaas)
- Continuous delivery has been implemented:

    - the `master` branch will now automatically build and publish Python wheels
      to `https://gitlab.com/api/v4/projects/19913787/packages/pypi/simple`. These
      "nightly" builds can be installed with:

          pip install \
            --extra-index-url https://gitlab.com/api/v4/projects/19913787/packages/pypi/simple \
            tamaas

    - version tags pushed to `master` will automatically publish the wheels to
      [PyPI](https://pypi.org/project/tamaas/)

### Deprecated

- The `finalize()` function is now deprecated, since it is automatically called
  when the process terminates
- Python versions 3.5 and below are not supported anymore

### Fixed

- Fixed a host of dump read/write issues when model type was not `volume_*d`.
  Dumper tests are now streamlined and systematic.
- Fixed a bug where `Model::solveDirichlet` would not compute correctly
- Fixed a bug where `Statistics::contact` would not normalize by the global
  number of surface points

## v2.2.2 -- 2021-04-02

### Added

- Entry-point `tamaas` defines a grouped CLI for `examples/pipe_tools`. Try
  executing `tamaas surface -h` from the command-line!

### Changed

- `CXXFLAGS` are now passed to the linker
- Added this changelog
- Using absolute paths for environmental variables when running `scons test`
- Reorganized documentation layout
- Gave the build system a facelift (docs are now generated directly with SCons
  instead of a Makefile)

### Deprecated

- Python 2 support is discontinued. Version `v2.2.1` is the last PyPi build with
  a Python 2 wheel.
- The scripts in `examples/pipe_tools` have been replaced by the `tamaas` command

### Fixed

- `UVWDumper` no longer imports `mpi4py` in sequential
- Compiling with different Thrust/FFTW backends


## v2.2.1 -- 2021-03-02

### Added

- Output registered fields and dumpers in `print(model)`
- Added `operator[]` to the C++ model class (for fields)
- Added `traction` and `displacement` properties to Python model bindings
- Added `operators` property to Python model bindings, which provides a
  dict-like access to registered operators
- Added `shape` and `spectrum` to properties to Python surface generator
  bindings
- Surface generator constructor accepts surface global shape as argument
- Choice of FFTW thread model

### Changed

- Tests use `/tmp` for temporary files
- Updated dependency versions (Thrust, Pybind11)

### Deprecated

- Most `get___()` and `set___()` in Python bindings have been deprecated. They
  will generate a `DeprecationWarning`.

### Removed

- All legacy code


## v2.2.0 -- 2020-12-31

### Added

- More accurate function for computation of contact area
- Function to compute deviatoric of tensor fields
- MPI implementation
- Convenience `hdf5toVTK` function
- Readonly properties `shape`, `global_shape`, `boundary_shape` on model to give
  shape information

### Changed

- Preprocessor defined macros are prefixed with `TAMAAS_`
- Moved `tamaas.to_voigt` to `tamaas.compute.to_voigt`

### Fixed

- Warning about deprecated constructors with recent GCC versions
- Wrong computation of grid strides
- Wrong computation of grid sizes in views


## v2.1.4 -- 2020-08-07

### Added

- Possibility to generate a static `libTamaas`
- C++ implementation of DFSANE solver
- Allowing compilation without OpenMP

### Changed

- NetCDF dumper writes frames to a single file

### Fixed

- Compatibility with SCons+Python 3

## v2.1.3 -- 2020-07-27

### Added

- Version number to `TamaasInfo`

### Changed

- Prepending root directory when generating archive


## v2.1.2 -- 2020-07-24

This release changes some core internals related to discrete Fourier transforms
for future MPI support.

### Added

- Caching `CXXFLAGS` in SCons build
- SCons shortcut to create code archive
- Test of the elastic-plastic contact solver
- Paraview data dumper (`.pvd` files)
- Compression for UVW dumper
- `__contains__` and `__iter__` Python bindings of model
- Warning message of possible overflow in Kelvin

### Changed

- Simplified `tamaas_info.cpp`, particularly the diff part
- Using a new class `FFTEngine` to manage discrete Fourier transforms. Plans are
  re-used as much as possible with different data with the same shape. This is
  in view of future MPI developments
- Redirecting I/O streams in solve functions so they can be used from Python
  (e.g. in Jupyter notebooks)
- Calling `initialize()` and `finalize()` is no longer necessary

### Fixed

- Convergence issue with non-linear solvers
- Memory error in volume potentials


## v2.1.1 -- 2020-04-22

### Added

- SCons shortcut to run tests

### Fixed

- Correct `RPATH` for shared libraries
- Issues with SCons commands introduced in v2.1.0
- Tests with Python 2.7


## v2.1.0 -- 2020-04-17

### Added

- SCons shortcuts to build/install Tamaas and its components
- Selection of integration method for Kelvin operator
- Compilation option to remove the legacy part of Tamaas
- NetCDF dumper

### Fixed

- Link bug with clang
- NaNs in Kato saturated solver

## v2.0.0 -- 2019-11-11

First public release. Contains relatively mature elastic-plastic contact code.
