Tamaas --- A high-performance library for periodic rough surface contact
========================================================================

[![JOSS](https://joss.theoj.org/papers/10.21105/joss.02121/status.svg)](https://doi.org/10.21105/joss.02121)
[![Documentation Status](https://readthedocs.org/projects/tamaas/badge/?version=latest)](https://tamaas.readthedocs.io/en/latest/?badge=latest)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/tamaas%2Ftutorials/HEAD)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3479236.svg)](https://doi.org/10.5281/zenodo.3479236)

Tamaas is a C++/Python library that implements a number of numerical methods
based on integral equations to efficiently solve contact problems with rough
surfaces. The word تماس (tamaas) means "contact" in Arabic and Farsi.

## 🚀 Quick Start

If you have a Linux system, you can simply run `pip3 install tamaas`. Note
however that there due to compatibility reasons, this version of Tamaas is not
built with parallel/multi-threading capabilities. So if you want parallelism, or
encounter an issue with the [PyPI package](https://pypi.org/project/tamaas/),
please compile from source. This can be easily done with
[Spack](https://spack.io/) using the command `spack install tamaas`. See `spack
info tamaas` for build options.

We also provide up-to-date Docker images (to use on macOS for example):

    docker pull registry.gitlab.com/tamaas/tamaas

To discover Tamaas' functionality, you can use explore of our [interactive
tutorial
notebooks](https://tamaas.readthedocs.io/en/latest/overview.html#tutorials) to
get started.

## 📚 Dependencies

Here is a list of dependencies to compile Tamaas:

- a C++ compiler with full C++14 and OpenMP support
- [SCons](https://scons.org/) (python build system)
- [FFTW3](http://www.fftw.org/)
- [boost](https://www.boost.org/) (preprocessor)
- [thrust](https://github.com/NVIDIA/thrust) (1.9.2+)
- [python 3+](https://www.python.org/) with [numpy](https://numpy.org/), and pip
  to install the Python package
- [pybind11](https://github.com/pybind/pybind11)

Optional dependencies are:

- an MPI implementation
- [FFTW3](http://www.fftw.org/) with MPI/pthreads/OpenMP support
- [scipy](https://scipy.org) (for nonlinear solvers)
- [uvw](https://pypi.org/project/uvw/),
  [netCDF4](https://unidata.github.io/netcdf4-python/netCDF4/index.html#section1),
  [h5py](https://www.h5py.org/) (for various dumpers)
- [googletest](https://github.com/google/googletest) and
  [pytest](https://docs.pytest.org/en/latest/) (for tests)
- [Doxygen](http://doxygen.nl/) and
  [Sphinx](https://www.sphinx-doc.org/en/stable/) (for documentation)

Note that a Debian distribution should have the right packages for all these
dependencies (they package the right version of thrust extracted from CUDA in
`stretch-backports non-free` and `buster non-free`).

## 🔨 Compiling

Here are the instructions to compile Tamaas. Note that we provide a [Docker
image](#using-docker-image) and a [PyPI
package](https://pypi.org/project/tamaas/) that do not require compiling.

### Installing Dependencies

On Debian-based systems, mandatory dependencies can be installed with the
following command:

    apt install \
        g++ \
        libboost-dev \
        libthrust-dev \
        libfftw3-dev \
        pybind11-dev \
        python3-dev \
        python3-numpy \
        scons

Additionally, to build and run tests, one needs to install:

    apt install \
        libgtest-dev \
        python3-pytest

For documentation:

    apt install \
        doxygen \
        python3-sphinx \
        python3-breathe \
        python3-sphinx-rtd-theme

### Build System

The build system uses SCons. In order to compile Tamaas with the default
options (i.e. optimized release build with python bindings):

    scons

After compiling a first time (or running `scons -h` if you want to avoid
compiling), you can edit the compilation options in the file `build-setup.conf`,
or alternatively supply the options directly in the command line:

    scons option=value [...]

To get a list of *all* build options and their possible values, you can run
`scons -h`. You can run `scons -H` to see the SCons-specific options (among them
`-j n` executes the build with `n` threads and `-c` cleans the build). Note that
the build is aware of the `CXX` and `CXXFLAGS` environment variables.

## 🏗️ Installing

Before you can import tamaas in python, you need to install the python package
in some way.

### Using virtual environments and pip

Python best practices are to install modules in virtual environments so as to
not interfere with system package managers.

    python3 -m venv /path/to/tamaas_venv
    source /path/to/tamaas_venv/bin/activate
    scons install prefix=/path/to/tamaas_venv

You can check that everything is working fine with:

    python3 -c 'import tamaas; print(tamaas)'

You can make an editable installation with:

    source /path/to/tamaas_venv/bin/activate
    scons dev

### Using Docker image

Tamaas provides a `Dockerfile` you can use to compile and deploy Tamaas in a
tested environment. Simply run `docker build -t tamaas .` in the root folder of
the repository. Check the documentation to see which build options can be
adjusted.

## 🔍 Tests

To run tests, make sure to have [pytest](https://docs.pytest.org/en/latest/)
installed and run `scons test` if you have compiled Tamaas with tests activated
(`scons build_tests=True`).

## 📖 Documentation

The latest documentation is available on
[ReadTheDocs](https://tamaas.readthedocs.io/en/latest/)! It contains detailed
guides on the different components of Tamaas as well as a combined C++ and
Python API documentation.

You can also build the documentation locally: run `scons doc`. Make sure you
have [sphinx-rtd-theme](https://pypi.org/project/sphinx-rtd-theme/) and
[breathe](https://pypi.org/project/breathe/) installed. The compiled indexes for
the doxygen C++ API and Sphinx documentation can be found in
`build-release/doc/{doxygen,sphinx}/html/index.html`.

## 📑 Examples

Example simulations can be found in the `examples/` directory. These simulations
can be run in MPI.

- `rough_contact.py` shows a typical normal rough contact simulation
- `adhesion.py` shows how you can derive some classes from Tamaas in python,
  here to implement a custom adhesion potential
- `plasticity.py` computes an elastoplastic Hertz simulation and dumps the
  result in `examples/paraview/` in VTK format
- `stresses.py` shows how you can compute stresses from a boundary traction
  distribution

## 💻 Command-line Interface

The Tamaas python module exposes a `tamaas` command with three subcommands:

- `tamaas surface` generates rough surfaces
- `tamaas contact` solves elastic contact
- `tamaas plot` plots contact pressure and displacement

See `tamaas <cmd> --help` for command line arguments.

## 👷 Contributing

Contributions to Tamaas are welcome! Please follow the guidelines below.

### Report an issue

If you have an account on [gitlab](https://gitlab.com), you can [submit an
issue](https://gitlab.com/tamaas/tamaas/-/issues/new). The full list of issues
is available [here](https://gitlab.com/tamaas/tamaas/-/issues). Please read the
[FAQ](https://tamaas.readthedocs.io/en/latest/faq.html) before posting.

### Submit a patch / merge-request

Follow [this
guide](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#new-merge-request-from-a-fork)
to create a merge request on GitLab. Please target the repository's `master`
branch.

## 📜 Citing

Tamaas is the result of a science research project. To give proper credit to
Tamaas and the researchers who have developed the numerical methods that it
implements, please cite Tamaas as:

> Frérot , L., Anciaux, G., Rey, V., Pham-Ba, S., & Molinari, J.-F. Tamaas: a
> library for elastic-plastic contact of periodic rough surfaces. Journal of Open
> Source Software, 5(51), 2121 (2020).
> [doi:10.21105/joss.02121](https://doi.org/10.21105/joss.02121)

The paper above details relevant references for the different features of
Tamaas. In particular, if you use the elastic-plastic contact capabilities of
Tamaas, please cite:

> Frérot, L., Bonnet, M., Molinari, J.-F. & Anciaux, G. A Fourier-accelerated
> volume integral method for elastoplastic contact. Computer Methods in Applied
> Mechanics and Engineering 351, 951–976 (2019)
> [doi:10.1016/j.cma.2019.04.006](https://doi.org/10.1016/j.cma.2019.04.006).

If you use the adhesive contact capabilities of Tamaas, please cite:

> Rey, V., Anciaux, G. & Molinari, J.-F. Normal adhesive contact on rough
> surfaces: efficient algorithm for FFT-based BEM resolution. Comput Mech 1–13
> (2017)
> [doi:10.1007/s00466-017-1392-5](https://doi.org/10.1007/s00466-017-1392-5).

For an (almost) exhaustive list of publications, insert the following code
snippet at the end of your python scripts:

```python
from tamaas.utils import publications
publications()
```

## 📰 Changelog

The changelog can be consulted
[here](https://gitlab.com/tamaas/tamaas/-/blob/master/CHANGELOG.md).

## 🏦 Funding

Tamaas' development was funded by the [Swiss National Science
Foundation](https://www.snf.ch), grants [162569 ("Contact mechanics of rough
surfaces")](https://data.snf.ch/grants/grant/162569) and [191720 ("Tribology of
Polymers: from Atomistic to Continuum
Scales")](https://data.snf.ch/grants/grant/191720), and by [Deutsche
Forschungsgemeinschaft](https://www.dfg.de/), grant [461911253
("AWEARNESS")](https://gepris.dfg.de/gepris/projekt/461911253).

## ⚖️ License

Tamaas is distributed under the terms of the [GNU Affero General Public License
v3.0](https://www.gnu.org/licenses/agpl.html).
