ExPolIt - Exponential/Polynomial Integration
==============================================

This small header library is meant to integrate functions of the form `exp(qx)*p(x)` where `p` is a polynomial of any order. The code is designed so that the construction of the expression resulting from the integral is done at compile-time. There are however two limitations:

  - if you use a coefficient type for the polynomial that cannot serve as a constant expression (`constexpr`);
  - if the resulting expression contains an exponential, it cannot be numerically evaluated at compile time. However the integral expression is still built at compile time (provided the coefficient type can serve as `constexpr`).

It's a header only library, so just put the `include` directory in your include path and make sure your compiler is C++17 compliant. You can compile tests in the `tests` directory with SCons and GoogleTest.

License
-------

This program is licensed under the GNU General Public License v3. A copy of the license is provided with the code.
