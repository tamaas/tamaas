/*
 * Copyright (©)  2018-2024 EPFL  (Ecole Polytechnique  Fédérale de
 * Lausanne)  Laboratory (LSMS  -  Laboratoire de  Simulation  en Mécanique  des
 * Solides)
 *
 * Expolit is free software: you can redistribute it and/or  modify it under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Expolit is  distributed in the  hope that it  will be useful, but  WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License for
 * more details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with Expolit. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef EXPONENTIAL_HH
#define EXPONENTIAL_HH

#include "types.hh"
#include "operations.hh"
#include <cmath>
#include <ostream>
#include <iostream>

namespace expolit {

namespace detail {
// Necessary for proper dispatch
using std::exp;

struct ExponentialFunctor {
  template <typename Val>
  constexpr auto operator()(Val&& v) const {
    return exp(v);  // Should dispatch in case expression is passed
  }
};
}

/**
 * @brief Exponential expression
 *
 * Handles exponential of arbitrary expression
 */
template <typename T>
using Exponential = UnaryOperation<T, detail::ExponentialFunctor>;

template <typename Derived>
constexpr auto exp(const Expression<Derived>& e) {
  return Exponential<std::remove_cv_t<Derived>>(e.downcast());
}

/* -------------------------------------------------------------------------- */
/* Output */
/* -------------------------------------------------------------------------- */
template <typename T>
std::ostream& operator<<(std::ostream& os, const Exponential<T>& e) {
  os << "exp(" << e.expression << ")";
  return os;
}

}  // namespace expolit

#endif
