/*
 * Copyright (©)  2018-2024 EPFL  (Ecole Polytechnique  Fédérale de
 * Lausanne)  Laboratory (LSMS  -  Laboratoire de  Simulation  en Mécanique  des
 * Solides)
 *
 * Expolit is free software: you can redistribute it and/or  modify it under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Expolit is  distributed in the  hope that it  will be useful, but  WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License for
 * more details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with Expolit. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TYPES_HH
#define TYPES_HH

#include <cstddef>
#include <ostream>
#include <type_traits>
#include <utility>

namespace expolit {
using Int = int;
using UInt = unsigned int;

/// Expression class for expression templates
template <typename Derived>
struct Expression {
  constexpr const Derived& downcast() const { return static_cast<const Derived&>(*this); }
  constexpr Derived& downcast() { return static_cast<Derived&>(*this); }
};

template <typename Derived>
std::ostream& operator<<(std::ostream& os, const Expression<Derived>& e) {
  os << e.downcast();
  return os;
}
}  // namespace expolit
#endif
