/*
 * Copyright (©)  2018-2024 EPFL  (Ecole Polytechnique  Fédérale de
 * Lausanne)  Laboratory (LSMS  -  Laboratoire de  Simulation  en Mécanique  des
 * Solides)
 *
 * Expolit is free software: you can redistribute it and/or  modify it under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Expolit is  distributed in the  hope that it  will be useful, but  WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License for
 * more details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with Expolit. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef LITTERAL_HH
#define LITTERAL_HH

#include "operations.hh"
#include "polynomial.hh"
#include "types.hh"

namespace expolit {
template <typename Tag>
struct Litteral : Expression<Litteral<Tag>> {
  using tag = Tag;
  Litteral() = default;
};

/// Substitue litteral
template <typename Tag, typename New>
constexpr auto substitute(const Expression<New>& val,
			  const Litteral<Tag>& /*t*/) {
  return val.downcast();
}

/// Substitute litteral in binary operation
template <typename Tag, typename New, typename Der1, typename Der2,
          typename Op>
constexpr auto substitute(const Expression<New>& val,
                          const BinaryOperation<Der1, Der2, Op>& op) {
  return op.op(substitute<Tag>(val, op.operands.first),
               substitute<Tag>(val, op.operands.second));
}

/// Substitute litteral in polynomial (aka do nothing)
template <typename Tag, typename New, typename CT, UInt order>
constexpr auto substitute(const Expression<New>& /*val*/,
			  const Polynomial<CT, order>& p) {
  return p;
}

/// Substitute litteral in polynomial (aka do nothing)
template <typename Tag, typename New, typename T, typename Op>
constexpr auto substitute(const Expression<New>& val,
			  const UnaryOperation<T, Op>& p) {
  return p.op(substitute<Tag>(val, p.expression));
}

/// Substitute generic expression by downcast
// template <typename Tag, typename New, typename Derived>
// constexpr auto substitute(const Expression<New>& val,
// 			  const Expression<Derived>& e) {
//   return substitute<Tag>(val, e.downcast());
// }

}  // namespace expolit

#endif
