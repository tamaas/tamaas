import SCons
from SCons.Script import Exit, Configure

from os.path import join, abspath


def FindGBenchmark(env):
    """
    Sets environement for Google's benchmark library
    """
    if env.GetOption('clean'):
        return

    if 'BENCHMARK_PATH' not in env:
        print("Please set the 'BENCHMARK_PATH' vairiable " +
              "in your scons environment")
        Exit(1)

    bm_vars = {}
    bm_path = abspath(env['BENCHMARK_PATH'])
    bm_vars['CPPPATH'] = [join(bm_path, 'include')]
    bm_vars['LIBPATH'] = [join(bm_path, 'build/src')]
    bm_vars['RPATH'] = bm_vars['LIBPATH']
    bm_vars['LIBS'] = ['pthread']

    conf_env = env.Clone(**bm_vars)
    conf = Configure(conf_env)

    if not conf.CheckLibWithHeader("benchmark", "benchmark/benchmark.h",
                                   language="CXX"):
        raise SCons.Errors.StopError(
            "Google benchmark was not found in  {}".format(bm_path))
    conf_env = conf.Finish()
    bm_vars['LIBS'] += ['benchmark']

    env.AppendUnique(**bm_vars)

