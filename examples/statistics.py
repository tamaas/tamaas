#!/usr/bin/env python3
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os

import numpy as np
import matplotlib.pyplot as plt
import tamaas as tm

from tamaas.utils import publications, radial_average
from matplotlib.colors import LogNorm

tm.set_log_level(tm.LogLevel.info)  # Show progression of solver

# Surface size
n = 512

# Surface generator
sg = tm.SurfaceGeneratorFilter2D([n, n])
sg.random_seed = 1

# Spectrum
sg.spectrum = tm.Isopowerlaw2D()

# Parameters
sg.spectrum.q0 = 16
sg.spectrum.q1 = 16
sg.spectrum.q2 = 128
sg.spectrum.hurst = 0.8

# Generating surface
surface = sg.buildSurface()

# Computing PSD and shifting for plot
psd = tm.Statistics2D.computePowerSpectrum(surface)
psd = np.fft.fftshift(psd, axes=0).real

plt.imshow(np.clip(psd.real, 1e-10, np.inf), norm=LogNorm())
plt.gca().set_title('Power Spectrum Density')
plt.gcf().tight_layout()

# Computing autocorrelation and shifting for plot
acf = tm.Statistics2D.computeAutocorrelation(surface)
acf = np.fft.fftshift(acf)

plt.figure()
plt.imshow(acf)
plt.gca().set_title('Autocorrelation')
plt.gcf().tight_layout()

# Computing radial averages
fig, axs = plt.subplots(1, 2, figsize=(6, 3))
qx = np.fft.fftshift(np.fft.fftfreq(surface.shape[0], d=1 / surface.shape[0]))
qy = np.fft.rfftfreq(surface.shape[1], d=1 / surface.shape[1])
r = np.linspace(0, qy.max() - 1)
theta = np.linspace(0, np.pi, 30)
psd_rad = radial_average(qx,
                         qy,
                         psd,
                         r,
                         theta,
                         method='nearest',
                         endpoint=True)
axs[0].plot(r, psd_rad)

x = np.linspace(-.5, .5, surface.shape[0])
y = np.linspace(-.5, .5, surface.shape[1])
r = np.linspace(0, .5, surface.shape[0] // 2)
theta = np.linspace(0, 2 * np.pi, 60)
acf_rad = radial_average(x, y, acf, r, theta)
axs[1].plot(r, acf_rad)

axs[0].set_xlabel("q")
axs[0].set_ylabel("PSD")
axs[0].set_xscale('log')
axs[0].set_yscale('log')
axs[0].set_ylim(1e-5, 1)

axs[1].set_xlabel("r")
axs[1].set_ylabel("ACF")

fig.tight_layout()
plt.show()

# Write the rough surface in paraview's VTK format
try:
    import uvw
    try:
        os.mkdir('paraview')
    except FileExistsError:
        pass
    x = np.linspace(0, 1, n, endpoint=True)
    y = x.copy()

    with uvw.RectilinearGrid(os.path.join('paraview', 'surface.vtr'),
                             (x, y)) as grid:
        grid.addPointData(uvw.DataArray(surface, range(2), 'surface'))
except ModuleNotFoundError:
    print("uvw not installed, will not write VTK file")
    pass

publications()
