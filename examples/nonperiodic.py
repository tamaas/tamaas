#!/usr/bin/env python3
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import tamaas as tm
import matplotlib.pyplot as plt

from tamaas.utils import publications, hertz_surface

# Surface size
n = 512

# Surface radius
R = 0.1

# Surface generator
surface = hertz_surface([1., 1.], [n, n], R)

plt.imshow(surface)

# Creating model
model = tm.ModelFactory.createModel(tm.model_type.basic_2d, [1., 1.], [n, n])
tm.ModelFactory.registerNonPeriodic(model, 'dcfft')

# Solver
solver = tm.PolonskyKeerRey(model, surface, 1e-12)
solver.setIntegralOperator('dcfft')

# Solve for target pressure
p_target = 1.2
solver.solve(p_target)

# The concact solver always shifts the displacement so that gap >= 0
# To compute the true displacement, one needs to re-evaluate the displacement
model.operators['dcfft'](model.traction, model.displacement)
delta = (9 * p_target**2 / (16 * R * model.E_star**2))**(1/3)
print(f"Hertzian interference = {delta:.4f}, computed = {model.displacement.max():.4f}")

plt.figure()
plt.imshow(model.traction)
plt.title('Contact tractions')

plt.show()

publications()
