# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import tamaas as tm
import numpy as np
import matplotlib.pyplot as plt

from tamaas.utils import publications

grid_size = 256
load = 1.6
p_sat = 100

iso = tm.Isopowerlaw2D()
iso.q0 = 4
iso.q1 = 4
iso.q2 = 16
iso.hurst = 0.8

sg = tm.SurfaceGeneratorFilter2D([grid_size, grid_size])
sg.random_seed = 2
sg.spectrum = iso
surface = sg.buildSurface()
surface -= np.max(surface)

model = tm.ModelFactory.createModel(tm.model_type.basic_2d,
                                    [1., 1.], [grid_size, grid_size])
model.E = 1.
model.nu = 0
esolver = tm.PolonskyKeerRey(model, surface, 1e-12,
                             tm.PolonskyKeerRey.pressure,
                             tm.PolonskyKeerRey.pressure)

esolver.solve(load)

elastic_tractions = model.traction.copy()
plt.imshow(elastic_tractions)
plt.title('Elastic contact tractions')
plt.colorbar()

model.traction[:] = 0.
solver = tm.KatoSaturated(model, surface, 1e-12, p_sat)
solver.dump_freq = 1
solver.max_iter = 200
solver.solve(load)

plt.figure()
plt.imshow(model.traction)
plt.title('Saturated contact tractions')
plt.colorbar()
plt.show()

publications()
