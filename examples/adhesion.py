#!/usr/bin/env python3
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import tamaas as tm
import matplotlib.pyplot as plt
import numpy as np
import argparse

from tamaas.utils import publications
from matplotlib.colors import ListedColormap


class AdhesionPython(tm.Functional):
    """
    Functional class that extends a C++ class and implements the virtual
    methods
    """

    def __init__(self, rho, gamma):
        tm.Functional.__init__(self)
        self.rho = rho
        self.gamma = gamma

    def computeF(self, gap, pressure):
        return -self.gamma * np.sum(np.exp(-gap / self.rho))

    def computeGradF(self, gap, gradient):
        gradient += self.gamma * np.exp(-gap / self.rho) / self.rho


parser = argparse.ArgumentParser()
parser.add_argument('--local-functional', dest="py_adh", action="store_true",
                    help="use the adhesion functional written in python")

args = parser.parse_args()

# Surface size
n = 1024

# Surface generator
sg = tm.SurfaceGeneratorFilter2D([n, n])
sg.random_seed = 1

# Spectrum
sg.spectrum = tm.Isopowerlaw2D()

# Parameters
sg.spectrum.q0 = 16
sg.spectrum.q1 = 16
sg.spectrum.q2 = 64
sg.spectrum.hurst = 0.8

# Generating surface
surface = sg.buildSurface()
surface /= n

plt.imshow(surface)
plt.title('Rough surface')

# Creating model
model = tm.ModelFactory.createModel(tm.model_type.basic_2d, [1., 1.], [n, n])

# Solver
solver = tm.PolonskyKeerRey(model, surface, 1e-12,
                            tm.PolonskyKeerRey.gap,
                            tm.PolonskyKeerRey.gap)
adhesion_params = {
  "rho": 2e-3,
  "surface_energy": 2e-5
}

# Use the python derived from C++ functional class
if args.py_adh:
    adhesion = AdhesionPython(adhesion_params["rho"],
                              adhesion_params["surface_energy"])
# Use the C++ class
else:
    adhesion = tm.ExponentialAdhesionFunctional(surface)
    adhesion.parameters = adhesion_params

solver.addFunctionalTerm(adhesion)

# Solve for target pressure
g_target = 5e-2
solver.solve(g_target)

tractions = model.traction

plt.figure()
plt.imshow(tractions)
plt.colorbar()
plt.title('Contact tractions')

plt.figure()
zones = np.zeros_like(tractions)
tol = 1e-6
zones[tractions > tol] = 1
zones[tractions < -tol] = -1
plt.imshow(zones, cmap=ListedColormap(['white', 'gray', 'black']))
plt.colorbar(ticks=[-2/3, 0, 2/3]).set_ticklabels([
    'Adhesion', 'No Contact', 'Contact'
])

plt.title('Contact and Adhesion Zones')

plt.show()

publications()
