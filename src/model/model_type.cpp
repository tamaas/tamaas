/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "model_type.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

const std::vector<UInt> model_type_traits<model_type::basic_1d>::indices = {};
const std::vector<UInt> model_type_traits<model_type::basic_2d>::indices = {};
const std::vector<UInt> model_type_traits<model_type::surface_1d>::indices = {};
const std::vector<UInt> model_type_traits<model_type::surface_2d>::indices = {};
const std::vector<UInt> model_type_traits<model_type::volume_1d>::indices = {0};
const std::vector<UInt> model_type_traits<model_type::volume_2d>::indices = {0};

constexpr char model_type_traits<model_type::basic_1d>::repr[];
constexpr char model_type_traits<model_type::basic_2d>::repr[];
constexpr char model_type_traits<model_type::surface_1d>::repr[];
constexpr char model_type_traits<model_type::surface_2d>::repr[];
constexpr char model_type_traits<model_type::volume_1d>::repr[];
constexpr char model_type_traits<model_type::volume_2d>::repr[];

/* -------------------------------------------------------------------------- */
}  // namespace tamaas
