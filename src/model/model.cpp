/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "model.hh"
#include "be_engine.hh"
#include "logger.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */
void Model::setElasticity(Real E_, Real nu_) {
  setYoungModulus(E_);
  setPoissonRatio(nu_);
}

/* -------------------------------------------------------------------------- */
void Model::applyElasticity(GridBase<Real>& stress,
                            const GridBase<Real>& strain) const {
  operators.at("hooke")->apply(const_cast<GridBase<Real>&>(strain), stress);
}

/* -------------------------------------------------------------------------- */
GridBase<Real>& Model::getTraction() { return this->field<Real>("traction"); }
const GridBase<Real>& Model::getTraction() const {
  return this->field<Real>("traction");
}

/* -------------------------------------------------------------------------- */
GridBase<Real>& Model::getDisplacement() {
  return this->field<Real>("displacement");
}
const GridBase<Real>& Model::getDisplacement() const {
  return this->field<Real>("displacement");
}

/* -------------------------------------------------------------------------- */
const std::vector<Real>& Model::getSystemSize() const { return system_size; }

/* -------------------------------------------------------------------------- */
const std::vector<UInt>& Model::getDiscretization() const {
  return discretization;
}

/* -------------------------------------------------------------------------- */
void Model::solveNeumann() {
  engine->registerNeumann();
  engine->solveNeumann(getTraction(), getDisplacement());
}

void Model::solveDirichlet() {
  engine->registerDirichlet();
  engine->solveDirichlet(getDisplacement(), getTraction());
}

/* -------------------------------------------------------------------------- */
void Model::registerIntegralOperator(const std::string& name,
                                     std::shared_ptr<IntegralOperator> op) {
  operators[name] = std::move(op);
}

/* -------------------------------------------------------------------------- */
std::shared_ptr<IntegralOperator>
Model::getIntegralOperator(const std::string& name) const {
  return operators.at(name);
}

/* -------------------------------------------------------------------------- */
std::vector<std::string> Model::getIntegralOperators() const {
  std::vector<std::string> keys;
  keys.reserve(operators.size());

  std::transform(operators.begin(), operators.end(), std::back_inserter(keys),
                 [](auto&& pair) { return pair.first; });
  return keys;
}

/* -------------------------------------------------------------------------- */
void Model::updateOperators() {
  for (auto& op : operators)
    op.second->updateFromModel();
}

/* -------------------------------------------------------------------------- */
void Model::addDumper(std::shared_ptr<ModelDumper> dumper) {
  this->dumpers.push_back(std::move(dumper));
}

void Model::dump() const {
  if (dumpers.empty())
    Logger().get(LogLevel::warning) << "dumper list is empty\n";

  for (const auto& dumper : dumpers)
    if (dumper)
      dumper->dump(*this);
}

/* -------------------------------------------------------------------------- */
std::vector<std::string> Model::getBoundaryFields() const {
  std::vector<std::string> boundary_fields;

  for (auto&& pair : this->fields_map()) {
    const auto boundary = boost::apply_visitor(
        [&](auto&& grid_ptr) { return this->isBoundaryField(*grid_ptr); },
        pair.second);

    if (boundary)
      boundary_fields.push_back(pair.first);
  }

  return boundary_fields;
}

/* -------------------------------------------------------------------------- */
std::ostream& operator<<(std::ostream& o, const Model& _this) {
  o << "Model<" << _this.getType() << "> (E = " << _this.getYoungModulus()
    << ", nu = " << _this.getPoissonRatio() << ")\n";

  auto out_collec = [&o](auto&& collec) {
    std::for_each(collec.begin(), collec.end() - 1,
                  [&o](const auto& x) { o << x << ", "; });
    o << collec.back();
  };

  // Printing domain size
  o << "  - domain = [";
  out_collec(_this.getSystemSize());
  o << "]\n";

  // Printing discretization
  o << "  - discretization = [";
  out_collec(_this.getDiscretization());
  o << "]\n";

  if (mpi::size() > 1) {
    o << "  - global discretization = [";
    out_collec(_this.getGlobalDiscretization());
    o << "]\n";
  }

  // Print fields
  o << "  - registered fields = [";
  out_collec(_this.fields());
  o << "]\n";

  o << "  - registered operators = [";
  out_collec(_this.getIntegralOperators());
  o << "]";

  if (not _this.dumpers.empty())
    o << "\n  - " << _this.dumpers.size() << " registered dumpers";

  return o;
}

/* --------------------------------------------------------------------------
 */
}  // namespace tamaas
