/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef VOLUME_POTENTIAL_HH
#define VOLUME_POTENTIAL_HH
/* -------------------------------------------------------------------------- */
#include "fft_engine.hh"
#include "grid_hermitian.hh"
#include "grid_view.hh"
#include "integral_operator.hh"
#include "logger.hh"
#include "model_type.hh"
/* -------------------------------------------------------------------------- */
#include <functional>
/* -------------------------------------------------------------------------- */
namespace tamaas {

/// Trait type for component management
template <UInt derivative>
struct derivative_traits;

template <>
struct derivative_traits<0> {
  template <model_type type>
  static constexpr UInt source_components = model_type_traits<type>::components;
  template <model_type type>
  static constexpr UInt out_components = model_type_traits<type>::components;
};

template <>
struct derivative_traits<1> {
  template <model_type type>
  static constexpr UInt source_components = model_type_traits<type>::voigt;
  template <model_type type>
  static constexpr UInt out_components = model_type_traits<type>::components;
};

template <>
struct derivative_traits<2> {
  template <model_type type>
  static constexpr UInt source_components = model_type_traits<type>::voigt;
  template <model_type type>
  static constexpr UInt out_components = model_type_traits<type>::voigt;
};

/**
 * @brief Volume potential operator class. Applies the operators for computation
 * of displacements and strains due to residual/eigen strains
 */
template <model_type type>
class VolumePotential : public IntegralOperator {
  using trait = model_type_traits<type>;

protected:
  using filter_t = const std::function<bool(UInt)>&;

public:
  VolumePotential(Model* model);

  /// Update from model (does nothing)
  void updateFromModel() override {}

  /// Kind
  IntegralOperator::kind getKind() const override {
    return IntegralOperator::neumann;
  }

  /// Type
  model_type getType() const override;

  /// Apply to all of the source layers
  void apply(GridBase<Real>& input, GridBase<Real>& output) const override {
    applyIf(input, output, [](UInt) { return true; });
  }

protected:
  /// Transform source layer-by-layer
  void transformSource(GridBase<Real>& in, filter_t pred) const;
  /// Transform all source
  void transformSource(GridBase<Real>& in) const {
    transformSource(in, [](auto) { return true; });
  }
  /// Transform output layer-by-layer
  template <typename Func>
  void transformOutput(Func func, GridBase<Real>& out) const;

  /// Initialize fourier buffers
  void initialize(UInt source_components, UInt out_components,
                  UInt out_buffer_size);

protected:
  Grid<Real, trait::boundary_dimension> wavevectors;
  using BufferType = GridHermitian<Real, trait::boundary_dimension>;
  mutable std::vector<BufferType> source_buffer;
  mutable std::vector<BufferType> out_buffer;
  mutable std::unique_ptr<FFTEngine> engine;
};

/* -------------------------------------------------------------------------- */
/* Template implementation */
/* -------------------------------------------------------------------------- */

template <model_type type>
template <typename Func>
void VolumePotential<type>::transformOutput(Func func,
                                            GridBase<Real>& out) const {
  constexpr UInt dim = trait::dimension;
  auto& o = dynamic_cast<Grid<Real, dim>&>(out);

  // Transforming output
  for (UInt layer : Loop::range(o.sizes().front())) {
    auto out_layer = make_view(o, layer);
    auto& fourier_out_layer = func(out_buffer, layer);
    engine->backward(out_layer, fourier_out_layer);
  }
}

}  // namespace tamaas

#endif  // VOLUME_POTENTIAL_HH
