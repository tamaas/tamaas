/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef HOOKE_HH
#define HOOKE_HH
/* -------------------------------------------------------------------------- */
#include "integral_operator.hh"
#include "model_type.hh"
/* -------------------------------------------------------------------------- */

namespace tamaas {

/**
 * @brief Applies Hooke's law of elasticity
 */
template <model_type type>
class Hooke : public IntegralOperator {
public:
  using IntegralOperator::IntegralOperator;

  /// Type of underlying model
  model_type getType() const override { return type; }

  /// Operator is local in real space
  IntegralOperator::kind getKind() const override {
    return IntegralOperator::dirac;
  }

  /// Does not update
  void updateFromModel() override {}

  /// Apply Hooke's tensor
  void apply(GridBase<Real>& strain, GridBase<Real>& stress) const override;

  /// LinearOperator shape
  std::pair<UInt, UInt> matvecShape() const override;

  /// LinearOperator interface
  GridBase<Real> matvec(GridBase<Real>& X) const override;
};

template <model_type type>
class HookeField : public Hooke<type> {
public:
  HookeField(Model* model);

  /// Apply Hooke's tensor
  void apply(GridBase<Real>& strain, GridBase<Real>& stress) const override;
};

}  // namespace tamaas

#endif  // HOOKE_HH
