/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef MINDLIN_HH
#define MINDLIN_HH
/* -------------------------------------------------------------------------- */
#include "grid_hermitian.hh"
#include "kelvin.hh"
#include "model_type.hh"
#include "volume_potential.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

/**
 * @brief Mindlin tensor
 */
template <model_type type, UInt derivative>
class Mindlin : public Kelvin<type, derivative> {
  static_assert(type == model_type::volume_1d || type == model_type::volume_2d,
                "Only volume types are supported");
  using trait = model_type_traits<type>;
  using parent = Kelvin<type, derivative>;
  using filter_t = typename parent::filter_t;

public:
  /// Constructor
  Mindlin(Model* model);
  /// Apply the Mindlin-tensor_order operator
  void applyIf(GridBase<Real>& source, GridBase<Real>& out,
               filter_t pred) const override;

protected:
  void linearIntegral(GridBase<Real>& out) const;
  void cutoffIntegral(GridBase<Real>& out) const;

protected:
  mutable GridHermitian<Real, trait::boundary_dimension> surface_tractions;
};

}  // namespace tamaas

#endif  // MINDLIN_HH
