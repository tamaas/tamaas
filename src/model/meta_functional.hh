/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef META_FUNCTIONAL_HH
#define META_FUNCTIONAL_HH

#include "functional.hh"
#include "tamaas.hh"
#include <list>
#include <memory>

namespace tamaas {

namespace functional {

/// Meta functional that contains list of functionals
class MetaFunctional : public Functional {
  using FunctionalList = std::list<std::shared_ptr<Functional>>;

public:
  /// Compute functional
  Real computeF(GridBase<Real>& variable, GridBase<Real>& dual) const override;
  /// Compute functional gradient
  void computeGradF(GridBase<Real>& variable,
                    GridBase<Real>& gradient) const override;

  /// Add functional to the list
  void addFunctionalTerm(std::shared_ptr<Functional> functional);

  /// Clears the functional list
  void clear();

protected:
  FunctionalList functionals;
};

}  // namespace functional

}  // namespace tamaas

#endif  // META_FUNCTIONAL_HH
