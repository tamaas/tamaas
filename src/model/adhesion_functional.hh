/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef ADHESION_FUNCTIONAL_HH
#define ADHESION_FUNCTIONAL_HH
/* -------------------------------------------------------------------------- */
#include "functional.hh"
#include <map>
/* -------------------------------------------------------------------------- */

namespace tamaas {

namespace functional {

/// Functional class for adhesion energy
class AdhesionFunctional : public Functional {
protected:
  /// Constructor
  AdhesionFunctional(const GridBase<Real>& surface) {
    this->surface.wrap(surface);
  }

public:
  /// Get parameters
  const std::map<std::string, Real>& getParameters() const {
    return parameters;
  }
  /// Set parameters
  void setParameters(std::map<std::string, Real> other) {
    parameters = std::move(other);
  }

protected:
  GridBase<Real> surface;
  std::map<std::string, Real> parameters;
};

/// Exponential adhesion functional
class ExponentialAdhesionFunctional : public AdhesionFunctional {
public:
  /// Explicit declaration of constructor for swig
  ExponentialAdhesionFunctional(const GridBase<Real>& surface)
      : AdhesionFunctional(surface) {}
  /// Compute the total adhesion energy
  Real computeF(GridBase<Real>& gap, GridBase<Real>& pressure) const override;
  /// Compute the gradient of the adhesion functional
  void computeGradF(GridBase<Real>& gap,
                    GridBase<Real>& gradient) const override;
};

/// Constant adhesion functional
class MaugisAdhesionFunctional : public AdhesionFunctional {
public:
  /// Explicit declaration of constructor for swig
  MaugisAdhesionFunctional(const GridBase<Real>& surface)
      : AdhesionFunctional(surface) {}
  /// Compute the total adhesion energy
  Real computeF(GridBase<Real>& gap, GridBase<Real>& pressure) const override;
  /// Compute the gradient of the adhesion functional
  void computeGradF(GridBase<Real>& gap,
                    GridBase<Real>& gradient) const override;
};

/// Squared exponential adhesion functional
class SquaredExponentialAdhesionFunctional : public AdhesionFunctional {
public:
  /// Explicit declaration of constructor for swig
  SquaredExponentialAdhesionFunctional(const GridBase<Real>& surface)
      : AdhesionFunctional(surface) {}
  /// Compute the total adhesion energy
  Real computeF(GridBase<Real>& gap, GridBase<Real>& pressure) const override;
  /// Compute the gradient of the adhesion functional
  void computeGradF(GridBase<Real>& gap,
                    GridBase<Real>& gradient) const override;
};

}  // namespace functional

}  // namespace tamaas

#endif  // ADHESION_FUNCTIONAL_HH
