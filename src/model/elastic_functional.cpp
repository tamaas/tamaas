/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "elastic_functional.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

namespace functional {

Real ElasticFunctionalPressure::computeF(GridBase<Real>& pressure,
                                         GridBase<Real>& dual) const {
  // This doesn't work if dual is multi-component
  *buffer = dual;
  // this can get large
  const unsigned long long n = dual.getGlobalNbPoints();
  return (pressure.dot(*buffer) / 2 - pressure.dot(surface)) / n;
}

/* -------------------------------------------------------------------------- */

void ElasticFunctionalPressure::computeGradF(GridBase<Real>& pressure,
                                             GridBase<Real>& gradient) const {
  this->op.apply(pressure, *buffer);
  // This doesn't work if dual is multi-component
  *buffer -= surface;
  gradient += *buffer;
}

/* -------------------------------------------------------------------------- */

Real ElasticFunctionalGap::computeF(GridBase<Real>& gap,
                                    GridBase<Real>& dual) const {
  // This doesn't work if dual is multi-component
  *buffer = gap;
  *buffer += surface;
  const UInt n = dual.getGlobalNbPoints();
  const Real F = 0.5 * buffer->dot(dual);
  return F / n;  // avoid integer overflow
}

/* -------------------------------------------------------------------------- */

void ElasticFunctionalGap::computeGradF(GridBase<Real>& gap,
                                        GridBase<Real>& gradient) const {
  *buffer = gap;
  *buffer += surface;  // Now we have displacements
  this->op.apply(*buffer, *buffer);
  gradient += *buffer;
}

}  // namespace functional

}  // namespace tamaas
