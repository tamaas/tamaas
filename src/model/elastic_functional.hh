/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef ELASTIC_FUNCTIONAL_HH
#define ELASTIC_FUNCTIONAL_HH
/* -------------------------------------------------------------------------- */
#include "functional.hh"
#include "model.hh"
#include "tamaas.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

namespace functional {

/// Generic functional for elastic energy
class ElasticFunctional : public Functional {
public:
  ElasticFunctional(const IntegralOperator& op, const GridBase<Real>& surface)
      : op(op), surface() {
    this->surface.wrap(surface);
    const Model& model = op.getModel();
    buffer = allocateGrid<true, Real>(op.getType(),
                                      model.getBoundaryDiscretization());
  }

protected:
  const IntegralOperator& op;
  GridBase<Real> surface;
  std::unique_ptr<GridBase<Real>> mutable buffer;
};

/* -------------------------------------------------------------------------- */

/// Functional with pressure as primal field
class ElasticFunctionalPressure : public ElasticFunctional {
public:
  using ElasticFunctional::ElasticFunctional;
  /// Compute functional with input pressure
  Real computeF(GridBase<Real>& pressure, GridBase<Real>& dual) const override;
  /// Compute functional gradient with input pressure
  void computeGradF(GridBase<Real>& pressure,
                    GridBase<Real>& gradient) const override;
};

/* -------------------------------------------------------------------------- */

/// Functional with gap as primal field
class ElasticFunctionalGap : public ElasticFunctional {
public:
  using ElasticFunctional::ElasticFunctional;
  /// Compute functional with input gap
  Real computeF(GridBase<Real>& gap, GridBase<Real>& dual) const override;
  /// Compute functional gradient with input gap
  void computeGradF(GridBase<Real>& gap,
                    GridBase<Real>& gradient) const override;
};

}  // namespace functional

}  // namespace tamaas
/* -------------------------------------------------------------------------- */
#endif  // ELASTIC_FUNCTIONAL_HH
