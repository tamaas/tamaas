/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "mindlin.hh"
#include "boussinesq_helper.hh"
#include "influence.hh"
#include "kelvin_helper.hh"
#include "model_type.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */
template <model_type type, UInt derivative>
Mindlin<type, derivative>::Mindlin(Model* model) : parent(model) {
  surface_tractions.setNbComponents(trait::components);
  surface_tractions.resize(this->source_buffer.front().sizes());
}

/* -------------------------------------------------------------------------- */
template <model_type type, UInt derivative>
void Mindlin<type, derivative>::applyIf(GridBase<Real>& source,
                                        GridBase<Real>& out,
                                        filter_t pred) const {
  Real nu = this->model->getPoissonRatio(), mu = this->model->getShearModulus();
  influence::Kelvin<trait::dimension, 2> kelvin_strain(mu, nu);
  influence::ElasticHelper<trait::dimension> elasticity(mu, nu);
  detail::SurfaceTractionHelper<type> thelper;

  const Real L = this->model->getSystemSize().front();

  parent::transformSource(source, pred);

  // Reset buffer values
  for (auto&& layer : this->out_buffer)
    layer = 0;

  // computing surface tractions (q power cancelled with
  // boussinesq)
  surface_tractions = 0;
  thelper.template computeSurfaceTractions<true>(
      this->source_buffer, surface_tractions, this->wavevectors, L,
      kelvin_strain, elasticity);
  surface_tractions *= -1;

  if (this->method == integration_method::linear)
    linearIntegral(out);
  else
    cutoffIntegral(out);
}

template <model_type type, UInt derivative>
void Mindlin<type, derivative>::linearIntegral(GridBase<Real>& out) const {
  Real nu = this->model->getPoissonRatio(), mu = this->model->getShearModulus();
  Real L = this->model->getSystemSize().front();

  influence::Kelvin<trait::dimension, derivative> kelvin(mu, nu);
  influence::Boussinesq<trait::dimension, derivative - 1> boussinesq(mu, nu);
  influence::ElasticHelper<trait::dimension> elasticity(mu, nu);

  detail::KelvinHelper<type, decltype(kelvin)> helper;
  detail::SurfaceTractionHelper<type> thelper;
  detail::BoussinesqHelper<type, decltype(boussinesq)> bhelper;

  // apply kelvin
  helper.applyIntegral(this->source_buffer, this->out_buffer, this->wavevectors,
                       L, kelvin);
  helper.applyFreeTerm(this->source_buffer, this->out_buffer, kelvin);
  // no need for fundamental correction

  // apply boussinesq (q power cancelled with mindlin 2-grad)
  bhelper.template apply<true>(surface_tractions, this->out_buffer,
                               this->wavevectors, L, boussinesq);

  // Correcting for fundamental mode
  Vector<Real, trait::dimension> n{{{0, 0, -1}}};

  for (UInt i : Loop::range(this->source_buffer.size())) {
    typename detail::KelvinTrait<decltype(kelvin)>::source_t w(
        this->source_buffer[i](0));
    typename detail::KelvinTrait<decltype(kelvin)>::out_t u(
        this->out_buffer[i](0));

    auto t = dense(w) * n;
    bhelper.makeFundamentalModeGreatAgain(t, u, elasticity);
  }

  parent::transformOutput(
      [](auto&& out_buffer, auto layer) ->
      typename parent::BufferType& { return out_buffer[layer]; },
      out);
}

template <model_type type, UInt derivative>
void Mindlin<type, derivative>::cutoffIntegral(GridBase<Real>& out) const {
  Real nu = this->model->getPoissonRatio(), mu = this->model->getShearModulus();
  Real L = this->model->getSystemSize().front();

  influence::Kelvin<trait::dimension, derivative> kelvin(mu, nu);
  influence::Boussinesq<trait::dimension, derivative - 1> boussinesq(mu, nu);
  influence::ElasticHelper<trait::dimension> elasticity(mu, nu);

  detail::KelvinHelper<type, decltype(kelvin)> helper;
  detail::SurfaceTractionHelper<type> thelper;
  detail::BoussinesqHelper<type, decltype(boussinesq)> bhelper;

  Vector<Real, trait::dimension> n{{{0, 0, -1}}};

  auto func = [&](auto&& out_buffer, auto layer) ->
      typename parent::BufferType& {
    auto&& out = out_buffer.front();

    // Reset previous values
    out = 0;

    // Apply kelvin
    helper.applyIntegral(this->source_buffer, out, layer, this->wavevectors, L,
                         this->cutoff, kelvin);
    helper.applyFreeTerm(this->source_buffer[layer], out, kelvin);
    helper.makeFundamentalGreatAgain(out);

    // Apply boussinesq to cancel surface tractions
    bhelper.template apply<true>(surface_tractions, out, layer,
                                 this->wavevectors, this->source_buffer.size(),
                                 L, boussinesq);

    // Take care of fundamental mode
    typename detail::KelvinTrait<decltype(kelvin)>::source_t w(
        this->source_buffer[layer](0));
    typename detail::KelvinTrait<decltype(kelvin)>::out_t u(out(0));

    auto t = dense(w) * n;
    bhelper.makeFundamentalModeGreatAgain(t, u, elasticity);

    return out;
  };
  parent::transformOutput(func, out);
}

/* -------------------------------------------------------------------------- */
/* Template instanciation                                                     */
/* -------------------------------------------------------------------------- */
template class Mindlin<model_type::volume_2d, 1>;
template class Mindlin<model_type::volume_2d, 2>;
}  // namespace tamaas
