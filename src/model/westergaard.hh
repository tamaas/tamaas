/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef WESTERGAARD_HH
#define WESTERGAARD_HH
/* -------------------------------------------------------------------------- */
#include "fft_engine.hh"
#include "grid_hermitian.hh"
#include "integral_operator.hh"
#include "model_type.hh"
#include "tamaas.hh"
/* -------------------------------------------------------------------------- */

namespace tamaas {

/**
 * @brief Operator based on Westergaard solution and the Dicrete Fourier
 * Transform.
 * This class is templated with model type to allow efficient storage of the
 * influence coefficients.
 * The integral operator is only applied to surface pressure/displacements,
 * even for volume models.
 */
template <model_type mtype, IntegralOperator::kind otype>
class Westergaard : public IntegralOperator {
  using trait = model_type_traits<mtype>;
  static constexpr UInt dim = trait::dimension;
  static constexpr UInt bdim = trait::boundary_dimension;
  static constexpr UInt comp = trait::components;

public:
  /// Constuctor: initalizes influence coefficients and allocates buffer
  Westergaard(Model* model);

  /// Get influence coefficients
  const GridHermitian<Real, bdim>& getInfluence() const { return *influence; }

  /// Apply influence coefficients to input
  void apply(GridBase<Real>& input, GridBase<Real>& output) const override;

  /// Update the influence coefficients
  void updateFromModel() override { initInfluence(); }

  /// Kind
  IntegralOperator::kind getKind() const override { return otype; }

  /// Type
  model_type getType() const override { return mtype; }

  /// Initialize influence coefficients
  void initInfluence();

  template <typename Functor>
  void initFromFunctor(Functor func);

  /// Apply a functor in Fourier space
  template <typename Functor>
  void fourierApply(Functor func, GridBase<Real>& in,
                    GridBase<Real>& out) const;

  /// Compute L_2 norm of influence functions
  Real getOperatorNorm() override;

  /// Dense shape
  std::pair<UInt, UInt> matvecShape() const override;

  /// Dense matvec
  GridBase<Real> matvec(GridBase<Real>& x) const override;

public:
  std::shared_ptr<GridHermitian<Real, bdim>> influence;
  mutable GridHermitian<Real, bdim> buffer;
  mutable std::unique_ptr<FFTEngine> engine;
};

}  // namespace tamaas

#endif  // WESTERGAARD_HH
