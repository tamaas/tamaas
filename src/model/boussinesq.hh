/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef BOUSSINESQ_HH
#define BOUSSINESQ_HH
/* -------------------------------------------------------------------------- */
#include "grid_hermitian.hh"
#include "model_type.hh"
#include "volume_potential.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

/**
 * @brief Boussinesq tensor
 */
template <model_type type, UInt derivative>
class Boussinesq : public VolumePotential<type> {
  static_assert(type == model_type::volume_1d || type == model_type::volume_2d,
                "Only volume types are supported");
  using trait = model_type_traits<type>;
  using parent = VolumePotential<type>;

public:
  /// Constructor
  Boussinesq(Model* model);

protected:
  void initialize(UInt source_components, UInt out_components);

public:
  /// Apply the Boussinesq operator
  void apply(GridBase<Real>& source, GridBase<Real>& out) const override;
};

}  // namespace tamaas

#endif  // BOUSSINESQ_HH
