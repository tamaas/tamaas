/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef LINEAR_ELASTIC_HH
#define LINEAR_ELASTIC_HH
/* -------------------------------------------------------------------------- */
#include "material.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

class LinearElastic : public Material {
  using parent = Material;
  using Field = parent::Field;
  using trait = model_type_traits<model_type::volume_2d>;
  static constexpr UInt dim = trait::dimension;
  static constexpr UInt comp = trait::voigt;

public:
  LinearElastic(Model* model, std::string operator_name);

  void computeStress(Field& stress, const Field& strain,
                     const Field& strain_increment) override;
  void computeEigenStress(Field& stress, const Field& strain,
                          const Field& strain_increment) override;
  void applyTangent(Field& output, const Field& input, const Field& strain,
                    const Field& strain_increment) override;
  void update() override;

protected:
  std::string operator_name;
};

}  // namespace tamaas

#endif
