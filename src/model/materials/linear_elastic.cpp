/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "linear_elastic.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

LinearElastic::LinearElastic(Model* model, std::string op_name)
    : Material(model), operator_name(std::move(op_name)) {
  TAMAAS_ASSERT(
      this->model->getIntegralOperator(operator_name)->getKind() ==
          IntegralOperator::dirac,
      "Linear operator for linear elastic material should be the dirac kind");
}

void LinearElastic::computeStress(Field& stress, const Field& strain,
                                  const Field& strain_increment) {
  stress = strain;
  stress += strain_increment;
  Grid<Real, dim> stressv{model->getDiscretization(), comp, stress.view()};
  this->model->getIntegralOperator(operator_name)->apply(stressv, stressv);
}

void LinearElastic::computeEigenStress(Field& stress, const Field& /*strain*/,
                                       const Field& /*strain_increment*/) {
  stress = 0;
}

void LinearElastic::applyTangent(Field& output, const Field& input,
                                 const Field& /*strain*/,
                                 const Field& /*strain_increment*/) {
  auto input_nc{const_cast<Field&>(input)};
  Grid<Real, dim> inputv{model->getDiscretization(), comp, input_nc.view()},
      outputv{model->getDiscretization(), comp, output.view()};
  this->model->getIntegralOperator(operator_name)->apply(inputv, outputv);
}

void LinearElastic::update() {}

}  // namespace tamaas
