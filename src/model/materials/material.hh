/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef MATERIAL_HH
#define MATERIAL_HH
/* -------------------------------------------------------------------------- */
#include "grid.hh"
#include "model.hh"
#include "model_type.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

class Material {
  // Defining some helper types
protected:
  using Field = GridBase<Real>;

public:
  /// Constructor
  Material(Model* model) : model(model) {}
  /// Destructor
  virtual ~Material() = default;

  // Material Interface
public:
  /// Compute the stress from total strain and strain increment
  virtual void computeStress(Field& stress, const Field& strain,
                             const Field& strain_increment) = 0;

  /// Compute stress due to inelastic increment
  virtual void computeEigenStress(Field& stress, const Field& strain,
                                  const Field& strain_increment) = 0;

  /// Update internal variables
  virtual void update() = 0;

  /// Applt consistent tangent
  virtual void applyTangent(Field& /*out*/, const Field& /*in*/,
                            const Field& /*strain*/,
                            const Field& /*strain_increment*/) {
    throw not_implemented_error{
        TAMAAS_MSG("consistent tangent not implemented")};
  }

protected:
  Model* model;
};

}  // namespace tamaas
#endif  // MATERIAL_HH
