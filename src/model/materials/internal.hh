/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef INTERNAL_HH
#define INTERNAL_HH
/* -------------------------------------------------------------------------- */
#include "grid.hh"
#include <memory>
/* -------------------------------------------------------------------------- */
namespace tamaas {

template <typename T, UInt dim>
struct Internal {
  /// Stored field
  std::shared_ptr<Grid<T, dim>> field;

  /// Initialize previous field
  void initialize() {
    previous = std::make_shared<Grid<T, dim>>(field->sizes(),
                                              field->getNbComponents());
    update();
  }

  /// Update the field previous state
  void update() { *previous = *field; }
  /// Reset the current field
  void reset() { *field = *previous; }

  /// Assign the field pointer
  void operator=(decltype(field)&& field) { this->field = std::move(field); }
  /// Dereference the field pointer
  typename decltype(field)::element_type& operator*() { return *field; }
  /// Dereference the field pointer (const version)
  const typename decltype(field)::element_type& operator*() const {
    return *field;
  }
  /// Upcast to GridBase pointer
  operator std::shared_ptr<GridBase<T>>() { return field; }

public:
  decltype(field) previous;
};

}  // namespace tamaas
#endif  // INTERNAL_HH
