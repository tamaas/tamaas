/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "model_template.hh"
#include "computes.hh"
#include "hooke.hh"
#include "influence.hh"
#include "kelvin.hh"
#include "partitioner.hh"
#include "westergaard.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

namespace detail {
template <class Compute_t>
class ComputeOperator : public IntegralOperator {
public:
  /// Constructor
  ComputeOperator(Model* model) : IntegralOperator(model) {}

  IntegralOperator::kind getKind() const override {
    return IntegralOperator::dirac;
  }
  model_type getType() const override { return model->getType(); }
  void updateFromModel() override {}

  /// Apply functor
  void apply(GridBase<Real>& in, GridBase<Real>& out) const override {
    applyCompute<Compute_t>(model->getType(), out, in);
  }
};
}  // namespace detail

template <model_type type>
ModelTemplate<type>::ModelTemplate(std::vector<Real> system_size,
                                   std::vector<UInt> discretization)
    : Model(std::move(system_size), std::move(discretization)) {
  constexpr UInt dim = trait::dimension;
  constexpr UInt dim_b = trait::boundary_dimension;
  constexpr UInt nb_components = trait::components;

  TAMAAS_ASSERT(this->system_size.size() == dim,
                "System size does not match model type");
  TAMAAS_ASSERT(this->discretization.size() == dim,
                "Discretization size does not match model type");

  // Copying sizes for traction grid
  std::array<UInt, dim_b> traction_size;
  auto disc_it = this->discretization.begin() + (dim > dim_b);
  std::copy_n(disc_it, dim_b, traction_size.begin());

  // Adjust MPI sizes
  traction_size = Partitioner<dim_b>::local_size(traction_size);
  *disc_it = traction_size.front();

  // Allocating
  (*this)["traction"] =
      allocateGrid<type, true, Real>(traction_size, nb_components);
  (*this)["displacement"] =
      allocateGrid<type, false, Real>(getDiscretization(), nb_components);

  this->initializeBEEngine();
  registerIntegralOperator<Hooke<type>>("hooke");
  registerIntegralOperator<detail::ComputeOperator<compute::Eigenvalues>>(
      "eigenvalues");
  registerIntegralOperator<detail::ComputeOperator<compute::VonMises>>(
      "von_mises");
  registerIntegralOperator<detail::ComputeOperator<compute::Deviatoric>>(
      "deviatoric");
}

/* -------------------------------------------------------------------------- */
template <model_type type>
void ModelTemplate<type>::initializeBEEngine() {
  engine = std::make_unique<BEEngineTmpl<type>>(this);
}

/* -------------------------------------------------------------------------- */
template <model_type type>
void ModelTemplate<type>::setIntegrationMethod(integration_method method,
                                               Real cutoff) {
  if (type != model_type::volume_2d)
    return;

#define CAST(derivative, ptr)                                                  \
  do {                                                                         \
    auto* casted =                                                             \
        dynamic_cast<Kelvin<model_type::volume_2d, derivative>*>(ptr);         \
    if (casted != nullptr) {                                                   \
      casted->setIntegrationMethod(method, cutoff);                            \
      return;                                                                  \
    }                                                                          \
  } while (0)

  for (auto&& pair : this->operators) {
    auto* ptr = pair.second.get();
    CAST(0, ptr);
    CAST(1, ptr);
    CAST(2, ptr);
  }
#undef CAST
}

namespace {
template <typename T>
std::vector<T> extract(const std::vector<T>& v, UInt n) {
  std::vector<T> ex(v.size() - n);
  std::copy(v.begin() + n, v.end(), ex.begin());
  return ex;
}
}  // namespace

/// \cond DO_NOT_DOCUMENT
template <model_type type>
std::vector<UInt> ModelTemplate<type>::getGlobalDiscretization() const {
  auto global_bdisc = Partitioner<trait::boundary_dimension>::global_size(
      getBoundaryDiscretization());
  if (trait::dimension != trait::boundary_dimension)
    global_bdisc.insert(global_bdisc.begin(), getDiscretization().front());
  return global_bdisc;
}

template <model_type type>
std::vector<UInt> ModelTemplate<type>::getBoundaryDiscretization() const {
  return getDiscretization();
}

template <>
std::vector<UInt>
ModelTemplate<model_type::volume_1d>::getBoundaryDiscretization() const {
  return extract(getDiscretization(), 1);
}

template <>
std::vector<UInt>
ModelTemplate<model_type::volume_2d>::getBoundaryDiscretization() const {
  return extract(getDiscretization(), 1);
}

template <model_type type>
std::vector<Real> ModelTemplate<type>::getBoundarySystemSize() const {
  return getSystemSize();
}

template <>
std::vector<Real>
ModelTemplate<model_type::volume_1d>::getBoundarySystemSize() const {
  return extract(getSystemSize(), 1);
}

template <>
std::vector<Real>
ModelTemplate<model_type::volume_2d>::getBoundarySystemSize() const {
  return extract(getSystemSize(), 1);
}
/// \endcond

/* -------------------------------------------------------------------------- */
/* Template instanciation                                                     */
/* -------------------------------------------------------------------------- */
template class ModelTemplate<model_type::basic_1d>;
template class ModelTemplate<model_type::basic_2d>;
template class ModelTemplate<model_type::surface_1d>;
template class ModelTemplate<model_type::surface_2d>;
template class ModelTemplate<model_type::volume_1d>;
template class ModelTemplate<model_type::volume_2d>;

}  // namespace tamaas
