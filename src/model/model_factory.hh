/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef MODEL_FACTORY_HH
#define MODEL_FACTORY_HH
/* -------------------------------------------------------------------------- */
#include "be_engine.hh"
#include "model.hh"
#include "model_type.hh"
#include "residual.hh"
#include <vector>
/* -------------------------------------------------------------------------- */

namespace tamaas {

/**
 * @brief Factory class for model
 */
class ModelFactory {
public:
  /// Create new model
  static std::unique_ptr<Model>
  createModel(model_type type, const std::vector<Real>& system_size,
              const std::vector<UInt>& discretization);

  /// Make a deep copy of existing model
  static std::unique_ptr<Model> createModel(const Model& model);

  /// Create a plasticity residual
  static std::unique_ptr<Residual> createResidual(Model& model, Real sigma_y,
                                                  Real hardening);

  /// Register volume integral operators in a model
  static void registerVolumeOperators(Model& m);

  static void registerNonPeriodic(Model& m, std::string name);

  /// Set integration method for a volume integral operator
  static void setIntegrationMethod(IntegralOperator& op,
                                   integration_method method, Real cutoff);
};

}  // namespace tamaas

#endif  // MODEL_FACTORY_HH
