/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "be_engine.hh"
#include "logger.hh"
#include "model.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

void solve(IntegralOperator::kind kind,
           const std::map<IntegralOperator::kind,
                          std::shared_ptr<IntegralOperator>>& operators,
           GridBase<Real>& input, GridBase<Real>& output) try {
  operators.at(kind)->apply(input, output);
} catch (std::out_of_range& e) {
  Logger().get(LogLevel::warning)
      << "Operator ("
      << "Westergaard::" << kind << ") not registered\n";
  throw e;
}

template <model_type mtype, IntegralOperator::kind otype>
void registerWestergaardOperator(
    std::map<IntegralOperator::kind, std::shared_ptr<IntegralOperator>>&
        operators,
    Model& model) {
  std::stringstream sstr;
  sstr << "Westergaard::" << otype;
  if (operators.find(otype) == operators.end())
    operators[otype] =
        model.template registerIntegralOperator<Westergaard<mtype, otype>>(
            sstr.str());
}

template <model_type type>
void BEEngineTmpl<type>::solveNeumann(GridBase<Real>& neumann,
                                      GridBase<Real>& dirichlet) const {
  solve(IntegralOperator::neumann, this->operators, neumann, dirichlet);
}

template <model_type type>
void BEEngineTmpl<type>::solveDirichlet(GridBase<Real>& dirichlet,
                                        GridBase<Real>& neumann) const {
  solve(IntegralOperator::dirichlet, this->operators, dirichlet, neumann);
}

template <model_type type>
void BEEngineTmpl<type>::registerNeumann() {
  Logger().get(LogLevel::debug)
      << TAMAAS_MSG("Registering Neumann Westergaard");
  registerWestergaardOperator<type, IntegralOperator::neumann>(this->operators,
                                                               *this->model);
}
/// Register dirichlet operator
template <model_type type>
void BEEngineTmpl<type>::registerDirichlet() {
  Logger().get(LogLevel::debug)
      << TAMAAS_MSG("Registering Dirichlet Westergaard");
  registerWestergaardOperator<type, IntegralOperator::dirichlet>(
      this->operators, *this->model);
}

template class BEEngineTmpl<model_type::basic_1d>;
template class BEEngineTmpl<model_type::basic_2d>;
template class BEEngineTmpl<model_type::surface_1d>;
template class BEEngineTmpl<model_type::surface_2d>;
template class BEEngineTmpl<model_type::volume_1d>;
template class BEEngineTmpl<model_type::volume_2d>;

/* -------------------------------------------------------------------------- */
}  // namespace tamaas
