/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef ELEMENT_HH
#define ELEMENT_HH
/* -------------------------------------------------------------------------- */
#include "tamaas.hh"
#include <expolit/expolit>
/* -------------------------------------------------------------------------- */
namespace tamaas {

template <UInt interpolation_order>
struct ExponentialElement;

template <>
struct ExponentialElement<1> {

  template <UInt shape>
  static constexpr __device__ __host__ expolit::Polynomial<Real, 1> shapes() {
    constexpr expolit::Polynomial<Real, 1> z({0, 1});

    if (shape == 0)
      return 0.5 + (-0.5) * z;
    return 0.5 + 0.5 * z;
  }

  static constexpr __device__ __host__ auto sign(bool upper) {
    return (upper) ? -1 : 1;
  }

  template <bool upper, UInt shape>
  static constexpr __device__ __host__ auto g0(Real q) {
    constexpr expolit::Polynomial<Real, 1> z({0, 1});
    return expolit::exp((q * sign(upper)) * z) * shapes<shape>();
  }

  template <bool upper, UInt shape>
  static constexpr __device__ __host__ auto g1(Real q) {
    constexpr expolit::Polynomial<Real, 1> z({0, 1});
    return expolit::exp((q * sign(upper)) * z) * (q * z * shapes<shape>());
  }

};  // namespace tamaas
}  // namespace tamaas

#endif  // ELEMENT_HH
