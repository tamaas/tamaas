/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef BE_ENGINE_HH
#define BE_ENGINE_HH
/* -------------------------------------------------------------------------- */
#include "integral_operator.hh"
#include "model_type.hh"
#include "westergaard.hh"
/* -------------------------------------------------------------------------- */

namespace tamaas {

/**
 * @brief Boundary equation engine class. Solves the Neumann/Dirichlet problem
 * This class should be dimension and model-type agnostic.
 */
class BEEngine {
public:
  BEEngine(Model* model) : model(model) {}
  /// Destructor
  virtual ~BEEngine() = default;

public:
  /// Solve Neumann problem (expects boundary data)
  virtual void solveNeumann(GridBase<Real>& neumann,
                            GridBase<Real>& dirichlet) const = 0;
  /// Solve Dirichlet problem (expects boundary data)
  virtual void solveDirichlet(GridBase<Real>& dirichlet,
                              GridBase<Real>& neumann) const = 0;

  /// Register neumann operator
  virtual void registerNeumann() = 0;
  /// Register dirichlet operator
  virtual void registerDirichlet() = 0;

  /// Get model
  const Model& getModel() const { return *model; }

  /// Compute L_2 norm of influence functions
  Real getNeumannNorm() {
    return operators[IntegralOperator::neumann]->getOperatorNorm();
  }

protected:
  Model* model;
  std::map<IntegralOperator::kind, std::shared_ptr<IntegralOperator>> operators;
};

template <model_type type>
class BEEngineTmpl : public BEEngine {
public:
  BEEngineTmpl(Model* model) : BEEngine(model) {}

  void solveNeumann(GridBase<Real>& neumann,
                    GridBase<Real>& dirichlet) const override;
  void solveDirichlet(GridBase<Real>& dirichlet,
                      GridBase<Real>& neumann) const override;
  /// Register neumann operator
  void registerNeumann() override;
  /// Register dirichlet operator
  void registerDirichlet() override;
};

}  // namespace tamaas

#endif  // BE_ENGINE_HH
