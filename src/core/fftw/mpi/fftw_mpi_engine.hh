/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef FFTW_MPI_ENGINE_HH
#define FFTW_MPI_ENGINE_HH
/* -------------------------------------------------------------------------- */
#include "fftw/fftw_engine.hh"
#include "fftw/interface.hh"
#include "grid.hh"
#include "grid_hermitian.hh"
#include <map>
/* -------------------------------------------------------------------------- */
namespace tamaas {

class FFTWMPIEngine : public FFTWEngine {
public:
  using FFTWEngine::FFTWEngine;

  void forward(const Grid<Real, 1>& /*real*/,
               GridHermitian<Real, 1>& /*spectral*/) override {
    throw not_implemented_error{
        TAMAAS_MSG("FFTW/MPI does not support 1D transforms")};
  }
  void backward(Grid<Real, 1>& /*real*/,
                GridHermitian<Real, 1>& /*spectral*/) override {
    throw not_implemented_error{
        TAMAAS_MSG("FFTW/MPI does not support 1D transforms")};
  }

  /// FFTW/MPI forward (r2c) transform
  void forward(const Grid<Real, 2>& real,
               GridHermitian<Real, 2>& spectral) override;
  /// FFTW/MPI backward (c2r) transform
  void backward(Grid<Real, 2>& real, GridHermitian<Real, 2>& spectral) override;

protected:
  /// Make a transform signature from a pair of grids
  static key_t make_key(const Grid<Real, 2>& real,
                        const GridHermitian<Real, 2>& spectral);
  /// Return the plans pair for a given transform signature
  plan_t& getPlans(key_t key);
  /// Get FFTW local sizes from an hermitian grid
  static auto local_size(const key_t& key);

protected:
  /// Buffer for real data because of FFTW/MPI layout
  std::map<key_t, Grid<Real, 2>> workspaces;
};

}  // namespace tamaas
#endif  // FFT_MPI_ENGINE_HH
