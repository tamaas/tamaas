/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef FFTW_ENGINE_HH
#define FFTW_ENGINE_HH
/* -------------------------------------------------------------------------- */
#include "fft_engine.hh"
#include "fftw/interface.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

class FFTWEngine : public FFTEngine {
protected:
  using plan_t = std::pair<fftw::plan<Real>, fftw::plan<Real>>;
  using complex_t = fftw::helper<Real>::complex;

  /// Perform forward (R2C) transform
  template <UInt dim>
  void forwardImpl(const Grid<Real, dim>& real,
                   GridHermitian<Real, dim>& spectral);
  /// Perform backward (C2R) transform
  template <UInt dim>
  void backwardImpl(Grid<Real, dim>& real,
                    const GridHermitian<Real, dim>& spectral);
  /// Return the plans pair for a given transform signature
  plan_t& getPlans(key_t key);

public:
  /// Initialize with flags
  explicit FFTWEngine(unsigned int flags = FFTW_ESTIMATE) noexcept
      : _flags(flags) {}

  void forward(const Grid<Real, 1>& real,
               GridHermitian<Real, 1>& spectral) override {
    forwardImpl(real, spectral);
  }

  void forward(const Grid<Real, 2>& real,
               GridHermitian<Real, 2>& spectral) override {
    forwardImpl(real, spectral);
  }

  void backward(Grid<Real, 1>& real,
                GridHermitian<Real, 1>& spectral) override {
    backwardImpl(real, spectral);
  }

  void backward(Grid<Real, 2>& real,
                GridHermitian<Real, 2>& spectral) override {
    backwardImpl(real, spectral);
  }

  unsigned int flags() const { return _flags; }

  /// Cast to FFTW complex type
  static auto cast(Complex* data) { return reinterpret_cast<complex_t*>(data); }
  static auto cast(const Complex* data) {
    return const_cast<complex_t*>(reinterpret_cast<const complex_t*>(data));
  }

protected:
  unsigned int _flags;            ///< FFTW flags
  std::map<key_t, plan_t> plans;  ///< plans corresponding to signatures
};

/* -------------------------------------------------------------------------- */
template <UInt dim>
void FFTWEngine::forwardImpl(const Grid<Real, dim>& real,
                             GridHermitian<Real, dim>& spectral) {
  auto& plans = getPlans(make_key(real, spectral));
  fftw::execute(plans.first, const_cast<Real*>(real.getInternalData()),
                cast(spectral.getInternalData()));
}

template <UInt dim>
void FFTWEngine::backwardImpl(Grid<Real, dim>& real,
                              const GridHermitian<Real, dim>& spectral) {
  auto& plans = getPlans(make_key(real, spectral));
  fftw::execute(plans.second, cast(spectral.getInternalData()),
                real.getInternalData());
  // Normalize
  real *= (1. / real.getNbPoints());
}

}  // namespace tamaas
#endif
