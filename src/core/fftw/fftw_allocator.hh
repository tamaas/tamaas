/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef FFTW_ALLOCATOR_HH
#define FFTW_ALLOCATOR_HH
/* -------------------------------------------------------------------------- */
#include "span.hh"
#include <fftw3.h>
#include <memory>
/* -------------------------------------------------------------------------- */

namespace tamaas {

/// Class allocating
/// [SIMD](http://www.fftw.org/fftw3_doc/SIMD-alignment-and-fftw_005fmalloc.html#SIMD-alignment-and-fftw_005fmalloc)
/// aligned memory
template <typename T>
struct FFTWAllocator {
  /// Allocate memory
  static span<T> allocate(typename span<T>::size_type n) noexcept {
    T* p = nullptr;
    p = static_cast<T*>(fftw_malloc(sizeof(T) * n));
    return span<T>{p, n};
  }

  /// Free memory
  static void deallocate(span<T> view) noexcept { fftw_free(view.data()); }
};

}  // namespace tamaas

#endif  // FFTW_ALLOCATOR_HH
