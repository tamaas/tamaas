/**
 *  @mainpage Tamaas - A high-performance periodic contact library
 *
 *  @section Introduction
 *  Tamaas is a spectral-integral-equation based contact library. It is made
 *  with love to be fast and friendly!
 *
 *  @author Lucas Frérot <lucas.frerot@sorbonne-universite.fr>
 *  @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *  @author Valentine Rey <valentine.rey@univ-nantes.fr>
 *  @author Son Pham-Ba <son.phamba@epfl.ch>
 *  @author Zichen Li <zichen.li@dalembert.upmc.fr>
 *  @author Jean-François Molinari <jean-francois.molinari@epfl.ch>
 *
 *  @section License
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef TAMAAS_HH
#define TAMAAS_HH
/* -------------------------------------------------------------------------- */

#ifndef TAMAAS_USE_CUDA
#define TAMAAS_USE_FFTW
#endif

// Values for fftw backends
#define TAMAAS_FFTW_BACKEND_OMP 2
#define TAMAAS_FFTW_BACKEND_THREADS 2
#define TAMAAS_FFTW_BACKEND_NONE 3
// Values for thrust backends
#define TAMAAS_LOOP_BACKEND_OMP 1
#define TAMAAS_LOOP_BACKEND_TBB 2
#define TAMAAS_LOOP_BACKEND_CPP 3
#define TAMAAS_LOOP_BACKEND_CUDA 4
// Default loop backend is CPP / host
#ifndef TAMAAS_LOOP_BACKEND
#define TAMAAS_LOOP_BACKEND TAMAAS_LOOP_BACKEND_CPP
#endif
// Default FFTW backend is none
#ifndef TAMAAS_FFTW_BACKEND
#define TAMAAS_FFTW_BACKEND TAMAAS_FFTW_BACKEND_NONE
#endif
// If the thrust device hasn't been set, set host
#ifndef THRUST_DEVICE_SYSTEM
#define THRUST_DEVICE_SYSTEM THRUST_DEVICE_SYSTEM_CPP
#endif

/// Convenience macros
#define TAMAAS_ACCESSOR(var, type, name)                                       \
  type& get##name() { return var; }                                            \
  void set##name(const type& new_var) { var = new_var; }

/* -------------------------------------------------------------------------- */
// Standard includes
#include <exception>
#include <iostream>
#include <memory>
#include <string>
#include <type_traits>
/* -------------------------------------------------------------------------- */
// Special thrust includes
#include <thrust/complex.h>
#include <thrust/random.h>

/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

/// Cuda specific definitions
#define CUDA_LAMBDA __device__ __host__

/// Common types definitions
// If type macros have not been set, put default values
#ifndef TAMAAS_REAL_TYPE
#define TAMAAS_REAL_TYPE double
#endif
#ifndef TAMAAS_INT_TYPE
#define TAMAAS_INT_TYPE int
#endif

using Real = TAMAAS_REAL_TYPE;           ///< default floating point type
using Int = TAMAAS_INT_TYPE;             ///< default signed integer type
using UInt = std::make_unsigned_t<Int>;  ///< default unsigned integer type

#if THRUST_VERSION == 200300
#error "Tamaas cannot compile with CCCL version 2.3.0"
#endif

template <typename T>
using complex = thrust::complex<T>;  ///< template complex wrapper
using Complex = complex<Real>;       ///< default floating point complex type

/// Defining random toolbox
using ::thrust::random::normal_distribution;
using ::thrust::random::uniform_real_distribution;
using random_engine = ::thrust::random::default_random_engine;

namespace detail {
template <bool acc, template <typename> class Trait, typename Head,
          typename... Tail>
struct fold_trait_tail_rec
    : std::integral_constant<bool,
                             fold_trait_tail_rec<acc and Trait<Head>::value,
                                                 Trait, Tail...>::value> {};

template <bool acc, template <typename> class Trait, typename Head>
struct fold_trait_tail_rec<acc, Trait, Head>
    : std::integral_constant<bool, acc and Trait<Head>::value> {};
}  // namespace detail

template <template <typename> class Trait, typename... T>
struct fold_trait : detail::fold_trait_tail_rec<true, Trait, T...> {};

/* -------------------------------------------------------------------------- */
/// initialize tamaas (0 threads => let OMP_NUM_THREADS decide)
void initialize(UInt num_threads = 0);
/// cleanup tamaas
void finalize();
/* -------------------------------------------------------------------------- */

/// CUDA-compatible exchange function
template <class U, class V = U>
__device__ __host__ U exchange(U& obj, V&& new_value) {
  U old_value = std::move(obj);
  obj = std::forward<V>(new_value);
  return old_value;
}

/// Generic exception class
class Exception : public std::exception {
public:
  /// Constructor
  Exception(std::string mesg) : msg(std::move(mesg)) {}

  const char* what() const noexcept override { return msg.c_str(); }

  ~Exception() override = default;

private:
  std::string msg;  ///< message of exception
};

/* -------------------------------------------------------------------------- */
/// Enumeration of reduction operations
enum class operation { plus, times, min, max };
/* -------------------------------------------------------------------------- */
}  // namespace tamaas
/* -------------------------------------------------------------------------- */

#endif  // TAMAAS_HH
