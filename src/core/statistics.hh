/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef STATISTICS_HH
#define STATISTICS_HH
/* -------------------------------------------------------------------------- */
#include "fft_engine.hh"
#include "grid.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

/**
 * @brief Suitcase class for all statistics related functions
 */
template <UInt dim>
struct Statistics {
  /// Compute hrms
  static Real computeRMSHeights(Grid<Real, dim>& surface);
  /// Compute hrms' in fourier space
  static Real computeSpectralRMSSlope(Grid<Real, dim>& surface);
  /// Compute hrms' with finite differences
  static Real computeFDRMSSlope(Grid<Real, dim>& surface);
  /// Compute PSD of surface
  static GridHermitian<Real, dim>
  computePowerSpectrum(Grid<Real, dim>& surface);
  /// Compute autocorrelation
  static Grid<Real, dim> computeAutocorrelation(Grid<Real, dim>& surface);
  /// Compute spectral moments
  static std::vector<Real> computeMoments(Grid<Real, dim>& surface);
  /// Compute (corrected) contact area fraction
  static Real contact(const Grid<Real, dim>& tractions, UInt perimeter = 0);
  /// Compute the area scaling factor of a periodic graph
  static Real graphArea(const Grid<Real, dim>& displacement);

  using PVector = VectorProxy<Real, dim>;

private:
  template <class T>
  static Real rmsSlopesFromPSD(const GridHermitian<Real, dim>& psd,
                               const Grid<T, dim>& diff);
};

/* -------------------------------------------------------------------------- */
}  // namespace tamaas
#endif  // STATISTICS_HH
