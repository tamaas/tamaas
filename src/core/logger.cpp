/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "logger.hh"
#include "mpi_interface.hh"
#include <cstdlib>
#include <iostream>
#include <map>
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */
LogLevel Logger::current_level = LogLevel::info;

std::ostream& operator<<(std::ostream& o, const LogLevel& val) {
  static const std::map<LogLevel, std::string> strings{
      {LogLevel::debug, "DEBUG"},
      {LogLevel::info, "INFO"},
      {LogLevel::warning, "WARNING"},
      {LogLevel::error, "ERROR"}};

  o << strings.at(val);
  return o;
}

Logger::~Logger() noexcept {
  if (wish_level >= current_level and
      not(mpi::rank() != 0 and wish_level == LogLevel::info)) {
    std::cerr << stream.str();
    std::cerr.flush();
  }
}

std::ostream& Logger::get(LogLevel level) {
  wish_level = level;

  // Print rank if not info level
  if (wish_level != LogLevel::info)
    stream << '[' << mpi::rank() << '|' << mpi::size() << "] "
           << wish_level << ": ";

  return stream;
}

void Logger::setLevel(LogLevel level) { current_level = level; }

/* -------------------------------------------------------------------------- */
}  // namespace tamaas
