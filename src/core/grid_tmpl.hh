/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef GRID_TMPL_HH
#define GRID_TMPL_HH
/* -------------------------------------------------------------------------- */
#include "grid.hh"
#include "tamaas.hh"

namespace tamaas {

template <typename T, UInt dim>
template <typename RandomAccessIterator>
Grid<T, dim>::Grid(RandomAccessIterator begin, RandomAccessIterator end,
                   UInt nb_components)
    : GridBase<T>() {
  TAMAAS_ASSERT(end - begin == dim, "Provided sizes (", end - begin,
                ") for grid do not match dimension (", dim, ")");

  this->nb_components = nb_components;
  this->resize(begin, end);
}

/* -------------------------------------------------------------------------- */

template <typename T, UInt dim>
template <typename RandomAccessIterator>
Grid<T, dim>::Grid(RandomAccessIterator begin, RandomAccessIterator end,
                   UInt nb_components, span<value_type> data)
    : GridBase<T>() {
  TAMAAS_ASSERT(end - begin == dim, "Provided sizes (", end - begin,
                ") for grid do not match dimension (", dim, ")");

  std::copy(begin, end, this->n.begin());
  this->nb_components = nb_components;

  TAMAAS_ASSERT(data.size() == computeSize(), "incompatible wrap span size");

  this->data.wrap(data);
  this->computeStrides();
}

/* -------------------------------------------------------------------------- */

template <typename T, UInt dim>
inline UInt Grid<T, dim>::computeSize() const {
  UInt size = 1;
  for (UInt i = 0; i < dim; i++)
    size *= n[i];
  size *= this->nb_components;
  return size;
}

/* -------------------------------------------------------------------------- */

template <typename T, UInt dim>
template <typename ForwardIt>
void Grid<T, dim>::resize(ForwardIt begin, ForwardIt end) {
  std::copy(begin, end, this->n.begin());
  UInt size = this->computeSize();
  GridBase<T>::resize(size);
  this->computeStrides();
}

/* -------------------------------------------------------------------------- */

template <typename T, UInt dim>
template <typename... T1>
inline UInt Grid<T, dim>::unpackOffset(UInt offset, UInt index_pos, UInt index,
                                       T1... rest) const {
  offset += index * strides[index_pos];
  return unpackOffset(offset, index_pos + 1, rest...);  // tail-rec bb
}

template <typename T, UInt dim>
template <typename... T1>
inline UInt Grid<T, dim>::unpackOffset(UInt offset, UInt index_pos,
                                       UInt index) const {
  return offset + index * strides[index_pos];
}

/* -------------------------------------------------------------------------- */

template <typename T, UInt dim>
template <std::size_t tdim>
inline UInt Grid<T, dim>::computeOffset(std::array<UInt, tdim> tuple) const {
  static_assert(tdim == dim or tdim == dim + 1, "Tuple dimension is invalid");
  return std::inner_product(tuple.begin(), tuple.end(), strides.begin(), 0);
}

template <typename T, UInt dim>
template <std::size_t tdim>
inline T& Grid<T, dim>::operator()(std::array<UInt, tdim> tuple) {
  UInt offset = computeOffset(tuple);
  return this->data[offset];
}

template <typename T, UInt dim>
template <std::size_t tdim>
inline const T& Grid<T, dim>::operator()(std::array<UInt, tdim> tuple) const {
  UInt offset = computeOffset(tuple);
  return this->data[offset];
}

/* -------------------------------------------------------------------------- */

template <typename T, UInt dim>
Grid<T, dim>& Grid<T, dim>::operator=(const Grid<T, dim>& other) {
  this->copy(other);
  return *this;
}

template <typename T, UInt dim>
Grid<T, dim>& Grid<T, dim>::operator=(Grid<T, dim>&& other) noexcept {
  this->move(std::move(other));
  return *this;
}

/* -------------------------------------------------------------------------- */

template <typename T, UInt dim>
template <typename T1>
void Grid<T, dim>::copy(const Grid<T1, dim>& other) {
  GridBase<T>::copy(other);
  this->n = other.n;
  this->strides = other.strides;
}

template <typename T, UInt dim>
template <typename T1>
void Grid<T, dim>::move(Grid<T1, dim>&& other) noexcept {
  GridBase<T>::move(std::move(other));
  this->n = std::move(other.n);
  this->strides = std::move(other.strides);
}

/* -------------------------------------------------------------------------- */
/* Stream operator                                                            */
/* -------------------------------------------------------------------------- */

template <typename T, UInt dim>
inline std::ostream& operator<<(std::ostream& stream,
                                const Grid<T, dim>& _this) {
  _this.printself(stream);
  return stream;
}

}  // namespace tamaas
/* -------------------------------------------------------------------------- */
#endif  // GRID_TMPL_HH
