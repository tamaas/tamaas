/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef TAMAAS_ALLOCATOR_HH
#define TAMAAS_ALLOCATOR_HH

#include "tamaas.hh"

#ifdef TAMAAS_USE_CUDA
#include "cuda/unified_allocator.hh"
#else
#include "fftw/fftw_allocator.hh"
#endif

namespace tamaas {
#ifdef TAMAAS_USE_CUDA
template <typename T>
using Allocator = UnifiedAllocator<T>;
#else
template <typename T>
using Allocator = FFTWAllocator<T>;
#endif
}  // namespace tamaas

#endif  // TAMAAS_ALLOCATOR_HH
