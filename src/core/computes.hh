/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef EIGENVALUES_HH
#define EIGENVALUES_HH
/* -------------------------------------------------------------------------- */
#include "grid.hh"
#include "loop.hh"
#include "model_type.hh"
#include "ranges.hh"
#include "static_types.hh"

namespace tamaas {

namespace compute {
/// Compute eigenvalues of a symmetric matrix field
struct Eigenvalues {
  template <UInt dim>
  static void call(Grid<Real, dim>& eigs, const Grid<Real, dim>& field) {
    Loop::loop([] CUDA_LAMBDA(
                   VectorProxy<Real, dim> eig,
                   SymMatrixProxy<const Real, dim> f) { eig = eigenvalues(f); },
               range<VectorProxy<Real, dim>>(eigs),
               range<SymMatrixProxy<const Real, dim>>(field));
  }
};

/// Compute von Mises stress on a tensor field
struct VonMises {
  template <UInt dim>
  static void call(Grid<Real, dim>& vm, const Grid<Real, dim>& stress) {
    Loop::loop(
        [] CUDA_LAMBDA(Real & vm, SymMatrixProxy<const Real, dim> sigma) {
          SymMatrix<Real, dim> dev;
          dev.deviatoric(sigma, 3);
          vm = std::sqrt(1.5) * dev.l2norm();
        },
        vm, range<SymMatrixProxy<const Real, dim>>(stress));
  }
};

/// Compute deviatoric of tensor field
struct Deviatoric {
  template <UInt dim>
  static void call(Grid<Real, dim>& dev, const Grid<Real, dim>& field) {
    Loop::loop(
        [] CUDA_LAMBDA(SymMatrixProxy<Real, dim> s,
                       SymMatrixProxy<const Real, dim> sigma) {
          s.deviatoric(sigma, 3);
        },
        range<SymMatrixProxy<Real, dim>>(dev),
        range<SymMatrixProxy<const Real, dim>>(field));
  }
};
}  // namespace compute

template <typename Compute>
void applyCompute(model_type type, GridBase<Real>& result,
                  const GridBase<Real>& field) {
  if (type != model_type::volume_2d)
    throw model_type_error{TAMAAS_MSG(
        "Model type ", type, " not yet suported for field computation")};

  constexpr UInt dim = model_type_traits<model_type::volume_2d>::dimension;
  const auto& f = dynamic_cast<const Grid<Real, dim>&>(field);
  auto& e = dynamic_cast<Grid<Real, dim>&>(result);
  Compute::template call<dim>(e, f);
}

void eigenvalues(model_type type, GridBase<Real>& eigs,
                 const GridBase<Real>& field);
void vonMises(model_type type, GridBase<Real>& eigs,
              const GridBase<Real>& field);
void deviatoric(model_type type, GridBase<Real>& dev,
                const GridBase<Real>& field);

}  // namespace tamaas

#endif /* EIGENVALUES_HH */
