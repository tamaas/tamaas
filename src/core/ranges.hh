/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef RANGES_HH
#define RANGES_HH
/* -------------------------------------------------------------------------- */
#include "errors.hh"
#include "iterator.hh"
#include "mpi_interface.hh"
#include "static_types.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

/// Range class for complex iterators
template <typename LocalType, typename ValueType, UInt local_size>
class Range {

public:
  class iterator : public iterator_::iterator<ValueType> {
    using parent = iterator_::iterator<ValueType>;

  public:
    using value_type = LocalType;
    using reference = value_type;
    iterator(const parent& o) : parent(o) {}

    inline reference operator*() { return LocalType(this->data); }
    inline reference operator*() const { return LocalType(this->data); }
  };

public:
  // useful type trait
  template <typename Container>
  struct is_valid_container
      : std::is_same<
            std::remove_cv_t<typename std::decay_t<Container>::value_type>,
            std::remove_cv_t<ValueType>> {};

public:
  using value_type = LocalType;
  using reference = value_type&&;

  /// Construct from a container
  template <class Container>
  Range(Container&& cont) : _begin(cont.begin()), _end(cont.end()) {
    TAMAAS_ASSERT(
        cont.getNbComponents() == local_size,
        "Number of components does not match local tensor type size (",
        cont.getNbComponents(), ", expected ", local_size, ")");
    _begin.setStep(local_size);
    _end.setStep(local_size);
  }

  /// Construct from two iterators
  Range(iterator _begin, iterator _end)
      : _begin(std::move(_begin)), _end(std::move(_end)) {}

  iterator begin() { return _begin; }
  iterator end() { return _end; }

  Range headless() const {
    iterator shift(_begin);
    if (mpi::rank() == 0)
      ++shift;
    return Range{shift, _end};
  }

private:
  iterator _begin, _end;
};

/// Make range with proxy type
template <typename LocalType, class Container>
std::enable_if_t<
    is_proxy<LocalType>::value,
    Range<LocalType, typename LocalType::value_type, LocalType::size>>
range(Container&& cont) {
  using RangeClass =
      Range<LocalType, typename LocalType::value_type, LocalType::size>;
  static_assert(
      RangeClass::template is_valid_container<Container>::value,
      "Mismatch between range's value type and container's value type");
  return RangeClass(std::forward<Container>(cont));
}

/// Make range with standard type
template <typename LocalType, class Container>
std::enable_if_t<not is_proxy<LocalType>::value, Range<LocalType, LocalType, 1>>
range(Container&& cont) {
  using RangeClass = Range<LocalType, LocalType, 1>;
  static_assert(
      RangeClass::template is_valid_container<Container>::value,
      "Mismatch between range's value type and container's value type");
  return RangeClass(std::forward<Container>(cont));
}

/* -------------------------------------------------------------------------- */
}  // namespace tamaas

#endif
