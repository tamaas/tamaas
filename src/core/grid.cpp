/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "grid.hh"
#include "tamaas.hh"
#include <algorithm>
#include <complex>
#include <cstring>
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

template <typename T, UInt dim>
Grid<T, dim>::Grid() : GridBase<T>() {
  this->n.fill(0);
  this->strides.fill(1);
  this->nb_components = 1;
}

/* -------------------------------------------------------------------------- */

template <typename T, UInt dim>
void Grid<T, dim>::resize(const std::array<UInt, dim>& n) {
  this->resize(n.begin(), n.end());
}

/* -------------------------------------------------------------------------- */

template <typename T, UInt dim>
void Grid<T, dim>::resize(const std::vector<UInt>& n) {
  TAMAAS_ASSERT(n.size() == dim, "Shape vector not matching grid dimensions");
  this->resize(n.begin(), n.end());
}

/* -------------------------------------------------------------------------- */

template <typename T, UInt dim>
void Grid<T, dim>::resize(std::initializer_list<UInt> n) {
  TAMAAS_ASSERT(n.size() == dim,
                "Shape initializer list not matching grid dimensions");
  this->resize(std::begin(n), std::end(n));
}

/* -------------------------------------------------------------------------- */

template <typename T, UInt dim>
void Grid<T, dim>::computeStrides() {
  std::copy(n.begin() + 1, n.end(), strides.begin());
  strides[dim] = 1;
  strides[dim - 1] = this->nb_components;
  std::partial_sum(strides.rbegin(), strides.rend(), strides.rbegin(),
                   std::multiplies<UInt>());
}

/* -------------------------------------------------------------------------- */

template <typename T, UInt dim>
void Grid<T, dim>::printself(std::ostream& str) const {
  str << "Grid(" << dim << ", " << this->nb_components << ") {";
  for (auto& val : *this) {
    str << val << ", ";
  }
  str << "\b\b}";
}

/* -------------------------------------------------------------------------- */

/// Class instanciation
#define GRID_INSTANCIATE_TYPE(type)                                            \
  template class Grid<type, 1>;                                                \
  template class Grid<type, 2>;                                                \
  template class Grid<type, 3>

GRID_INSTANCIATE_TYPE(Real);
GRID_INSTANCIATE_TYPE(UInt);
GRID_INSTANCIATE_TYPE(Complex);
GRID_INSTANCIATE_TYPE(Int);
GRID_INSTANCIATE_TYPE(bool);
#undef GRID_INSTANCIATE_TYPE

}  // namespace tamaas
