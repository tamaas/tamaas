/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "statistics.hh"
#include "fft_engine.hh"
#include "loop.hh"
#include "static_types.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

template <UInt dim>
Real Statistics<dim>::computeRMSHeights(Grid<Real, dim>& surface) {
  return std::sqrt(surface.var());
}

template <UInt dim>
template <class T>
Real Statistics<dim>::rmsSlopesFromPSD(const GridHermitian<Real, dim>& psd,
                                       const Grid<T, dim>& diff) {
  return std::sqrt(Loop::reduce<operation::plus>(
      [] CUDA_LAMBDA(VectorProxy<const T, dim> q, const Complex& psd_val) {
        // Checking if we're in the zone that does not have hermitian symmetry
        Complex qback{q.back()}, qsq{q.l2squared()};
        auto factor = 2. - static_cast<double>(thrust::abs(qback) < 1e-15);
        return factor * thrust::abs(qsq) * psd_val.real();
      },
      range<VectorProxy<const T, dim>>(diff), psd));
}

template <UInt dim>
Real Statistics<dim>::computeSpectralRMSSlope(Grid<Real, dim>& surface) {
  const auto h_size =
      GridHermitian<Real, dim>::hermitianDimensions(surface.sizes());
  auto wavevectors =
      FFTEngine::template computeFrequencies<Real, dim, true>(h_size);

  // Diff operator for gradient norm is (qx^2 + qy^2), no need to change
  wavevectors *= 2 * M_PI;  // need q for slopes

  return rmsSlopesFromPSD(computePowerSpectrum(surface), wavevectors);
}

template <UInt dim>
Real Statistics<dim>::computeFDRMSSlope(Grid<Real, dim>& surface) {
  const auto h_size =
      GridHermitian<Real, dim>::hermitianDimensions(surface.sizes());
  auto wavevectors =
      FFTEngine::template computeFrequencies<Complex, dim, true>(h_size);

  // Diff operator is (exp(iqΔx) - 1)/Δx in each direction
  const auto N = Partitioner<dim>::global_size(surface);
  Loop::loop(
      [&N](VectorProxy<Complex, dim> q) {
        for (UInt i = 0; i < dim; ++i)
          q(i) = (thrust::polar(1., 2 * M_PI * q(i).real() / N[i]) - 1.) *
                 static_cast<double>(N[i]);
      },
      range<VectorProxy<Complex, dim>>(wavevectors));

  return rmsSlopesFromPSD(computePowerSpectrum(surface), wavevectors);
}

/* -------------------------------------------------------------------------- */

template <UInt dim>
GridHermitian<Real, dim>
Statistics<dim>::computePowerSpectrum(Grid<Real, dim>& surface) {
  const auto h_size =
      GridHermitian<Real, dim>::hermitianDimensions(surface.sizes());
  GridHermitian<Real, dim> psd(h_size, surface.getNbComponents());

  FFTEngine::makeEngine()->forward(surface, psd);

  Real factor = 1. / surface.getGlobalNbPoints();

  // Squaring the fourier transform of surface and normalizing
  Loop::loop(
      [factor] CUDA_LAMBDA(Complex & c) {
        c *= factor;
        c *= conj(c);
      },
      psd);

  return psd;
}

/* -------------------------------------------------------------------------- */

template <UInt dim>
Grid<Real, dim>
Statistics<dim>::computeAutocorrelation(Grid<Real, dim>& surface) {
  Grid<Real, dim> acf(surface.sizes(), surface.getNbComponents());
  auto psd = computePowerSpectrum(surface);

  FFTEngine::makeEngine()->backward(acf, psd);
  acf *= acf.getGlobalNbPoints();
  return acf;
}

/* -------------------------------------------------------------------------- */
template <UInt dim>
Real Statistics<dim>::contact(const Grid<Real, dim>& tractions,
                              UInt perimeter) {
  Real points = 0;
  UInt nc = tractions.getNbComponents();

  switch (nc) {
  case 1:
    points = Loop::reduce(
        [] CUDA_LAMBDA(const Real& t) -> Real { return t > 0; }, tractions);
    break;
  case 2:
    points = Loop::reduce(
        [] CUDA_LAMBDA(VectorProxy<const Real, 2> t) -> Real {
          return t.back() > 0;
        },
        range<VectorProxy<const Real, 2>>(tractions));
    break;
  case 3:
    points = Loop::reduce(
        [] CUDA_LAMBDA(VectorProxy<const Real, 3> t) -> Real {
          return t.back() > 0;
        },
        range<VectorProxy<const Real, 3>>(tractions));
    break;
  default:
    throw std::invalid_argument{
        TAMAAS_MSG("Invalid number of components in traction")};
  }

  auto area = points / tractions.getGlobalNbPoints();

  if (dim == 1)
    perimeter = 0;

  // Correction from Yastrebov et al. (Trib. Intl., 2017)
  // 10.1016/j.triboint.2017.04.023
  return area - (M_PI - 1 + std::log(2)) /
                    (24. * tractions.getGlobalNbPoints()) * perimeter;
}

/* -------------------------------------------------------------------------- */
namespace {
template <UInt dim>
class moment_helper {
public:
  moment_helper(const std::array<UInt, dim>& exp) : exponent(exp) {}

  CUDA_LAMBDA Complex operator()(VectorProxy<Real, dim> q,
                                 const Complex& phi) const {
    Real mul = 1;
    for (UInt i = 0; i < dim; ++i)
      mul *= std::pow(q(i), exponent[i]);

    // Do not duplicate everything from hermitian symmetry
    if (std::abs(q.back()) < 1e-15)
      return mul * phi;
    return 2 * mul * phi;
  }

private:
  std::array<UInt, dim> exponent;
};
}  // namespace

/* -------------------------------------------------------------------------- */
template <>
std::vector<Real> Statistics<1>::computeMoments(Grid<Real, 1>& surface) {
  constexpr UInt dim = 1;
  std::vector<Real> moments(3);
  const auto psd = computePowerSpectrum(surface);
  auto wavevectors =
      FFTEngine::template computeFrequencies<Real, dim, true>(psd.sizes());
  // we don't multiply by 2 pi because moments are computed with k
  moments[0] = Loop::reduce<operation::plus>(moment_helper<dim>{{{0}}},
                                             range<PVector>(wavevectors), psd)
                   .real();
  moments[1] = Loop::reduce<operation::plus>(moment_helper<dim>{{{2}}},
                                             range<PVector>(wavevectors), psd)
                   .real();
  moments[2] = Loop::reduce<operation::plus>(moment_helper<dim>{{{4}}},
                                             range<PVector>(wavevectors), psd)
                   .real();
  return moments;
}

template <>
std::vector<Real> Statistics<2>::computeMoments(Grid<Real, 2>& surface) {
  constexpr UInt dim = 2;
  std::vector<Real> moments(3);
  const auto psd = computePowerSpectrum(surface);
  auto wavevectors =
      FFTEngine::template computeFrequencies<Real, dim, true>(psd.sizes());
  // we don't multiply by 2 pi because moments are computed with k
  moments[0] = Loop::reduce<operation::plus>(moment_helper<dim>{{{0, 0}}},
                                             range<PVector>(wavevectors), psd)
                   .real();
  auto m02 = Loop::reduce<operation::plus>(moment_helper<dim>{{{0, 2}}},
                                           range<PVector>(wavevectors), psd)
                 .real();
  auto m20 = Loop::reduce<operation::plus>(moment_helper<dim>{{{2, 0}}},
                                           range<PVector>(wavevectors), psd)
                 .real();
  moments[1] = (m02 + m20) / 2;
  auto m22 = Loop::reduce<operation::plus>(moment_helper<dim>{{{2, 2}}},
                                           range<PVector>(wavevectors), psd)
                 .real();
  auto m40 = Loop::reduce<operation::plus>(moment_helper<dim>{{{4, 0}}},
                                           range<PVector>(wavevectors), psd)
                 .real();
  auto m04 = Loop::reduce<operation::plus>(moment_helper<dim>{{{0, 4}}},
                                           range<PVector>(wavevectors), psd)
                 .real();
  moments[2] = (3 * m22 + m40 + m04) / 3;
  return moments;
}

/* -------------------------------------------------------------------------- */
template <UInt dim>
Real Statistics<dim>::graphArea(const Grid<Real, dim>& zdisplacement) {
  const auto h_size =
      GridHermitian<Real, dim>::hermitianDimensions(zdisplacement.sizes());
  GridHermitian<Real, dim> fourier_disp(h_size, 1),
      fourier_gradient(h_size, dim);
  Grid<Real, dim> gradient(zdisplacement.sizes(), dim);

  // Compute gradient in Fourier domain
  FFTEngine::makeEngine()->forward(zdisplacement, fourier_disp);

  auto wavevectors =
      FFTEngine::template computeFrequencies<Real, dim, true>(h_size);
  wavevectors *= 2 * M_PI;

  Loop::loop(
      [] CUDA_LAMBDA(VectorProxy<const Real, dim> q,
                     VectorProxy<Complex, dim> grad, const Complex& f) {
        grad = q * f;
        grad *= Complex{0, 1};
      },
      range<VectorProxy<const Real, dim>>(wavevectors),
      range<VectorProxy<Complex, dim>>(fourier_gradient), fourier_disp);

  FFTEngine::makeEngine()->backward(gradient, fourier_gradient);

  // Integrate area of graph formula
  Real factor = Loop::reduce<operation::plus>(
      [] CUDA_LAMBDA(VectorProxy<const Real, dim> grad) {
        return std::sqrt(1 + grad.l2squared());
      },
      range<VectorProxy<const Real, dim>>(gradient));
  return factor / gradient.getGlobalNbPoints();
  // return 2 * M_PI * factor / gradient.getGlobalNbPoints();
}

template struct Statistics<1>;
template struct Statistics<2>;

}  // namespace tamaas
