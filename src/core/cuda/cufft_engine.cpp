/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "cufft_engine.hh"
#include <algorithm>
#include <functional>
#include <numeric>
/* -------------------------------------------------------------------------- */
namespace tamaas {

CuFFTEngine::plan_t& CuFFTEngine::getPlans(key_t key) {
  if (plans.find(key) != plans.end())
    return plans[key];

  const int rank = key.size() - 3;  // dimension of fft
  const auto components = key[rank];

  std::vector<int> n(rank);  // size of individual fft
  std::copy_n(key.begin(), rank, n.begin());

  const int howmany = components;  // how many fft to compute
  const int idist = 1,
            odist = 1;  // components are next to each other in memory
  const int istride = key[rank + 1] * components,
            ostride = key[rank + 2] * components;
  int *inembed = nullptr, *onembed = nullptr;  // row major

  plan_t cufft_plans;
  cufftResult forward_res =
      cufftPlanMany(&cufft_plans.first._plan, rank, n.data(), inembed, istride,
                    idist, onembed, ostride, odist, CUFFT_D2Z, howmany);
  cufftResult backward_res =
      cufftPlanMany(&cufft_plans.second._plan, rank, n.data(), onembed, ostride,
                    odist, inembed, istride, idist, CUFFT_Z2D, howmany);

  plans[key] = std::move(cufft_plans);
  return plans[key];
}

}  // namespace tamaas
