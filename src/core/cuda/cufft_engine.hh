/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef CUFFT_ENGINE_H
#define CUFFT_ENGINE_H
/* -------------------------------------------------------------------------- */
#include "fft_engine.hh"
#include <cufft.h>
/* -------------------------------------------------------------------------- */

namespace cufft {
struct plan {
  cufftHandle _plan;

  plan() = default;

  plan(plan&& o) noexcept : _plan(o._plan) {}

  plan& operator=(plan&& o) noexcept {
    _plan = o._plan;
    return *this;
  }

  /// Destroy plan
  ~plan() noexcept { cufftDestroy(_plan); }

  /// For seamless use with fftw api
  operator cufftHandle() const { return _plan; }
};
}  // namespace cufft

namespace tamaas {

class CuFFTEngine : public FFTEngine {
private:
  using plan_t = std::pair<cufft::plan, cufft::plan>;

  /// Perform forward (R2C) transform
  template <UInt dim>
  void forwardImpl(const Grid<Real, dim>& real,
                   GridHermitian<Real, dim>& spectral);
  /// Perform backward (C2R) transform
  template <UInt dim>
  void backwardImpl(Grid<Real, dim>& real,
                    const GridHermitian<Real, dim>& spectral);
  /// Return the plans pair for a given transform signature
  plan_t& getPlans(key_t key);

public:
  /// Initialize with flags
  explicit CuFFTEngine(unsigned int flags = FFTW_ESTIMATE) noexcept
      : _flags(flags), plans() {}

  void forward(const Grid<Real, 1>& real,
               GridHermitian<Real, 1>& spectral) override {
    forwardImpl(real, spectral);
  }

  void forward(const Grid<Real, 2>& real,
               GridHermitian<Real, 2>& spectral) override {
    forwardImpl(real, spectral);
  }

  void backward(Grid<Real, 1>& real,
                GridHermitian<Real, 1>& spectral) override {
    backwardImpl(real, spectral);
  }

  void backward(Grid<Real, 2>& real,
                GridHermitian<Real, 2>& spectral) override {
    backwardImpl(real, spectral);
  }

  unsigned int flags() const { return _flags; }

  /// Cast to FFTW complex type
  static auto cast(Complex* data) {
    return reinterpret_cast<cufftDoubleComplex*>(data);
  }
  static auto cast(const Complex* data) {
    return const_cast<cufftDoubleComplex*>(
        reinterpret_cast<const cufftDoubleComplex*>(data));
  }

protected:
  unsigned int _flags;            ///< FFTW flags
  std::map<key_t, plan_t> plans;  ///< plans corresponding to signatures
};

/* -------------------------------------------------------------------------- */
template <UInt dim>
void CuFFTEngine::forwardImpl(const Grid<Real, dim>& real,
                              GridHermitian<Real, dim>& spectral) {
  auto& plans = getPlans(make_key(real, spectral));
  if (cufftExecD2Z(plans.first, const_cast<Real*>(real.getInternalData()),
                   cast(spectral.getInternalData())) != CUFFT_SUCCESS)
    TAMAAS_EXCEPTION("Forward transform fail");
  cudaDeviceSynchronize();
}

template <UInt dim>
void CuFFTEngine::backwardImpl(Grid<Real, dim>& real,
                               const GridHermitian<Real, dim>& spectral) {
  auto& plans = getPlans(make_key(real, spectral));
  if (cufftExecZ2D(plans.second, cast(spectral.getInternalData()),
                   real.getInternalData()) != CUFFT_SUCCESS)
    TAMAAS_EXCEPTION("Backward transform fail");
  cudaDeviceSynchronize();

  // Normalize
  real *= (1. / real.getNbPoints());
}

}  // namespace tamaas
#endif
