/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "mpi_interface.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

namespace mpi_dummy {
comm& comm::world() {
#if defined(TAMAAS_USE_PETSC) && !defined(TAMAAS_USE_MPI)
  static comm _world{MPI_COMM_WORLD};
#else
  static comm _world;
#endif
  return _world;
}
}  // namespace mpi_dummy

#ifdef TAMAAS_USE_MPI
namespace mpi_impl {
comm sequential::old_comm{MPI_COMM_NULL};

comm& comm::world() {
  static comm _world{MPI_COMM_WORLD};
  return _world;
}

// Define type traits for MPI data types
#define TYPE(t, mpi_t)                                                         \
  const MPI_Datatype type_trait<t>::value { mpi_t }
TYPE(float, MPI_FLOAT);
TYPE(double, MPI_DOUBLE);
TYPE(int, MPI_INT);
TYPE(unsigned int, MPI_UNSIGNED);
TYPE(long double, MPI_LONG_DOUBLE);
TYPE(long, MPI_LONG);
TYPE(unsigned long, MPI_UNSIGNED_LONG);
TYPE(::tamaas::complex<float>, MPI_CXX_FLOAT_COMPLEX);
TYPE(::tamaas::complex<double>, MPI_CXX_DOUBLE_COMPLEX);
TYPE(::tamaas::complex<long double>, MPI_CXX_LONG_DOUBLE_COMPLEX);
TYPE(bool, MPI_CXX_BOOL);
#undef TYPE

// Define type traits for MPI operations
#define OPERATION(op, mpi_op)                                                  \
  const MPI_Op operation_trait<operation::op>::value { mpi_op }
OPERATION(plus, MPI_SUM);
OPERATION(min, MPI_MIN);
OPERATION(max, MPI_MAX);
OPERATION(times, MPI_PROD);
#undef OPERATION

}  // namespace mpi_impl
#endif

}  // namespace tamaas
