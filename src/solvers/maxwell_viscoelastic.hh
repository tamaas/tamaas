/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides),
 *  Laboratory (IJLRDA - Institut Jean Le Rond d'Alembert)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef MAXWELL_VISCOELASTIC_HH
#define MAXWELL_VISCOELASTIC_HH
/* -------------------------------------------------------------------------- */
#include "polonsky_keer_rey.hh"
#include "westergaard.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

/**
   @brief Viscoelastic, pressure-based normal contact solver, based on
   a generalized Maxwell model
*/
class MaxwellViscoelastic : public PolonskyKeerRey {
public:
  /// Constructor
  MaxwellViscoelastic(Model& model, const GridBase<Real>& surface,
                      Real tolerance, Real time_step,
                      std::vector<Real> shear_moduli_maxwell,
                      std::vector<Real> characteristic_times);
  ~MaxwellViscoelastic() override = default;

public:
  /// \cond DO_NOT_DOCUMENT
  using PolonskyKeerRey::solve;
  /// \endcond

  /// Main solve function with backward euler scheme
  Real solve(std::vector<Real> target) override;

  /// Reset function, reset the global displacement and the partial displacement
  void reset();

  /// Update internal variables
  void updateState() override;

  TAMAAS_ACCESSOR(time_step_, Real, TimeStep);

private:
  /// Compute the effective shear moduli for maxwell branches
  Real computeGtilde(const Real& time_step,
                     const std::vector<Real>& shear_moduli_maxwell,
                     const std::vector<Real>& characteristic_times) const;

  /// Compute the partial displacement coefficient for each maxwell branch to
  /// update surface
  std::vector<Real>
  computeGamma(const Real& time_step,
               const std::vector<Real>& shear_moduli_maxwell,
               const std::vector<Real>& characteristic_times) const;

protected:
  Real time_step_;
  std::vector<Real> shear_moduli_;  ///< list of shear moduli
  std::vector<Real>
      characteristic_times_;      ///< list of characteristic relaxation times
  std::vector<GridBase<Real>> M;  ///< partial displacements
  GridBase<Real> U, U_new;        ///< global viscoelastic displacement
public:
  bool solve_should_update = true;  ///< update at the end of a solve step
};

}  // namespace tamaas

#endif  // MAXWELL_VISCOELASTIC_HH
