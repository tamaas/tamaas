/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "contact_solver.hh"
#include "logger.hh"
#include <iomanip>
#include <iostream>
/* -------------------------------------------------------------------------- */
namespace tamaas {

ContactSolver::ContactSolver(Model& model, const GridBase<Real>& surface,
                             Real tolerance)
    : model(model), tolerance(tolerance),
      surface_stddev(std::sqrt(surface.var())) {

  auto discretization = model.getBoundaryDiscretization();
  auto n_points = model.getTraction().getNbPoints();

  TAMAAS_ASSERT(n_points == surface.dataSize(),
                "model size and surface size do not match");

  this->surface.wrap(surface);
  _gap = allocateGrid<true, Real>(model.getType(), discretization);
  model["gap"] = _gap;
}

/* -------------------------------------------------------------------------- */
void ContactSolver::printState(UInt iter, Real cost_f, Real error) const {
  if (iter % dump_frequency)
    return;
  logIteration(iter, cost_f, error);
}

/* -------------------------------------------------------------------------- */
void ContactSolver::logIteration(UInt iter, Real cost_f, Real error) const {
  Logger().get(LogLevel::info)
      << std::setw(5) << iter << " " << std::setw(15) << std::scientific
      << cost_f << " " << std::setw(15) << error << "\n"
      << std::fixed;
}

/* -------------------------------------------------------------------------- */
void ContactSolver::addFunctionalTerm(
    std::shared_ptr<functional::Functional> func) {
  functional.addFunctionalTerm(std::move(func));
}

/* -------------------------------------------------------------------------- */
void ContactSolver::setFunctional(
    std::shared_ptr<functional::Functional> func) {
  functional.clear();
  addFunctionalTerm(std::move(func));
}

/* -------------------------------------------------------------------------- */
void ContactSolver::updateState() {}

}  // namespace tamaas
/* -------------------------------------------------------------------------- */
