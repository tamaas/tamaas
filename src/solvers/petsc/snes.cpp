/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "snes.hh"
#include <petscsystypes.h>
/* -------------------------------------------------------------------------- */
namespace tamaas {
namespace petsc {
/* -------------------------------------------------------------------------- */

#define chk(error) PetscCallAbort(mpi::comm::world(), error)

PetscErrorCode form_residual(SNES, Vec x, Vec f, void* ctx) {
  auto* res_ctx = static_cast<NonlinearSolver::residual_ctx*>(ctx);
  auto& residual = *res_ctx->first;
  auto& strain_increment = *res_ctx->second;
  auto local_n = strain_increment.getNbPoints();
  auto nc = strain_increment.getNbComponents();

  // Retrive pointers from petsc vectors
  Real* xarray = nullptr;
  chk(VecGetArray(x, &xarray));
  Grid<Real, 1> x1d(std::array<UInt, 1>{local_n}, nc,
                    span<Real>{xarray, nc * local_n});

  residual.computeResidual(x1d);

  // Copy residual
  Vec res;
  chk(VecCreateMPIWithArray(mpi::comm::world(), 1, strain_increment.dataSize(),
                            strain_increment.globalDataSize(),
                            residual.getVector().getInternalData(), &res));
  chk(VecCopy(res, f));
  chk(VecRestoreArray(x, &xarray));
  chk(VecDestroy(&res));
  return 0;
}

PetscErrorCode form_jacobian(SNES, Vec, Mat, Mat, void*) {
  // empty on purpose, the matrix shell does all the work of forming the
  // jacobian
  return 0;
}

// y = Ax
PetscErrorCode tangent_shell(Mat mat, Vec x, Vec y) {
  NonlinearSolver::residual_ctx* res_ctx = nullptr;
  chk(MatShellGetContext(mat, &res_ctx));
  auto& residual = *res_ctx->first;
  auto& strain_increment = *res_ctx->second;
  auto local_n = strain_increment.getNbPoints();
  auto nc = strain_increment.getNbComponents();

  // Retrive pointers from petsc vectors
  Real *xarray = nullptr, *yarray = nullptr;
  chk(VecGetArray(x, &xarray));
  chk(VecGetArray(y, &yarray));

  // Convert to grid views
  Grid<Real, 1> x1d(std::array<UInt, 1>{local_n}, nc,
                    span<Real>{xarray, nc * local_n});
  Grid<Real, 1> y1d(std::array<UInt, 1>{local_n}, nc,
                    span<Real>{yarray, nc * local_n});

  // Apply tangent
  residual.applyTangent(y1d, x1d, strain_increment);

  // Restore petsc vectors
  chk(VecRestoreArray(x, &xarray));
  chk(VecRestoreArray(y, &yarray));
  return 0;
}

NonlinearSolver::NonlinearSolver(Residual& residual, std::string petsc_args)
    : EPSolver(residual) {
  // Create the options database and make petsc use it
  chk(PetscOptionsCreate(&snes_options));
  chk(PetscOptionsInsertString(snes_options, petsc_args.c_str()));
  chk(PetscOptionsPush(snes_options));

  // Wrap Petsc vectors on Tamaas' grids
  auto local_n = _x->dataSize(), global_n = _x->globalDataSize();
  chk(VecCreateMPIWithArray(mpi::comm::world(), 1, local_n, global_n,
                            _x->getInternalData(), &_xvec));
  chk(VecCreateMPIWithArray(mpi::comm::world(), 1, local_n, global_n,
                            residual.getVector().getInternalData(), &_rvec));

  // Context object for matrix and non-linear solver
  res_ctx = std::make_pair(&residual, _x.get());

  // Create jacobian matrix
  chk(MatCreateShell(mpi::comm::world(), local_n, local_n, global_n, global_n,
                     &res_ctx, &J));
  chk(MatShellSetOperation(J, MATOP_MULT, (void (*)(void))tangent_shell));

  // Create non-linear solver context
  chk(SNESCreate(mpi::comm::world(), &snes));
  chk(SNESSetType(snes, SNESNEWTONLS));

  // Set defaults for linear solver and preconditioner
  KSP ksp;
  PC pc;
  chk(SNESGetKSP(snes, &ksp));
  chk(KSPGetPC(ksp, &pc));
  chk(PCSetType(pc, PCNONE));
  Real atol = 1e-10;
  UInt maxiter = 20;
  chk(KSPSetTolerances(ksp, PETSC_DEFAULT, atol, PETSC_DEFAULT, maxiter));

  // Set callbacks and options
  chk(SNESSetFunction(snes, _rvec, form_residual, &res_ctx));
  chk(SNESSetJacobian(snes, J, J, form_jacobian, nullptr));
  chk(SNESSetFromOptions(snes));

  // Clearing solver-specific options
  chk(PetscOptionsPop());
}

NonlinearSolver::~NonlinearSolver() {
  chk(PetscOptionsDestroy(&snes_options));
  chk(SNESDestroy(&snes));
  chk(MatDestroy(&J));

  chk(VecDestroy(&_xvec));
  chk(VecDestroy(&_rvec));
}

void NonlinearSolver::solve() {
  chk(SNESSolve(snes, nullptr, _xvec));
  _residual.computeResidualDisplacement(*_x);
}

}  // namespace petsc
}  // namespace tamaas
