/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef PETSC_SNES_SOLVER_HH
#define PETSC_SNES_SOLVER_HH
/* -------------------------------------------------------------------------- */
#include "ep_solver.hh"
#include "residual.hh"
#include <petscmat.h>
#include <petscoptions.h>
#include <petscsnes.h>
#include <petscsys.h>
#include <petscvec.h>
#include <string>
#include <vector>
/* -------------------------------------------------------------------------- */
namespace tamaas {
namespace petsc {

/// Wrapper to PETSc SNES abstract solver
class NonlinearSolver : public EPSolver {
public:
  using residual_ctx = std::pair<Residual*, GridBase<Real>*>;

public:
  NonlinearSolver(Residual& residual, std::string petsc_args);
  ~NonlinearSolver() override;

  void solve() override;

private:
  PetscOptions snes_options;
  Vec _xvec, _rvec;
  Mat J;
  SNES snes;

  residual_ctx res_ctx;
};

}  // namespace petsc
}  // namespace tamaas
/* -------------------------------------------------------------------------- */
#endif
