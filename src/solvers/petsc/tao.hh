/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef PETSC_TAO_SOLVER_HH
#define PETSC_TAO_SOLVER_HH
/* -------------------------------------------------------------------------- */
#include "polonsky_keer_rey.hh"
#include <petscmat.h>
#include <petscoptions.h>
#include <petscsys.h>
#include <petsctao.h>
#include <petscvec.h>
#include <string>
#include <vector>
/* -------------------------------------------------------------------------- */
namespace tamaas {
namespace petsc {

/// Wrapper to PETSc TAO abstract solver
class OptimizationSolver : public PolonskyKeerRey {
public:
  using gradient_context =
      std::tuple<std::vector<UInt>, model_type, functional::MetaFunctional*>;

public:
  OptimizationSolver(Model& model, const GridBase<Real>& surface,
                     Real tolerance, std::string petsc_args);
  ~OptimizationSolver() override;

  Real solve(std::vector<Real> target_gap) override;

private:
  PetscOptions tao_options;
  Vec _primal_vec, _dual_vec, _lower_b, _upper_b;
  Mat H, P;
  Tao tao;

  gradient_context grad_ctx;
};

}  // namespace petsc
}  // namespace tamaas

#endif
