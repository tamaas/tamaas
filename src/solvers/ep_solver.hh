/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef EP_SOLVER_HH
#define EP_SOLVER_HH
/* -------------------------------------------------------------------------- */
#include "grid_base.hh"
#include "residual.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

struct ToleranceManager {
  Real start_tol, end_tol, rate, tolerance;

  ToleranceManager(Real start, Real end, Real rate)
      : start_tol(start / rate), end_tol(end), rate(rate),
        tolerance(start_tol) {}

  void step() { tolerance = std::max(end_tol, rate * tolerance); }
  void reset() { tolerance = start_tol; }
};

class EPSolver {
public:
  /// Constructor
  EPSolver(Residual& residual);
  /// Destructor
  virtual ~EPSolver() = default;

  // Main methods
public:
  virtual void solve() = 0;
  virtual void updateState();
  virtual void beforeSolve();

  // Accessors
public:
  GridBase<Real>& getStrainIncrement() { return *_x; }
  Residual& getResidual() { return _residual; }
  Real getTolerance() const { return abs_tol.tolerance; }
  void setTolerance(Real tol) { abs_tol = ToleranceManager{tol, tol, 1}; }
  void setToleranceManager(ToleranceManager manager) { abs_tol = manager; }

protected:
  std::shared_ptr<GridBase<Real>> _x;
  Residual& _residual;
  ToleranceManager abs_tol{1e-9, 1e-9, 1};
};

/* -------------------------------------------------------------------------- */
}  // namespace tamaas
#endif
