/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef EPIC_HH
#define EPIC_HH
/* -------------------------------------------------------------------------- */
#include "contact_solver.hh"
#include "ep_solver.hh"
#include "model.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

class EPICSolver {
public:
  /// Constructor
  EPICSolver(ContactSolver& csolver, EPSolver& epsolver, Real tolerance = 1e-10,
             Real relaxation = 0.3);

  virtual Real solve(const std::vector<Real>& load);
  Real acceleratedSolve(const std::vector<Real>& load);
  Real computeError(const GridBase<Real>& current, const GridBase<Real>& prev,
                    Real factor) const;

  void fixedPoint(GridBase<Real>& result, const GridBase<Real>& x,
                  const GridBase<Real>& initial_surface,
                  std::vector<Real> load);

  template <model_type type>
  void setViews();

  TAMAAS_ACCESSOR(tolerance, Real, Tolerance);
  TAMAAS_ACCESSOR(relaxation, Real, Relaxation);
  TAMAAS_ACCESSOR(max_iterations, UInt, MaxIterations);
  const Model& getModel() const { return csolver.getModel(); }

protected:
  GridBase<Real> surface;                         ///< corrected surface
  GridBase<Real> pressure;                        ///< current pressure
  std::unique_ptr<GridBase<Real>> residual_disp;  ///< plastic residual disp
  std::unique_ptr<GridBase<Real>> pressure_inc;   ///< pressure increment

  ContactSolver& csolver;
  EPSolver& epsolver;

  Real tolerance, relaxation;
  UInt max_iterations = 1000;
};

/* -------------------------------------------------------------------------- */
/* Template impl. */
/* -------------------------------------------------------------------------- */

template <model_type type>
void EPICSolver::setViews() {
  constexpr UInt dim = model_type_traits<type>::dimension;
  constexpr UInt bdim = model_type_traits<type>::boundary_dimension;
  constexpr UInt comp = model_type_traits<type>::components;

  pressure_inc =
      std::unique_ptr<GridBase<Real>>{new GridView<Grid, Real, bdim, bdim>(
          csolver.getModel().getTraction(), {}, comp - 1)};
  residual_disp =
      std::unique_ptr<GridBase<Real>>{new GridView<Grid, Real, dim, bdim>(
          csolver.getModel().getDisplacement(),
          model_type_traits<type>::indices, comp - 1)};
}

/* -------------------------------------------------------------------------- */
}  // namespace tamaas
#endif  // EPIC_HH
