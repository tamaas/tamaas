/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef DFSANE_SOLVER_HH
#define DFSANE_SOLVER_HH
/* -------------------------------------------------------------------------- */
#include "ep_solver.hh"
#include "grid_base.hh"
#include "residual.hh"
#include <deque>
#include <functional>
#include <utility>
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

/**
 @brief Derivative-free non-linear solver

 This algorithm is based on W. La Cruz, J. Martínez, and M. Raydan, “Spectral
 residual method without gradient information for solving large-scale nonlinear
 systems of equations,” Math. Comp., vol. 75, no. 255, pp. 1429–1448, 2006, doi:
 10.1090/S0025-5718-06-01840-0.

 The same algorithm is available in scipy.optimize, but this version is robustly
 parallel by default (i.e. does not depend on BLAS's parallelism and is
 future-proof for MPI parallelism).
*/
class DFSANESolver : public EPSolver {
  // Public interface
public:
  DFSANESolver(Residual& residual);
  void solve() override;

  TAMAAS_ACCESSOR(max_iterations, UInt, MaxIterations);

  // Algorithm functions
protected:
  Real computeSpectralCoeff(const std::pair<Real, Real>& bounds);
  void computeSearchDirection(Real sigma);
  void lineSearch(Real eta_k);
  static Real computeAlpha(Real alpha, Real f, Real fk,
                           const std::pair<Real, Real>& bounds);

protected:
  GridBase<Real> search_direction, previous_residual, current_x, delta_x,
      delta_residual;
  std::deque<Real> previous_merits;
  std::function<Real(UInt)> eta;
  UInt max_iterations = 100;
};

}  // namespace tamaas
#endif
