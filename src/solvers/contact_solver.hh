/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef CONTACT_SOLVER_HH
#define CONTACT_SOLVER_HH
/* -------------------------------------------------------------------------- */
#include "meta_functional.hh"
#include "model.hh"
#include "tamaas.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

class ContactSolver {
public:
  /// Constructor
  ContactSolver(Model& model, const GridBase<Real>& surface, Real tolerance);
  /// Destructor
  virtual ~ContactSolver() = default;

public:
  /// Print state of solve
  virtual void printState(UInt iter, Real cost_f, Real error) const;
  /// Log iteration info
  virtual void logIteration(UInt iter, Real cost_f, Real error) const;
  /// Get maximum number of iterations
  auto getMaxIterations() const { return max_iterations; }
  /// Set maximum number of iterations
  void setMaxIterations(UInt n) { max_iterations = n; }
  /// Get dump_frequency
  auto getDumpFrequency() const { return dump_frequency; }
  /// Set dump_frequency
  void setDumpFrequency(UInt n) { dump_frequency = n; }
  /// Add term to functional
  void addFunctionalTerm(std::shared_ptr<functional::Functional> func);
  /// Returns functional object
  const functional::Functional& getFunctional() const { return functional; }
  /// Sets functional sum to a single term
  void setFunctional(std::shared_ptr<functional::Functional> func);
  /// Update internal variables (if any)
  virtual void updateState();

  // Virtual functions
public:
  /// Solve for a mean traction vector
  virtual Real solve(std::vector<Real> /*load*/) {
    throw not_implemented_error{TAMAAS_MSG("not implemented")};
  }

  /// Solve for normal pressure
  virtual Real solve(Real load) { return solve(std::vector<Real>{load}); }

  /// Accessor for tolerance
  TAMAAS_ACCESSOR(tolerance, Real, Tolerance);

public:
  GridBase<Real>& getSurface() { return surface; }
  Model& getModel() { return model; }

protected:
  Model& model;
  GridBase<Real> surface;
  std::shared_ptr<GridBase<Real>> _gap = nullptr;
  functional::MetaFunctional functional;

  Real tolerance;
  UInt max_iterations = 1000;
  Real surface_stddev;
  UInt dump_frequency = 100;
};

}  // namespace tamaas
/* -------------------------------------------------------------------------- */
#endif  // CONTACT_SOLVER_HH
