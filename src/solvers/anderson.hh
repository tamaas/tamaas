/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef ANDERSON_HH
#define ANDERSON_HH
/* -------------------------------------------------------------------------- */
#include "epic.hh"
#include <deque>
/* -------------------------------------------------------------------------- */
namespace tamaas {

class AndersonMixing : public EPICSolver {
  using memory_t = std::deque<GridBase<Real>>;

public:
  AndersonMixing(ContactSolver& csolver, EPSolver& epsolver,
                 Real tolerance = 1e-10, UInt memory = 5);

  Real solve(const std::vector<Real>& load) override;

private:
  static std::vector<Real> computeGamma(const memory_t& residual);
  static GridBase<Real> mixingUpdate(GridBase<Real> x, std::vector<Real> gamma,
                                     const memory_t& memory,
                                     const memory_t& residual_memory,
                                     Real relaxation);

private:
  UInt M;
};

}  // namespace tamaas
/* -------------------------------------------------------------------------- */
#endif
