/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef KATO_SATURATED_HH
#define KATO_SATURATED_HH
/* -------------------------------------------------------------------------- */
#include "polonsky_keer_rey.hh"
#include <limits>
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

/// Polonsky-Keer algorithm with saturation of the pressure
class KatoSaturated : public PolonskyKeerRey {
public:
  /// Constructor
  KatoSaturated(Model& model, const GridBase<Real>& surface, Real tolerance,
                Real pmax);
  ~KatoSaturated() override = default;

public:
  /// Solve
  Real solve(std::vector<Real> load) override;
  /// Mean on unsaturated constraint zone
  Real meanOnUnsaturated(const GridBase<Real>& field) const override;
  /// Compute squared norm
  Real computeSquaredNorm(const GridBase<Real>& field) const override;
  /// Update search direction
  void updateSearchDirection(Real factor) override;
  /// Compute critical step
  Real computeCriticalStep(Real target = 0) override;
  /// Update primal and check non-admissible state
  bool updatePrimal(Real step) override;
  /// Compute error/stopping criterion
  Real computeError() const override;
  /// Enfore mean value constraint
  void enforceMeanValue(Real mean) override;
  /// Enforce contact constraints on final state
  void enforceAdmissibleState() override;
  /// Access to pmax
  Real getPMax() const { return pmax; }
  /// Set pax
  void setPMax(Real p) {
    if (p <= 0)
      pmax = std::numeric_limits<Real>::max();
    else
      pmax = p;
  }

protected:
  Real pmax = std::numeric_limits<Real>::max();  ///< saturation pressure
};

}  // namespace tamaas
#endif  // KATO_SATURATED_HH
