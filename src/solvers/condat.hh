/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef CONDAT_HH
#define CONDAT_HH
/* -------------------------------------------------------------------------- */
#include "kato.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

class Condat : public Kato {
public:
  /// Constructor
  Condat(Model& model, const GridBase<Real>& surface, Real tolerance, Real mu);

public:
  /// Solve
  Real solve(std::vector<Real> p0) override;

  /// Template for solve function
  template <model_type type>
  Real solveTmpl(GridBase<Real>& p0, Real grad_step);
  /// Update gap
  template <UInt comp>
  void updateGap(Real sigma, Real grad_step, GridBase<Real>& q);
  /// Update Lagrange multiplier q
  template <UInt comp>
  void updateLagrange(GridBase<Real>& q, GridBase<Real>& p0);

  void setGradStep(Real gstep) { grad_step = gstep; }

private:
  std::unique_ptr<GridBase<Real>> pressure_old = nullptr;
  Real grad_step = 0.9;
};

}  // namespace tamaas

#endif  // CONDAT_HH
