/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef POLONSKY_KEER_REY_HH
#define POLONSKY_KEER_REY_HH
/* -------------------------------------------------------------------------- */
#include "contact_solver.hh"
#include "grid_view.hh"
#include "meta_functional.hh"
#include "westergaard.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

class PolonskyKeerRey : public ContactSolver {
public:
  /// Types of algorithm (primal/dual) or constraint
  enum type { gap, pressure };

public:
  /// Constructor
  PolonskyKeerRey(Model& model, const GridBase<Real>& surface, Real tolerance,
                  type variable_type, type constraint_type);
  ~PolonskyKeerRey() override = default;

public:
  /// \cond DO_NOT_DOCUMENT
  using ContactSolver::solve;
  ///\endcond

  /// Solve
  Real solve(std::vector<Real> target) override;
  /// Mean on unsaturated constraint zone
  virtual Real meanOnUnsaturated(const GridBase<Real>& field) const;
  /// Compute squared norm
  virtual Real computeSquaredNorm(const GridBase<Real>& field) const;
  /// Update search direction
  virtual void updateSearchDirection(Real factor);
  /// Compute critical step
  virtual Real computeCriticalStep(Real target = 0);
  /// Update primal and check non-admissible state
  virtual bool updatePrimal(Real step);
  /// Compute error/stopping criterion
  virtual Real computeError() const;
  /// Enforce contact constraints on final state
  virtual void enforceAdmissibleState();
  /// Enfore mean value constraint
  virtual void enforceMeanValue(Real mean);
  /// Set integral operator for gradient computation
  void setIntegralOperator(std::string name);
  /// Set functionals for contact
  void setupFunctional();

private:
  /// Set correct views on normal traction and gap
  template <model_type type>
  void setViews();
  /// Set the default integral operator
  template <model_type type>
  void defaultOperator();

protected:
  type variable_type, constraint_type;
  model_type operation_type;
  GridBase<Real>* primal = nullptr;  ///< non-owning pointer for primal varaible
  GridBase<Real>* dual = nullptr;    ///< non-owning pointer for dual variable

  /// CG search direction
  std::unique_ptr<GridBase<Real>> search_direction = nullptr;
  /// Projected CG search direction
  std::unique_ptr<GridBase<Real>> projected_search_direction = nullptr;
  /// View on normal pressure, gap, displacement
  std::unique_ptr<GridBase<Real>> pressure_view, gap_view,
      displacement_view = nullptr;
  /// Integral operator for gradient computation
  std::shared_ptr<IntegralOperator> integral_op = nullptr;
};

/* -------------------------------------------------------------------------- */
template <model_type mtype>
void PolonskyKeerRey::setViews() {
  constexpr UInt dim = model_type_traits<mtype>::dimension;
  constexpr UInt bdim = model_type_traits<mtype>::boundary_dimension;
  constexpr UInt comp = model_type_traits<mtype>::components;

  pressure_view = std::unique_ptr<GridBase<Real>>{
      new GridView<Grid, Real, bdim, bdim>(model.getTraction(), {}, comp - 1)};
  gap_view = std::unique_ptr<GridBase<Real>>{
      new GridView<Grid, Real, bdim, bdim>(*this->_gap, {}, comp - 1)};
  displacement_view =
      std::unique_ptr<GridBase<Real>>{new GridView<Grid, Real, dim, bdim>(
          model.getDisplacement(), model_type_traits<mtype>::indices,
          comp - 1)};

  if (bdim == 1)
    operation_type = model_type::basic_1d;
  else
    operation_type = model_type::basic_2d;
}

template <model_type mtype>
void PolonskyKeerRey::defaultOperator() {
  constexpr UInt bdim = model_type_traits<mtype>::boundary_dimension;
  std::shared_ptr<IntegralOperator> dirichlet, neumann;

#define WESTERGAARD(type, kind, desc)                                          \
  this->model.registerIntegralOperator<                                        \
      Westergaard<model_type::type, IntegralOperator::kind>>(desc)
  // I noz is fugly - TODO factory?
  if (bdim == 1) {
    dirichlet = WESTERGAARD(basic_1d, dirichlet, "westergaard_dirichlet");
    neumann = WESTERGAARD(basic_1d, neumann, "westergaard_neumann");
  } else if (bdim == 2) {
    dirichlet = WESTERGAARD(basic_2d, dirichlet, "westergaard_dirichlet");
    neumann = WESTERGAARD(basic_2d, neumann, "westergaard_neumann");
  }
#undef WESTERGAARD

  if (variable_type == gap)
    integral_op = std::move(dirichlet);
  else
    integral_op = std::move(neumann);
}

}  // namespace tamaas

#endif  // POLONSKY_KEER_REY_HH
