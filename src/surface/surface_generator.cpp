/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "surface_generator.hh"
#include "partitioner.hh"
#include <algorithm>
/* -------------------------------------------------------------------------- */

namespace tamaas {

/* -------------------------------------------------------------------------- */

template <UInt dim>
void SurfaceGenerator<dim>::setSizes(std::array<UInt, dim> n) {
  global_size = n;
  auto local_size = Partitioner<dim>::local_size(std::move(n));
  grid.resize(local_size);

  Logger().get(LogLevel::debug)
      << TAMAAS_MSG("Local N0 ", local_size[0], ", Global N0 ", global_size[0]);

  // TODO exclude invalid ranks from the main communicator
  if (global_size[0] != 0 and local_size[0] == 0) {
    std::cerr << TAMAAS_MSG("FFT data partition gave no data on [", mpi::rank(),
                            "|", mpi::size(), "], aborting\n");
    mpi::abort(MPI_ERR_TOPOLOGY);
  }
}

/* -------------------------------------------------------------------------- */

template <UInt dim>
void SurfaceGenerator<dim>::setSizes(const UInt n[dim]) {
  std::array<UInt, dim> size;
  std::copy_n(n, dim, size.begin());
  setSizes(std::move(size));
}

/* -------------------------------------------------------------------------- */

template <UInt dim>
void SurfaceGenerator<dim>::setRandomSeed(long seed) {
  mpi::bcast(&seed, 1);
  random_seed = seed;

  if (seed < 1)
    Logger().get(LogLevel::warning)
        << "Setting a seed < 1 is the same as seed = 1\n";
}

/* -------------------------------------------------------------------------- */

template class SurfaceGenerator<1>;
template class SurfaceGenerator<2>;

}  // namespace tamaas
