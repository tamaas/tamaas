/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
#include "surface_generator_random_phase.hh"
#include <iostream>
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

template <UInt dim>
Grid<Real, dim>& SurfaceGeneratorRandomPhase<dim>::buildSurface() {
  TAMAAS_ASSERT(this->grid.dataSize() != 0,
                "the size of the grid is zero, did you call setSizes() ?");
  TAMAAS_ASSERT(this->filter, "filter is null, did you call setFilter() ?");

  // Resizing arrays
  auto hermitian_dim =
      GridHermitian<Real, dim>::hermitianDimensions(this->grid.sizes());
  this->white_noise.resize(hermitian_dim);
  this->filter_coefficients.resize(hermitian_dim);

  auto wavevectors =
      FFTEngine::template computeFrequencies<Real, dim, true>(hermitian_dim);
  auto real_coeffs = FFTEngine::template realCoefficients<dim>(hermitian_dim);

  /// Generating white noise with uniform distribution for phase
  this->template generateWhiteNoise<uniform_real_distribution<Real>>();

  /// Remove numbers that should not be complex
  for (auto&& idx : real_coeffs)
    this->white_noise(idx) = 0;

  /// Applying filters
  this->filter->computeFilter(this->filter_coefficients);

  Loop::loop(
      [] CUDA_LAMBDA(Complex & coeff, Real & phase) {
        coeff *= thrust::polar(1., 2 * M_PI * phase);
      },
      this->filter_coefficients, this->white_noise);

  this->engine->backward(this->grid, this->filter_coefficients);

  /// Normalizing surface for hrms independent of number of points
  this->grid *= this->grid.globalDataSize();

  return this->grid;
}

/* -------------------------------------------------------------------------- */
template class SurfaceGeneratorRandomPhase<1>;
template class SurfaceGeneratorRandomPhase<2>;
/* -------------------------------------------------------------------------- */

}  // namespace tamaas
