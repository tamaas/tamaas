/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#ifndef ISOPOWERLAW_H
#define ISOPOWERLAW_H
/* -------------------------------------------------------------------------- */
#include "filter.hh"
#include "grid_hermitian.hh"
#include "static_types.hh"
/* -------------------------------------------------------------------------- */

namespace tamaas {

/// Class representing an isotropic power law spectrum
template <UInt dim>
class Isopowerlaw : public Filter<dim> {
public:
  /// Compute filter coefficients
  void
  computeFilter(GridHermitian<Real, dim>& filter_coefficients) const override;

  /// Compute a point of the PSD
  CUDA_LAMBDA inline Real operator()(const VectorProxy<Real, dim>& q_vec) const;

  /// Analytical rms of heights
  Real rmsHeights() const;

  /// Analytical moments
  std::vector<Real> moments() const;

  /// Analytical Nayak's parameter
  Real alpha() const;

  /// Analytical RMS slopes
  Real rmsSlopes() const;

  /// Computes ∫ k^q ɸ(k) k dk from 0 to ∞
  Real radialPSDMoment(Real q) const;

  /// Compute full contact energy (unscaled by E*)
  Real elasticEnergy() const;

public:
  TAMAAS_ACCESSOR(q0, UInt, Q0);
  TAMAAS_ACCESSOR(q1, UInt, Q1);
  TAMAAS_ACCESSOR(q2, UInt, Q2);
  TAMAAS_ACCESSOR(hurst, Real, Hurst);

protected:
  UInt q0, q1, q2;
  Real hurst;
};

/* -------------------------------------------------------------------------- */
/* Inline implementations                                                     */
/* -------------------------------------------------------------------------- */

template <UInt dim>
CUDA_LAMBDA inline Real Isopowerlaw<dim>::
operator()(const VectorProxy<Real, dim>& q_vec) const {
  const Real C = 1.;
  const Real q = q_vec.l2norm();

  Real val;

  if (q < q0)
    val = 0;
  else if (q > q2)
    val = 0;
  else if (q < q1)
    val = C;
  else
    val = C * std::pow((q / q1), -(2. * hurst + dim));

  return std::sqrt(val);
}

}  // namespace tamaas

#endif  // ISOPOWERLAW_H
