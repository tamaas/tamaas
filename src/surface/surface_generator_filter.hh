/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */

#ifndef SURFACE_GENERATOR_FILTER_H
#define SURFACE_GENERATOR_FILTER_H
/* -------------------------------------------------------------------------- */
#include "fft_engine.hh"
#include "filter.hh"
#include "partitioner.hh"
#include "surface_generator.hh"
#include "tamaas.hh"
/* -------------------------------------------------------------------------- */

namespace tamaas {

template <UInt dim>
class SurfaceGeneratorFilter : public SurfaceGenerator<dim> {
public:
  /// Default constructor
  using SurfaceGenerator<dim>::SurfaceGenerator;
  /// Construct with surface size
  // using SurfaceGenerator<dim>::SurfaceGenerator(std::array<UInt, dim> sizes);

public:
  /// Build surface with Hu & Tonder algorithm
  Grid<Real, dim>& buildSurface() override;
  /// Set filter object
  void setFilter(std::shared_ptr<Filter<dim>> new_filter) {
    setSpectrum(new_filter);
  }
  /// Set spectrum
  void setSpectrum(std::shared_ptr<Filter<dim>> spectrum) { filter = spectrum; }
  /// Get spectrum
  const Filter<dim>* getSpectrum() const { return filter.get(); }

protected:
  /// Apply filter coefficients on white noise
  void applyFilterOnSource();
  /// Generate white noise with given distribution
  template <typename T>
  void generateWhiteNoise();

protected:
  std::shared_ptr<Filter<dim>> filter = nullptr;
  GridHermitian<Real, dim> filter_coefficients;
  Grid<Real, dim> white_noise;
  std::unique_ptr<FFTEngine> engine = FFTEngine::makeEngine();
};

/* -------------------------------------------------------------------------- */
/* Template implementations                                                   */
/* -------------------------------------------------------------------------- */

template <UInt dim>
template <typename T>
void SurfaceGeneratorFilter<dim>::generateWhiteNoise() {
  random_engine gen(this->random_seed);
  T distribution;

  const auto offset = Partitioner<dim>::local_offset(white_noise);

  // Exhausting the random values of lower rank processes
  for (UInt i = 0; i < offset; ++i)
    distribution(gen);

  for (auto& noise : white_noise)
    noise = distribution(gen);
}

}  // namespace tamaas

#endif
