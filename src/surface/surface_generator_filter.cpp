/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
#include "surface_generator_filter.hh"
#include <iostream>
/* -------------------------------------------------------------------------- */
namespace tamaas {

/* -------------------------------------------------------------------------- */

template <UInt dim>
void SurfaceGeneratorFilter<dim>::applyFilterOnSource() {
  GridHermitian<Real, dim> fft_white_noise(filter_coefficients.sizes(), 1);
  engine->forward(this->white_noise, fft_white_noise);
  fft_white_noise *= filter_coefficients;
  engine->backward(this->grid, fft_white_noise);
}

/* -------------------------------------------------------------------------- */

template <UInt dim>
Grid<Real, dim>& SurfaceGeneratorFilter<dim>::buildSurface() {
  TAMAAS_ASSERT(this->grid.dataSize() != 0,
                "the size of the grid is zero, did you call setSizes() ?");
  TAMAAS_ASSERT(this->filter, "filter is null, did you call setFilter() ?");

  // Resizing arrays
  this->white_noise.resize(this->grid.sizes());
  auto hermitian_dim =
      GridHermitian<Real, dim>::hermitianDimensions(this->grid.sizes());
  this->filter_coefficients.resize(hermitian_dim);

  /// Generating white noise with gaussian distribution
  this->generateWhiteNoise<normal_distribution<Real>>();

  /// Applying filters
  filter->computeFilter(this->filter_coefficients);
  this->applyFilterOnSource();

  /// Normalizing surface for hrms independent of number of points
  this->grid *= std::sqrt(this->grid.globalDataSize());

  return this->grid;
}

/* -------------------------------------------------------------------------- */
template class SurfaceGeneratorFilter<1>;
template class SurfaceGeneratorFilter<2>;
/* -------------------------------------------------------------------------- */

}  // namespace tamaas
